Atlas Pipeline
===============================

In JVM process pipeline based on [Akka Streams](http://doc.akka.io/docs/akka/2.4/scala/stream/index.html). This will be
the primary "Runner" for executing the pipeline (ETL, PLD, Persistence etc)

Prerequisites
-------------

This application requires:

1. Scala 2.12.1+
2. Java 1.8+
3. SBT 0.13.9+

The application depends on other Atlas libraries, notably atlas-logging and atlas-batch-collector which are deployed in the 
`http://artifactory.dev.onzo.cloud/artifactory/virsbt` repository. A relevant entry should be made in the
~/.sbt/repositories file. Alternatively the repository can be added to this project's build.sbt file

Building
--------

The pipeline is built using SBT, along with the SBT fat jar plugin. To build it check out the source
and run `$sbt clean update assembly`. This will produce `target/scala-2.12/atlas-pipeline-fat-1.0-SNAPSHOT.jar`

Starting
--------

The application can be run using `java -jar target/scala-2.12/atlas-pipeline-fat-1.0-SNAPSHOT.jar`. By default it will
look for a cassandra host running on 127.0.0.1 however this can be overridden by setting the `CASSANDRA_HOSTS` environment
variable to a comma separated list of host names e.g. `export CASSANDRA_HOSTS="10.0.0.1,10.0.0.2,10.0.0.3"`

The pipeline is deployed as a webservice and it will listen on port 8080 by default. This can be changed be editing
src/main/resources/application.conf, setting an environment variable e.g. `export WEBSERVER_PORT=8080` 
or passing a system property e.g. `-Dwebserver.port=8080`

**Cassandra schema**

By default the pipeline will run in `prod` mode meaning no DDL type changes will be applied. This implies that the correct
schema must be in place before the app runs. Alternatively the pipeline can be run in dev mode by passing `-Denv=dev` which
will result in DDL changes being applied i.e. drop and recreate the schema

Running for all clients
-----------------------

The pipeline can be invoked for all configured clients by calling `http://server:port/run/all`

Logging
-------

Application logs will be written to stdout in [Logstash](https://www.elastic.co/products/logstash) format
