package com.onzo.pipeline.persistence

import java.io.InputStream

import com.datastax.driver.core.Session
import com.onzo.common.domain.{ClientId, HouseholdId}
import com.onzo.pipeline.domain.{HouseholdProfile, HouseholdProfileEntity}
import com.onzo.pipeline.persistence.HouseholdProfilePersisterSpec._
import org.scalactic.TypeCheckedTripleEquals
import org.scalatest.{BeforeAndAfterAll, FunSuite, Matchers}

class HouseholdProfilePersisterSpec extends FunSuite with BeforeAndAfterAll
  with Matchers
  with TypeCheckedTripleEquals
  with TestCluster {

  import scala.concurrent.ExecutionContext.Implicits.global

  var householdPersister: HouseholdProfilePersister = _
  private implicit val cluster = buildCluster()
  var session: Session = _

  override protected def beforeAll(): Unit = {
    var is: InputStream = null
    try {
      is = this.getClass.getClassLoader.getResourceAsStream("cassandra_schema.cdl")
      val cql = scala.io.Source.fromInputStream(is).mkString.split(";")
      session = cluster.connect()
      cql.foreach(session.execute)
      householdPersister = HouseholdProfilePersister.create(session, "onzo")
    } finally is.close()
  }

  test("persist profile with no values") {
    val householdProfileWithId = HouseholdProfileEntity(ClientId(1), HouseholdId("1"), emptyProfile)
    val status = householdPersister.persist(householdProfileWithId)
    withClue("no exceptions") {
      if (status.isFailure) fail(status.failed.get)
    }
  }

  // Success(HouseholdProfile(None, Some(0),Some(false),Some(0),Some(false),Some(false),Some(false),Some(false),Some(false),Some(false),Some(false),Some(false),Some(false),Some(false),Some(false))) did not equal
  // Success(HouseholdProfile(None, None,   None,       None,   None,       Some(true), None,       None,       None,       None,       None,       None,       None,       None,       None))

  /* consider implementing these
  test("persist and reload profile with no values") {
    val householdProfileWithId = HouseholdProfileWithId(ClientId(1), HouseholdId("1"), emptyProfile)
    val status = householdPersister.persist(householdProfileWithId)
    val reload: Try[HouseholdProfile] = householdPersister.findProfileById(clientId, onzoId)
    reload should === (Success(electricHeatingProfile))
  }

  test("persist and reload profile with a single value") {
    val householdProfileWithId: HouseholdProfileWithId = HouseholdProfileWithId(clientId, onzoId, electricHeatingProfile)
    val status = householdPersister.persist(householdProfileWithId)
    val reload: Try[HouseholdProfile] = householdPersister.findProfileById(clientId, onzoId)
    reload should === (Success(emptyProfile))
  } */



  override protected def afterAll(): Unit = {
    session.close()
    cluster.close()
  }
}
/*
ref python api:
https://github.com/onzo-com/atlas-python-api/blob/master/atlas_api/resources/profile.py # line 46
  def _validate_put(request_json, onzo_household_id, organisation_id):

V2

{type: gasheating},
'gas_heating': {'bothElecGas': True, 'gas': True, 'elec': False,
                                    'other': False}[request_json['heatingType']],

{type: gashotwater},
gas_hot_water': {'bothElecGas': True, 'gas': True, 'elec': False,
                                      'other': False}[request_json['hotWaterType']],
{type: gascooking}
'gas_cooking': {'bothElecGas': True, 'gas': True, 'elec': False,
                                    'other': False, None: None}[request_json.get('cookingType')],
{type: electricheating},
'electric_heating': {'bothElecGas': True, 'gas': False, 'elec': True,
                                         'other': False}[request_json['heatingType']],
{type: electrichotwater}
'electric_hot_water': {'bothElecGas': True, 'gas': False, 'elec': True,
                                           'other': False}[request_json['hotWaterType']],

V1
'gas_heating': None,
'gas_hot_water': None,
'gas_cooking': None,
'electric_heating': {'topUpElec': False, 'noElec': False,
                                         'elec': True}[request_json['heatingType']],

'electric_hot_water': None,
UndefinedHotWater

CREATE TABLE IF NOT EXISTS household_profile (
    organisation_id int,
    onzo_household_id text,
    house_type text,
    number_of_people int,
    people_plus boolean,
    number_of_bedrooms int,
    bedrooms_plus boolean,
    electric_heating boolean,
    top_up_electric_heating boolean,
    electric_hot_water boolean,
    electric_cooking boolean,
    top_up_electric_cooking boolean,
    gas_heating boolean,
    gas_cooking boolean,
    gas_hot_water boolean,
    compressive_cooling boolean,
    fan_cooling boolean,
    PRIMARY KEY(organisation_id, onzo_household_id)
);
 */

object HouseholdProfilePersisterSpec {
  val clientId: ClientId = ClientId(1)
  val onzoId: HouseholdId = HouseholdId("1")

  val emptyProfile = HouseholdProfile(
    None,
    None,
    None,
    None,
    None,
    None,
    None,
    None,
    None,
    None,
    None,
    None,
    None,
    None,
    None
  )

  val electricHeatingProfile = HouseholdProfile(
    None,
    None,
    None,
    None,
    None,
    electricHeating = Some(true),
    None,
    None,
    None,
    None,
    None,
    None,
    None,
    None,
    None
  )
}