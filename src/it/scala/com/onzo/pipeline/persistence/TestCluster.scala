package com.onzo.pipeline.persistence

import com.datastax.driver.core.Cluster

import scala.util.Properties

trait TestCluster {
  private val socketOptions = new com.datastax.driver.core.SocketOptions()
  socketOptions.setReadTimeoutMillis(60000)
  socketOptions.setConnectTimeoutMillis(60000)

  def buildCluster(): Cluster = Cluster.builder()
    .addContactPoints(cassandraHosts: _*)
    .withSocketOptions(socketOptions)
    .build()

  private def cassandraHosts: Seq[String] = {
    Properties.envOrNone("CASSANDRA_HOSTS").map(_.trim.split(",").toSeq.map(_.trim)).getOrElse(
      Seq("localhost")
    )
  }
}
