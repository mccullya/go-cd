package com.onzo.pipeline.persistence

import java.io.InputStream
import java.time.{LocalDate, LocalDateTime, ZoneId, ZonedDateTime}
import java.util.Date
import java.util.concurrent.TimeUnit

import com.datastax.driver.core.{Cluster, Session, UDTValue, UserType}
import com.onzo.common.domain.{ClientId, HouseholdId, OnzoDeviceId}
import com.onzo.pipeline.domain._
import com.onzo.pipeline.persistence.TestData.runDay20170101
import org.scalatest.{BeforeAndAfterAll, FunSuite}
import squants.energy.{WattHours, Watts}
import squants.market.{EUR, GBP}
import squants.motion.CubicMetersPerSecond
import squants.space.CubicMeters

import scala.collection.JavaConverters._
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.util.Properties

/**
  * Created by toby.hobson on 08/03/2017.
  */
class GasConsumptionPersisterSpec extends FunSuite with BeforeAndAfterAll
  with TestCluster {

  import scala.concurrent.ExecutionContext.Implicits.global

  var gasPersister: GasConsumptionPersister = _
  var cluster: Cluster = buildCluster()
  var session: Session = _
  var gasConsumptionUDT: UserType = _

  override protected def beforeAll(): Unit = {
    var is: InputStream = null
    try {
      is = this.getClass.getClassLoader.getResourceAsStream("cassandra_schema.cdl")
      val cql = scala.io.Source.fromInputStream(is).mkString.split(";")
      session = cluster.connect()
      cql.foreach(session.execute)
      gasPersister = new GasConsumptionCassandraPersister(session, "onzo")
      gasConsumptionUDT = cluster.getMetadata.getKeyspace("onzo").getUserType("gas_consumption")
    } finally is.close()
  }

  test("persist daily consumption with no intervals") {
    val data = WindowedGasConsumption(
      clientId = ClientId(2),
      onzoHouseholdId = HouseholdId("1"),
      onzoDeviceId = OnzoDeviceId("2"),
      day = runDay20170101,
      resolution = Resolution.Min15,
      timezone = ZoneId.of("Europe/London"),
      energy = WattHours(1),
      volume = CubicMeters(2),
      flowRate = CubicMetersPerSecond(4), // N.B. will be saved as cm3_per_minute i.e. 4 * 60 == 240.0
      averagePower = Watts(3),
      cost = GBP(10),
      intervals = Map.empty
    )

    val status = gasPersister.persist(data)

    withClue("no exceptions") {
      assert(status.isSuccess)
    }

    val result = session.execute(
      s"SELECT * FROM onzo.consumption_intraday_gas_by_household WHERE organisation_id = 2 AND onzo_household_id = '1'"
    )

    val all = result.all().asScala
    withClue("single record written") {
      assert(all.length == 1)
    }
    val first = all.head

    val actualClientId = first.getInt("organisation_id")
    withClue("client_id == greenchoice") {
      assert(actualClientId == 2)
    }

    val actualHouseholdId = first.getString("onzo_household_id")
    withClue("household_id == 1") {
      assert(actualHouseholdId == "1")
    }

    val actualDeviceId = first.getString("onzo_device_id")
    withClue("device_id == 2") {
      assert(actualDeviceId == "2")
    }

    val actualDay = first.getDate("day")
    withClue("day == 01-01-2017") {
      assert(actualDay.getYear == 2017)
      assert(actualDay.getMonth == 1)
      assert(actualDay.getDay == 1)
    }

    val actualResolution = first.getInt("resolution_seconds")
    withClue("resolution_seconds == 900") {
      assert(actualResolution == 900)
    }

    val actualZone = first.getString("timezone")
    withClue("timezone == Europe/London") {
      assert(actualZone == "Europe/London")
    }

    val actualEnergy = first.getDecimal("energy_wh")
    withClue("energy_wh == 1000") {
      assert(actualEnergy == BigDecimal(1.0).bigDecimal)
    }

    val actualVolume = first.getDecimal("volume_cm3")
    withClue("volume_cm3 == 1000") {
      assert(actualVolume == BigDecimal(2.0).bigDecimal)
    }

    val actualFlowRate = first.getDecimal("flow_rate_cm3_per_minute")
    withClue("flow_rate_cm3_per_minute == 2400") {
      assert(actualFlowRate == BigDecimal(240.0).bigDecimal)
    }

    val actualPower = first.getDecimal("power_w")
    withClue("power_w == 1000") {
      assert(actualPower == BigDecimal(3.0).bigDecimal)
    }

    val actualCost = first.getDecimal("cost")
    withClue("cost == 10") {
      assert(actualCost == BigDecimal(10.0).bigDecimal)
    }

    withClue("no intervals") {
      val intervals = first.getMap("consumption", classOf[Date], classOf[UDTValue])
      assert(intervals.isEmpty)
    }
  }

  test("persist consumption intervals") {
    val midnightConsumption = Map(
      ZonedDateTime.of(2017, 1, 1, 0, 0, 0, 0, ZoneId.of("CET")) ->
        GasConsumption(
          energy = WattHours(10),
          power = Watts(20),
          volume = CubicMeters(2),
          flowRate = CubicMetersPerSecond(3), // N.B. will be saved as cm3_per_minute i.e 3 * 60 == 180
          cost = GBP(30)
        )
    )

    val data = WindowedGasConsumption(
      clientId = ClientId(2),
      onzoHouseholdId = HouseholdId("1"),
      onzoDeviceId = OnzoDeviceId("2"),
      day = runDay20170101,
      resolution = Resolution.Min15,
      timezone = ZoneId.of("Europe/London"),
      energy = WattHours(1),
      volume = CubicMeters(2),
      flowRate = CubicMetersPerSecond(4),
      averagePower = Watts(3),
      cost = GBP(10),
      intervals = midnightConsumption
    )

    val status = gasPersister.persist(data)
    status.failed.foreach(_.printStackTrace())
    withClue("no exceptions") {
      assert(status.isSuccess)
    }

    val result = session.execute(
      s"SELECT * FROM onzo.consumption_intraday_gas_by_household WHERE organisation_id = 2 AND onzo_household_id = '1'"
    )

    val all = result.all().asScala
    withClue("single record written") {
      assert(all.length == 1)
    }
    val first = all.head
    val intervals = first.getMap("consumption", classOf[Date], classOf[UDTValue]).asScala

    withClue("interval length of 1") {
      assert(intervals.size == 1)
    }

    val firstInterval = intervals.head
    withClue("first interval has midnight timestamp") {
      val expectedDate = Date.from(
        LocalDateTime.of(2017, 1, 1, 0, 0).atZone(ZoneId.of("CET")).toInstant
      )
      assert(firstInterval._1 == expectedDate)
    }

    val udt = firstInterval._2

    withClue("first interval has correct energy") {
      val actualEnergy = udt.getDecimal("energy_wh")
      assert(actualEnergy == BigDecimal(10.0).bigDecimal)
    }

    withClue("first interval has correct power") {
      val actualPower = udt.getDecimal("power_w")
      assert(actualPower == BigDecimal(20.0).bigDecimal)
    }

    withClue("first interval has correct cost") {
      val actualCost = udt.getDecimal("cost")
      assert(actualCost == BigDecimal(30.0).bigDecimal)
    }

    withClue("first interval has correct volume") {
      val actualVolume = udt.getDecimal("volume_cm3")
      assert(actualVolume == BigDecimal(2.0).bigDecimal)
    }

    withClue("first interval has correct flow rate") {
      val actualFlowRate = udt.getDecimal("flow_rate_cm3_per_minute")
      assert(actualFlowRate == BigDecimal(180.0).bigDecimal)
    }
  }

  override protected def afterAll(): Unit = {
    session.close()
    cluster.close()
  }
}