package com.onzo.pipeline.persistence

import java.io.InputStream
import java.time.Instant

import com.datastax.driver.core.{Cluster, Session}
import com.onzo.common.domain.{ClientId, HouseholdId, SensorId}
import com.onzo.pipeline.domain.{BreakdownOutputWasProduced, ConsumptionOutputWasProduced}
import org.scalactic.TypeCheckedTripleEquals
import org.scalatest.{BeforeAndAfterAll, FunSuite}

import scala.collection.JavaConverters._

class ServiceMetricsCassandraPersisterSpec extends FunSuite with TypeCheckedTripleEquals with BeforeAndAfterAll with TestCluster {
  private val cluster: Cluster = buildCluster()
  private var persister: ServiceMetricsCassandraPersister = _
  var session: Session = _

  override protected def beforeAll(): Unit = {
    var is: InputStream = null
    try {
      is = this.getClass.getClassLoader.getResourceAsStream("cassandra_schema.cdl")
      val cql = scala.io.Source.fromInputStream(is).mkString.split(";")
      session = cluster.connect()
      cql.foreach(session.execute)
      persister = ServiceMetricsCassandraPersister.create(session, "onzo")
    } finally is.close()
  }

  override protected def afterAll(): Unit = {
    session.close()
    cluster.close()
  }

  test("persist output breakdown timestamp") {
    // given
    val tmNow = Instant.ofEpochSecond(1234567890)
    val txNow = "2009-02-13T23:31:30Z"
    val rawClientId = 42
    val rawHouseholdId = "HOUSEHOLDID"
    val rawSensorId = "SENSORID"
    val data = BreakdownOutputWasProduced(
      when = tmNow,
      clientId = ClientId(rawClientId),
      householdId = HouseholdId(rawHouseholdId),
      sensorId = SensorId(rawSensorId)
    )

    // when
    val status = persister.persist(data)

    // then
    withClue("there are no exceptions") {
      assert(status.isSuccess)
    }

    val rows = session.execute {
      s"""SELECT *
         |FROM onzo.service_metrics
         |WHERE organisation_id = $rawClientId
         |      AND onzo_household_id = '$rawHouseholdId'
         |      AND onzo_device_id = '$rawSensorId'
       """.stripMargin
    }.all().asScala

    assert(rows.length === 1)
    val row = rows.head

    val actualOutputBreakdownTimestamp = row.getString("when_breakdown_output_produced")
    assert(actualOutputBreakdownTimestamp === txNow)
  }

  test("persist output consumption timestamp") {
    // given
    val tmNow = Instant.ofEpochSecond(1234567890)
    val txNow = "2009-02-13T23:31:30Z"
    val rawClientId = 42
    val rawHouseholdId = "HOUSEHOLDID"
    val rawSensorId = "SENSORID"
    val data = ConsumptionOutputWasProduced(
      when = tmNow,
      clientId = ClientId(rawClientId),
      householdId = HouseholdId(rawHouseholdId),
      sensorId = SensorId(rawSensorId)
    )

    // when
    val status = persister.persist(data)

    // then
    withClue("there are no exceptions") {
      assert(status.isSuccess)
    }

    val rows = session.execute {
      s"""SELECT *
         |FROM onzo.service_metrics
         |WHERE organisation_id = $rawClientId
         |      AND onzo_household_id = '$rawHouseholdId'
         |      AND onzo_device_id = '$rawSensorId'
       """.stripMargin
    }.all().asScala

    assert(rows.length === 1)
    val row = rows.head

    val actualOutputConsumptionTimestamp = row.getString("when_consumption_output_produced")
    assert(actualOutputConsumptionTimestamp === txNow)
  }

  test("persist two different timestamps") {
    // given
    val rawClientId = 42
    val rawOnzoHouseholdId = "HOUSEHOLDID"
    val rawSensorId = "SENSORID"
    val tmA = Instant.ofEpochSecond(1230987654)
    val txA = "2009-01-03T13:00:54Z"
    val dataA = BreakdownOutputWasProduced(
      when = tmA,
      clientId = ClientId(rawClientId),
      householdId = HouseholdId(rawOnzoHouseholdId),
      sensorId = SensorId(rawSensorId)
    )
    val tmB = Instant.ofEpochSecond(1234567890)
    val txB = "2009-02-13T23:31:30Z"
    val dataB = ConsumptionOutputWasProduced(
      when = tmB,
      clientId = ClientId(rawClientId),
      householdId = HouseholdId(rawOnzoHouseholdId),
      sensorId = SensorId(rawSensorId)
    )

    // when
    val statusA = persister.persist(dataA)
    val statusB = persister.persist(dataB)

    // then
    withClue("there are no exceptions") {
      assert(statusA.isSuccess && statusB.isSuccess)
    }

    val rows = session.execute {
      s"""SELECT *
         |FROM onzo.service_metrics
         |WHERE organisation_id = $rawClientId
         |      AND onzo_household_id = '$rawOnzoHouseholdId'
         |      AND onzo_device_id = '$rawSensorId'
       """.stripMargin
    }.all().asScala

    assert(rows.length === 1)
    val row = rows.head

    val actualOutputBreakdownTimestamp = row.getString("when_breakdown_output_produced")
    assert(actualOutputBreakdownTimestamp === txA)

    val actualOutputConsumptionTimestamp = row.getString("when_consumption_output_produced")
    assert(actualOutputConsumptionTimestamp === txB)
  }
}
