package com.onzo.pipeline.persistence

import java.io.InputStream
import java.time._
import java.util.Date
import java.util.concurrent.TimeUnit

import com.datastax.driver.core.{Cluster, Session, UDTValue, UserType}
import com.onzo.common.domain.{ClientId, HouseholdId, OnzoDeviceId}
import com.onzo.pipeline.domain._
import com.onzo.pipeline.persistence.TestData.runDay20170101
import org.scalatest.{BeforeAndAfterAll, FunSuite}
import squants.energy.{WattHours, Watts}
import squants.market.EUR

import scala.collection.JavaConverters._
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.util.Properties

/**
  * Created by toby.hobson on 08/03/2017.
  */
class ElecConsumptionPersisterSpec extends FunSuite with BeforeAndAfterAll
  with TestCluster {

  import scala.concurrent.ExecutionContext.Implicits.global

  var elecPersister: ElectricConsumptionPersister = _
  var elecFindAllByUser: ElectricConsumptionFindAllByUser = _
  private implicit val cluster = buildCluster()
  implicit var session: Session = _
  var electricConsumptionUDT: UserType = _

  override protected def beforeAll(): Unit = {
    var is: InputStream = null
    try {
      is = this.getClass.getClassLoader.getResourceAsStream("cassandra_schema.cdl")
      val cql = scala.io.Source.fromInputStream(is).mkString.split(";")
      session = cluster.connect()
      cql.foreach(session.execute)
      elecPersister = new ElectricConsumptionCassandraPersister(session, "onzo")
      elecFindAllByUser = ElectricConsumptionFindAllByUser.create(session, "onzo")
      electricConsumptionUDT = cluster.getMetadata.getKeyspace("onzo").getUserType("electric_consumption")
    } finally is.close()
  }

  test("persist daily consumption with no intervals") {
    val data = WindowedElectricConsumption(
      clientId = ClientId(2),
      onzoHouseholdId = HouseholdId("1"),
      onzoDeviceId = OnzoDeviceId("2"),
      day = runDay20170101,
      resolution = Resolution.Min15,
      timezone = ZoneId.of("Europe/London"),
      energy = WattHours(1),
      averagePower = Watts(1),
      cost = EUR(10),
      intervals = Map.empty
    )
    val status = elecPersister.persist(data)
    withClue("no exceptions") {
      if (status.isFailure) fail(status.failed.get)
    }
    val result = session.execute(
      s"SELECT * FROM onzo.consumption_intraday_elec_by_household WHERE organisation_id = 2 AND onzo_household_id = '1'"
    )
    val all = result.all().asScala
    withClue("single record written") {
      assert(all.length == 1)
    }
    val first = all.head

    val actualClientId = first.getInt("organisation_id")
    withClue("client_id == greenchoice") {
      assert(actualClientId == 2)
    }

    val actualHouseholdId = first.getString("onzo_household_id")
    withClue("household_id == 1") {
      assert(actualHouseholdId == "1")
    }

    val actualDeviceId = first.getString("onzo_device_id")
    withClue("device_id == 2") {
      assert(actualDeviceId == "2")
    }

    val cassandraDay = first.getDate("day")
    // need to convert to ZonedDateTime
    val timestamp = cassandraDay.getMillisSinceEpoch
    val actualDay = ZonedDateTime.ofInstant(Instant.ofEpochMilli(timestamp), ZoneId.of("CET"))
    withClue("day == 01-01-2017") {
      assert(actualDay.getYear == 2017)
      assert(actualDay.getMonth == Month.JANUARY)
      assert(actualDay.getDayOfYear == 1)
    }

    val actualResolution = first.getInt("resolution_seconds")
    withClue("resolution_seconds == 900") {
      assert(actualResolution == 900)
    }

    val actualTimezone = first.getString("timezone")
    withClue("timezone == Europe/Lodon") {
      assert(actualTimezone == "Europe/London")
    }

    val actualEnergy = first.getDecimal("energy_wh")
    withClue("daily_energy_wh == 1000") {
      assert(actualEnergy == BigDecimal(1.0).bigDecimal)
    }

    val actualPower = first.getDecimal("power_w")
    withClue("daily_power_w == 1000") {
      assert(actualPower == BigDecimal(1.0).bigDecimal)
    }

    val actualCost = first.getDecimal("cost")
    withClue("cost == 10") {
      assert(actualCost == BigDecimal(10.0).bigDecimal)
    }

    withClue("no intervals") {
      val intervals = first.getMap("consumption", classOf[Date], classOf[UDTValue])
      assert(intervals.isEmpty)
    }
  }

  test("persist consumption intervals") {
    val midnightConsumption = Map(
      ZonedDateTime.of(2017, 1, 1, 0, 0, 0, 0, ZoneId.of("CET")) ->
        ElectricConsumption(
          energy = WattHours(10),
          power = Watts(20),
          cost = EUR(30)
        )
    )

    val data = WindowedElectricConsumption(
      clientId = ClientId(2),
      onzoHouseholdId = HouseholdId("1"),
      onzoDeviceId = OnzoDeviceId("2"),
      day = runDay20170101,
      resolution = Resolution.Min15,
      timezone = ZoneId.of("Europe/London"),
      energy = WattHours(1),
      averagePower = Watts(1),
      cost = EUR(10),
      intervals = midnightConsumption
    )

    val status = elecPersister.persist(data)
    withClue("no exceptions") {
      if (status.isFailure) fail(status.failed.get)
    }

    val result = session.execute(
      s"SELECT * FROM onzo.consumption_intraday_elec_by_household WHERE organisation_id = 2 AND onzo_household_id = '1'"
    )

    val all = result.all().asScala
    withClue("single record written") {
      assert(all.length == 1)
    }
    val first = all.head
    val intervals = first.getMap("consumption", classOf[Date], classOf[UDTValue]).asScala

    withClue("interval length of 1") {
      assert(intervals.size == 1)
    }

    val firstInterval = intervals.head
    withClue("first interval has midnight timestamp") {
      val expectedDate = Date.from(
        LocalDateTime.of(2017, 1, 1, 0, 0).atZone(ZoneId.of("CET")).toInstant
      )
      assert(firstInterval._1 == expectedDate)
    }

    val udt = firstInterval._2

    withClue("first interval has correct energy") {
      val actualEnergy = udt.getDecimal("energy_wh")
      assert(actualEnergy == BigDecimal(10.0).bigDecimal)
    }

    withClue("first interval has correct power") {
      val actualPower = udt.getDecimal("power_w")
      assert(actualPower == BigDecimal(20.0).bigDecimal)
    }

    withClue("first interval has correct cost") {
      val actualCost = udt.getDecimal("cost")
      assert(actualCost == BigDecimal(30.0).bigDecimal)
    }
  }

  test("findAllByUser") {
    val clientId = ClientId(2)
    val onzoHouseholdId = HouseholdId("1")
    val onzoDeviceId = OnzoDeviceId("2")

    val midnightConsumption = Map(
      ZonedDateTime.of(2017, 1, 1, 0, 0, 0, 0, ZoneId.of("CET")) ->
        ElectricConsumption(
          energy = WattHours(10),
          power = Watts(20),
          cost = EUR(30)
        )
    )

    val data = WindowedElectricConsumption(
      clientId = clientId,
      onzoHouseholdId = onzoHouseholdId,
      onzoDeviceId = onzoDeviceId,
      day = runDay20170101,
      resolution = Resolution.Min15,
      timezone = ZoneId.of("Europe/London"),
      energy = WattHours(1),
      averagePower = Watts(1),
      cost = EUR(10),
      intervals = midnightConsumption
    )

    val status = elecPersister.persist(data)
    withClue("no exceptions") {
      if (status.isFailure) fail(status.failed.get)
    }

    val results = elecFindAllByUser.findAllByUser(clientId, onzoHouseholdId).toSeq
    withClue("1 record found") {
      assert(results.length == 1)
    }

    assert(results.head == data)
  }

  override protected def afterAll(): Unit = {
    session.close()
    cluster.close()
  }
}
