package com.onzo.pipeline.persistence

import java.io.InputStream
import java.time.LocalDate

import com.datastax.driver.core.{Cluster, Session}
import com.onzo.common.domain.{ClientId, HouseholdId, SensorId}
import com.onzo.pipeline.domain.{BreakdownOutputWasProduced, DailyElectricBreakdown, RunDay}
import org.scalatest.{BeforeAndAfterAll, FunSuite, Matchers}

import scala.collection.JavaConverters._

class CompositePersistableSpec extends FunSuite with Matchers with BeforeAndAfterAll with TestCluster {
  private type Data = (ClientId, HouseholdId, SensorId)
  private val cluster: Cluster = buildCluster()
  private var electricBreakdownPersister: ElectricBreakdownPersister = _
  private var serviceMetricsPersister: ServiceMetricsCassandraPersister = _
  private var session: Session = _

  override protected def beforeAll(): Unit = {
    var is: InputStream = null
    try {
      is = this.getClass.getClassLoader.getResourceAsStream("cassandra_schema.cdl")
      val cql = scala.io.Source.fromInputStream(is).mkString.split(";")
      session = cluster.connect()
      cql.foreach(session.execute)
      electricBreakdownPersister = ElectricBreakdownPersister.create(session, "onzo")
      serviceMetricsPersister = ServiceMetricsCassandraPersister.create(session, "onzo")
    } finally is.close()
  }

  override protected def afterAll(): Unit = {
    session.close()
    cluster.close()
  }

  test("persist to both underlying persisters") {
    // given
    val persister = {
      electricBreakdownPersister.contramap[Data] {
        case (clientId, householdId, sensorId) =>
          DailyElectricBreakdown(
            clientId,
            householdId,
            sensorId,
            RunDay(LocalDate.ofEpochDay(17301)),
            Map()
          )
      }
    } andThen {
      serviceMetricsPersister.contramap[Data] {
        case (clientId, householdId, sensorId) => BreakdownOutputWasProduced(clientId, householdId, sensorId)
      }
    }

    val rawClientId = 42
    val rawHouseholdId = "HOUSEHOLDID"
    val rawSensorId = "SENSORID"
    val data = (ClientId(rawClientId), HouseholdId(rawHouseholdId), SensorId(rawSensorId))

    // when
    val status = persister.persist(data)

    // then
    withClue("there are no exceptions") {
      assert(status.isSuccess)
    }

    withClue("first persister should have persisted") {
      val rows = session.execute {
        s"""SELECT *
           |FROM onzo.daily_electric_bill_breakdown
           |WHERE organisation_id = $rawClientId
           |      AND onzo_household_id = '$rawHouseholdId'
       """.stripMargin
      }.all().asScala

      rows.length should be(1)
    }

    withClue("second persister should have persisted") {
      val rows = session.execute {
        s"""SELECT *
           |FROM onzo.service_metrics
           |WHERE organisation_id = $rawClientId
           |      AND onzo_household_id = '$rawHouseholdId'
           |      AND onzo_device_id = '$rawSensorId'
       """.stripMargin
      }.all().asScala

      rows.length should be(1)
    }
  }

  test("persist to the first persister even when the second persister throws an exception") {
    // given
    val persister = {
      electricBreakdownPersister.contramap[Data] {
        case (clientId, householdId, sensorId) =>
          DailyElectricBreakdown(
            clientId,
            householdId,
            sensorId,
            RunDay(LocalDate.ofEpochDay(17301)),
            Map()
          )
      }
    } andThen {
      serviceMetricsPersister.contramap[Data] { _ => ??? }
    }

    val rawClientId = 43
    val rawHouseholdId = "HOUSEHOLDID"
    val rawSensorId = "SENSORID"
    val data = (ClientId(rawClientId), HouseholdId(rawHouseholdId), SensorId(rawSensorId))

    // when
    val status = persister.persist(data)

    // then
    withClue("there is an exception") {
      assert(status.isFailure)
    }

    withClue("first persister should have persisted") {
      val rows = session.execute {
        s"""SELECT *
           |FROM onzo.daily_electric_bill_breakdown
           |WHERE organisation_id = $rawClientId
           |      AND onzo_household_id = '$rawHouseholdId'
       """.stripMargin
      }.all().asScala

      rows.length should be(1)
    }
  }

  test("not persist to the second persister when the first persister throws an exception") {
    // given
    val persister = {
      electricBreakdownPersister.contramap[Data] { _ => ??? }
    } andThen {
      serviceMetricsPersister.contramap[Data] {
        case (clientId, householdId, sensorId) => BreakdownOutputWasProduced(clientId, householdId, sensorId)
      }
    }

    val rawClientId = 44
    val rawHouseholdId = "HOUSEHOLDID"
    val rawSensorId = "SENSORID"
    val data = (ClientId(rawClientId), HouseholdId(rawHouseholdId), SensorId(rawSensorId))

    // when
    val status = persister.persist(data)

    // then
    withClue("there is an exception") {
      assert(status.isFailure)
    }

    withClue("second persister should not have persisted") {
      val rows = session.execute {
        s"""SELECT *
           |FROM onzo.service_metrics
           |WHERE organisation_id = $rawClientId
           |      AND onzo_household_id = '$rawHouseholdId'
           |      AND onzo_device_id = '$rawSensorId'
       """.stripMargin
      }.all().asScala

      rows.length should be(0)
    }
  }
}
