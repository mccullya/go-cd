package com.onzo.pipeline.persistence

import java.io.InputStream
import java.time.LocalDate

import com.datastax.driver.core.{Cluster, Session}
import com.onzo.common.domain.HouseholdId
import com.onzo.etl.persistence.csr.HouseholdApiUsersRepository._
import org.scalatest.{BeforeAndAfterAll, FunSuite, Matchers}

import scala.concurrent.Await
import scala.util.Properties
import scala.concurrent.duration._

class CassandraHouseholdApiUsersRepositorySpec extends FunSuite with Matchers with BeforeAndAfterAll
  with TestCluster{

  private implicit val cluster = buildCluster()
  implicit var session: Session = _

  override protected def beforeAll(): Unit = {
    var is: InputStream = null
    try {
      is = this.getClass.getClassLoader.getResourceAsStream("cassandra_schema.cdl")
      val cql = scala.io.Source.fromInputStream(is).mkString.split(";")
      session = cluster.connect()
      cql.foreach(session.execute)
    } finally is.close()
  }

  test("save and retrieve an individual household_api_users record") {
    val householdApiUsersRepository =
      new CassandraHouseholdApiUsersRepository(session, "onzo")

    val householdApiUsersRecord =
      HouseholdApiUsers(
        OrganisationId(2),
        HouseholdId("H"),
        UserId("H"),
        active = true,
        ValidFrom(LocalDate.of(2017, 1, 1))
      )

    val insertResult =
      Await.result(householdApiUsersRepository.save(householdApiUsersRecord), 1.second)
    insertResult should be(())

    val fetchResult =
      Await.result(householdApiUsersRepository.fetchByPrimaryKey((OrganisationId(2), UserId("H"))), 1.second)

    fetchResult should be(Some(householdApiUsersRecord))
  }

  test("fetching a record that doesn't exist should return None") {
    val householdApiUsersRepository =
      new CassandraHouseholdApiUsersRepository(session, "onzo")

    val fetchResult =
      Await.result(householdApiUsersRepository.fetchByPrimaryKey((OrganisationId(2), UserId("I"))), 1.second)

    fetchResult should be(None)
  }

  override protected def afterAll(): Unit = {
    session.close()
    cluster.close()
  }
}
