package com.onzo.pipeline.persistence

import java.io.InputStream
import java.time.{ZoneId, ZonedDateTime}
import java.util.concurrent.TimeUnit

import com.datastax.driver.core.{Cluster, Session, UDTValue, UserType}
import com.onzo.common.domain.{ClientId, HouseholdId, SensorId}
import com.onzo.pipeline.domain._
import com.onzo.pipeline.persistence.TestData.runDay20170101
import org.scalatest.{BeforeAndAfterAll, FunSuite}
import pld.data.ConsumptionCategory.AlwaysOn
import squants.energy.WattHours
import squants.market.GBP

import scala.collection.JavaConverters._
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.util.Properties

/**
  * Created by toby.hobson on 08/03/2017.
  */
class ElectricBreakdownPersisterSpec extends FunSuite with BeforeAndAfterAll
  with TestCluster {

  var elecPersister: ElectricBreakdownPersister = _
  var cluster: Cluster = buildCluster()
  var session: Session = _
  var electricBreakdownUDT: UserType = _

  override protected def beforeAll(): Unit = {
    var is: InputStream = null
    try {
      is = this.getClass.getClassLoader.getResourceAsStream("cassandra_schema.cdl")
      val cql = scala.io.Source.fromInputStream(is).mkString.split(";")
      session = cluster.connect()
      cql.foreach(session.execute)
      elecPersister = new ElectricBreakdownCassandraPersister(session, "onzo")
      electricBreakdownUDT = cluster.getMetadata.getKeyspace("onzo").getUserType("electric_breakdown_commodities")
    } finally is.close()
  }

  test("persist daily electric breakdown with no categories") {
    val data = DailyElectricBreakdown(
      clientId = ClientId(2),
      onzoHouseholdId = HouseholdId("1"),
      sensorId = SensorId("I'm a sensor and I get ignored"),
      day = runDay20170101,
      breakdowns = Map.empty
    )

    val status = elecPersister.persist(data)
    withClue("no exceptions") {
      assert(status.isSuccess)
    }
    val result = session.execute(
      s"SELECT * FROM onzo.daily_electric_bill_breakdown WHERE organisation_id = 2 AND onzo_household_id = '1'"
    )

    val all = result.all().asScala
    withClue("single record written") {
      assert(all.length == 1)
    }
    val first = all.head

    val actualClientId = first.getInt("organisation_id")
    withClue("client_id == greenchoice") {
      assert(actualClientId == 2)
    }

    val actualHouseholdId = first.getString("onzo_household_id")
    withClue("household_id == 1") {
      assert(actualHouseholdId == "1")
    }

    val actualDay = first.getDate("day")
    withClue("day == 01-01-2017") {
      assert(actualDay.getYear == 2017)
      assert(actualDay.getMonth == 1)
      assert(actualDay.getDay == 1)
    }

    withClue("no breakdowns") {
      val breakdowns = first.getMap("categories", classOf[String], classOf[UDTValue])
      assert(breakdowns.isEmpty)
    }
  }

  test("persist category breakdowns") {
    val data = DailyElectricBreakdown(
      clientId = ClientId(2),
      onzoHouseholdId = HouseholdId("1"),
      sensorId = SensorId("I'm a sensor and I get ignored"),
      day = runDay20170101,
      breakdowns = Map(AlwaysOn -> ElectricEnergyBreakdown(WattHours(1), GBP(2)))
    )

    val status = elecPersister.persist(data)
    withClue("no exceptions") {
      assert(status.isSuccess)
    }

    val result = session.execute(
      s"SELECT * FROM onzo.daily_electric_bill_breakdown WHERE organisation_id = 2 AND onzo_household_id = '1'"
    )

    val all = result.all().asScala
    withClue("single record written") {
      assert(all.length == 1)
    }

    val first = all.head

    val categories = first.getMap("categories", classOf[String], classOf[UDTValue]).asScala

    withClue("breakdown length of 1") {
      assert(categories.size == 1)
    }

    // Baseload
    val alwaysOn = categories("always_on")

    withClue("baseload has correct energy") {
      val actualEnergy = alwaysOn.getDecimal("energy_wh")
      assert(actualEnergy == BigDecimal(1.0).bigDecimal)
    }

    withClue("baseload has correct cost") {
      val actualCost = alwaysOn.getDecimal("cost")
      assert(actualCost == BigDecimal(2.0).bigDecimal)
    }
  }

  override protected def afterAll(): Unit = {
    session.close()
    cluster.close()
  }
}