package com.onzo.pipeline.persistence

import java.time.LocalDate

import com.onzo.pipeline.domain.RunDay


object TestData {
  val runDay20170101 = RunDay(LocalDate.of(2017, 1, 1))
}
