package com.onzo.pipeline.persistence

import java.time.{LocalDate, ZoneId, ZonedDateTime}
import java.util.concurrent.TimeUnit

import com.datastax.driver.core.{Cluster, Session, UDTValue, UserType}
import com.onzo.common.domain.{ClientId, HouseholdId, SensorId}
import com.onzo.pipeline.domain._
import com.onzo.pipeline.persistence.TestData.runDay20170101
import jp.ne.opt.chronoscala.NamespacedImports._
import org.scalatest.{BeforeAndAfterAll, FunSuite}
import pld.data.ConsumptionCategory.{AlwaysOn, Heating}
import squants.energy.WattHours
import squants.market.GBP
import squants.space.CubicMeters

import scala.collection.JavaConverters._
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.util.Properties

class GasBreakdownPersisterSpec extends FunSuite with BeforeAndAfterAll
  with TestCluster {

  var gasPersister: GasBreakdownPersister = _
  var cluster: Cluster = _
  var session: Session = _
  var gasBreakdownUDT: UserType = _

  override protected def beforeAll(): Unit = {
    cluster = buildCluster()
    session = cluster.connect()
    gasPersister = new GasBreakdownCassandraPersister(session, "onzo")
    gasBreakdownUDT = cluster.getMetadata.getKeyspace("onzo").getUserType("gas_breakdown_commodities")
  }

  test("persist daily gas breakdown with no categories") {
    val data = DailyGasBreakdown(
      clientId = ClientId(2),
      onzoHouseholdId = HouseholdId("1"),
      sensorId = SensorId("I'm a sensor and I get ignored"),
      day = runDay20170101,
      breakdowns = Map.empty
    )

    val status = gasPersister.persist(data)
    withClue("no exceptions") {
      assert(status.isSuccess)
    }
    val result = session.execute(
      s"SELECT * FROM onzo.daily_gas_bill_breakdown WHERE organisation_id = 2 AND onzo_household_id = '1'"
    )

    val all = result.all().asScala
    withClue("single record written") {
      assert(all.length == 1)
    }
    val first = all.head

    val actualClientId = first.getInt("organisation_id")
    withClue("client_id == greenchoice") {
      assert(actualClientId == 2)
    }

    val actualHouseholdId = first.getString("onzo_household_id")
    withClue("household_id == 1") {
      assert(actualHouseholdId == "1")
    }

    val actualDay = first.getDate("day")
    withClue("day == 01-01-2017") {
      assert(actualDay.getYear == 2017)
      assert(actualDay.getMonth == 1)
      assert(actualDay.getDay == 1)
    }

    withClue("no breakdowns") {
      val breakdowns = first.getMap("categories", classOf[String], classOf[UDTValue])
      assert(breakdowns.isEmpty)
    }
  }

  test("persist category breakdowns") {
    val data = DailyGasBreakdown(
      clientId = ClientId(2),
      onzoHouseholdId = HouseholdId("1"),
      sensorId = SensorId("I'm a sensor and I get ignored"),
      day = runDay20170101,
      breakdowns = Map(
        AlwaysOn -> GasEnergyBreakdown(WattHours(1), GBP(2)),
        Heating -> GasVolumeBreakdown(CubicMeters(2), GBP(3))
      )
    )

    val status = gasPersister.persist(data)
    withClue("no exceptions") {
      assert(status.isSuccess)
    }

    val result = session.execute(
      s"SELECT * FROM onzo.daily_gas_bill_breakdown WHERE organisation_id = 2 AND onzo_household_id = '1'"
    )

    val all = result.all().asScala
    withClue("single record written") {
      assert(all.length == 1)
    }

    val first = all.head
    val categories = first.getMap("categories", classOf[String], classOf[UDTValue]).asScala

    withClue("breakdown length of 2") {
      assert(categories.size == 2)
    }

    // Baseload
    val alwaysOn = categories("always_on")

    withClue("baseload has correct energy") {
      val actualEnergy = alwaysOn.getDecimal("energy_wh")
      assert(actualEnergy == BigDecimal(1.0).bigDecimal)
    }

    withClue("baseload has correct cost") {
      val actualCost = alwaysOn.getDecimal("cost")
      assert(actualCost == BigDecimal(2.0).bigDecimal)
    }

    // Heating
    val heating = categories("heating")

    withClue("heating has correct energy") {
      val actualEnergy = heating.getDecimal("volume_cm3")
      assert(actualEnergy == BigDecimal(2.0).bigDecimal)
    }

    withClue("heating has correct cost") {
      val actualCost = heating.getDecimal("cost")
      assert(actualCost == BigDecimal(3.0).bigDecimal)
    }
  }

  override protected def afterAll(): Unit = {
    session.close()
    cluster.close()
  }
}