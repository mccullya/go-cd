package com.onzo.pipeline

import java.io.InputStream
import java.util.concurrent.Executors

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpMethods, HttpRequest}
import akka.stream.{ActorMaterializer, ActorMaterializerSettings}
import com.onzo.common.domain.{ClientId, HouseholdId}
import com.onzo.etl.persistence.csr.{CsrActor, InMemoryHouseholdApiUsersRepository}
import com.onzo.pipeline.config.ModularConfiguration
import com.onzo.pipeline.domain.HouseholdProfileEntity.GetProfileIdFn
import com.onzo.pipeline.domain.{HouseholdProfile, HouseholdProfileEntity}
import com.onzo.pipeline.persistence.{ElectricBreakdownPersister, ElectricConsumptionFindAllByUser, ElectricConsumptionPersister, GasBreakdownPersister, GasConsumptionPersister, TestCluster}
import com.onzo.pipeline.webserver.WebServer
import org.scalatest.{AsyncFunSuite, BeforeAndAfterAll}

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.Success

class WebServerSpec extends AsyncFunSuite with BeforeAndAfterAll with ExceptionLoggingDecider
  with TestCluster {

  private val executor = Executors.newSingleThreadExecutor()
  private implicit val ec = ExecutionContext.fromExecutor(executor)
  private implicit val system = ActorSystem("my-system")
  private implicit val materializer = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(decider))
  private implicit val cluster = buildCluster()
  private implicit val config = ModularConfiguration().config

  override protected def beforeAll(): Unit = {
    implicit val session = cluster.connect()
    var is: InputStream = null
    val cdl = try {
      is = this.getClass.getClassLoader.getResourceAsStream("cassandra_schema.cdl")
      scala.io.Source.fromInputStream(is).mkString.split(";")
    } finally is.close()
    cdl.foreach(session.execute)

    val electricConsumptionPersister = ElectricConsumptionPersister.create(session, "onzo")
    val electricBreakdownPersister = ElectricBreakdownPersister.create(session, "onzo")
    val gasConsumptionPersister = GasConsumptionPersister.create(session, "onzo")
    val gasBreakdownPersister = GasBreakdownPersister.create(session, "onzo")
    val householdApiUsersRepository = new InMemoryHouseholdApiUsersRepository
    val csrActorRef = CsrActorRef(system.actorOf(CsrActor.props(householdApiUsersRepository)))
    val findEmptyProfileById: GetProfileIdFn = (_: ClientId, _: HouseholdId) => HouseholdProfileEntity(ClientId(0), HouseholdId("dummy test household Id"), HouseholdProfile.empty)
    val electricConsumptionFindAllByUser = ElectricConsumptionFindAllByUser.create(session, "onzo")

    val bindingFuture = for {
      pipelineActor <- Future {
        system.actorOf(
          PipelineActor.props()(
            ec,
            electricConsumptionPersister,
            electricBreakdownPersister,
            gasConsumptionPersister,
            gasBreakdownPersister,
            householdApiUsersRepository,
            electricConsumptionFindAllByUser,
            csrActorRef,
            findEmptyProfileById,
            materializer
          ))
      }
      webServer <- Future {
        new WebServer(pipelineActor)(ec, system, materializer)
      }
      eventualBiding <- webServer.run()
    } yield eventualBiding

    bindingFuture.failed.foreach(t => throw t)
    Await.result(bindingFuture, 10 seconds)
  }

  test("ping should return pong") {
    val eventualResponse = Http().singleRequest(HttpRequest(uri = "http://localhost:8080/ping"))
    (eventualResponse andThen {
      case Success(response) => withClue("status == 200")(assert(response.status.isSuccess()))
    }).flatMap(_.entity.toStrict(3.seconds)).map { case HttpEntity.Strict(contentType, data) =>
      withClue("content type should be utf8") {
        assert(contentType.toString() == "text/html; charset=UTF-8")
      }
      withClue("body == pong") {
        assert(data.utf8String == "pong")
      }
    }
  }

  test("run for all clients should return 200") {
    val eventualResponse = Http().singleRequest(
      HttpRequest(uri = "http://localhost:8080/run/all").withMethod(HttpMethods.POST)
    )

    eventualResponse.map { response =>
      withClue("status == 200")(assert(response.status.isSuccess()))
    }
  }

  test("run for greenchoice should return 200") {
    val eventualResponse = Http().singleRequest(
      HttpRequest(uri = "http://localhost:8080/run/greenchoice").withMethod(HttpMethods.POST)
    )

    eventualResponse.map { response =>
      withClue("status == 200")(assert(response.status.isSuccess()))
    }
  }

  test("rerun PLD for a specific user should return 200") {
    val json = """{"house_type":"detached"}"""
    val organisationId = 2
    val householdId = 1
    val httpRequest = HttpRequest(uri = s"http://localhost:8080/pld/${organisationId}/${householdId}")
      .withMethod(HttpMethods.POST)
      .withEntity(ContentTypes.`application/json`, json)
    val eventualResponse = Http().singleRequest(httpRequest)
    eventualResponse.map { response =>
      withClue("status == 200")(assert(response.status.isSuccess()))
    }
  }

  test("rerun PLD for a specific user for a client id that is not an Int should return 500") {
    val json = """{"house_type":"detached"}"""
    val organisationId = "greenchoice"
    val householdId = 1
    val httpRequest = HttpRequest(uri = s"http://localhost:8080/pld/${organisationId}/${householdId}")
      .withMethod(HttpMethods.POST)
      .withEntity(ContentTypes.`application/json`, json)
    val eventualResponse = Http().singleRequest(httpRequest)
    eventualResponse.map { response =>
      withClue("status == 500")(assert(response.status.isFailure()))
    }
  }

  test("exceptions should return 500") {
    val httpRequest = HttpRequest(uri = "http://localhost:8080/fail")
    val eventualResponse = Http().singleRequest(httpRequest)
    eventualResponse.map { response =>
      withClue("status == 501")(assert(response.status.isFailure()))
    }
  }

  override protected def afterAll(): Unit = {
    cluster.close()
    executor.shutdown()
  }
}