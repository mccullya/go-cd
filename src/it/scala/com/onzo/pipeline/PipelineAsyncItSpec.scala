package com.onzo.pipeline

import java.io.InputStream
import java.util.concurrent.Executors

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, ActorMaterializerSettings}
import com.onzo.common.domain.{ClientId, HouseholdId}
import com.onzo.etl.persistence.csr.{CsrActor, InMemoryHouseholdApiUsersRepository}
import com.onzo.pipeline.Pipeline.ClientResult
import com.onzo.pipeline.config._
import com.onzo.pipeline.config.pld.PldConfigSpec.pldConfigurationBuilder
import com.onzo.pipeline.domain.HouseholdProfileEntity.GetProfileIdFn
import com.onzo.pipeline.domain.{HouseholdProfile, HouseholdProfileEntity}
import com.onzo.pipeline.persistence.{ElectricBreakdownPersister, ElectricConsumptionFindAllByUser, ElectricConsumptionPersister, GasBreakdownPersister, GasConsumptionPersister, TestCluster}
import org.scalactic.TypeCheckedTripleEquals
import org.scalatest.{AsyncFunSuite, BeforeAndAfter, BeforeAndAfterAll, Matchers}

import scala.collection.JavaConverters._
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.Properties

class PipelineAsyncItSpec extends AsyncFunSuite with BeforeAndAfter with BeforeAndAfterAll with ExceptionLoggingDecider
  with TestCluster with Matchers with TypeCheckedTripleEquals {

  private implicit val system = ActorSystem("EndToEndSpec")
  private implicit val config = ModularConfiguration().config
  private implicit val mat = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(decider))
  // Multi threaded tests are notoriously difficult and non deterministic
  private val executor = Executors.newSingleThreadScheduledExecutor()
  private implicit val ec: ExecutionContext = ExecutionContext.fromExecutor(executor)
  private val cluster = buildCluster()
  private implicit val session = cluster.connect()
  private val s3TestUtils = new S3TestUtils()
  private val GreenchoiceCsrFolder = "2/csr/2017-05-01"
  private val GreenchoiceCsrFile = "gc-csr-1_mapped.csv"
  private val GreenchoiceElecFolder = "2/consumption/electricity/2017-05-01"
  private val GreenchoiceElecFile = "gc-electric-1_mapped.csv"
  private val GreenchoiceGasFolder = "2/consumption/gas/2017-05-01"
  private val GreenchoiceGasFile = "gc-gas-1_mapped.csv"

  override protected def beforeAll(): Unit = {
    var is: InputStream = null
    val cdl = try {
      is = this.getClass.getClassLoader.getResourceAsStream("cassandra_schema.cdl")
      scala.io.Source.fromInputStream(is).mkString.split(";")
    } finally is.close()
    cdl.foreach(session.execute)
  }

  test("Run for a specific client") {
    val gcConfig = ClientConfig(
      id = 2,
      name = "greenchoice",
      timestampPattern = "yyyy-MM-dd'T'HH:mm:ssZ",
      columnMappings = ClientColumnMappings(
        skipNumberOfRows = 1,
        elecConsumption = ElecConsumptionColumnMapping(
          sensorId = "A",
          timestamp = "B",
          cumulativeEnergy = "C"
        ),
        gasConsumption = GasConsumptionColumnMapping(
          sensorId = "A",
          timestamp = "B",
          cumulativeVolume = "C"
        ),
        electricCsr = Some(ElectricCsrColumnMapping(
          householdId = "A",
          status = "B",
          sensorId = "F",
          standingCharge = "G",
          unitRate = "H",
          tags = "O",
          zipPostalCode = Some("P"),
          latitude = None,
          longitude = None
        )),
        gasCsr = Some(GasCsrColumnMapping(
          householdId = "A",
          status = "C",
          sensorId = "J",
          standingCharge = "K",
          unitRate = "L",
          calorificValue = "N",
          tags = "O",
          zipPostalCode = Some("P"),
          latitude = None,
          longitude = None
        ))
      ),
      pldConfigurationBuilder
    )

    val electricConsumptionPersister = ElectricConsumptionPersister.create(session, "onzo")
    val electricBreakdownPersister = ElectricBreakdownPersister.create(session, "onzo")
    val gasConsumptionPersister = GasConsumptionPersister.create(session, "onzo")
    val gasBreakdownPersister = GasBreakdownPersister.create(session, "onzo")
    val householdApiUsersRepository = new InMemoryHouseholdApiUsersRepository
    val electricConsumptionFindAllByUser = ElectricConsumptionFindAllByUser.create(session, "onzo")
    val csrActorRef = CsrActorRef(system.actorOf(CsrActor.props(householdApiUsersRepository)))
    val findEmptyProfileById: GetProfileIdFn = (clientId: ClientId, householdId: HouseholdId) => {
      HouseholdProfileEntity(clientId, householdId, HouseholdProfile.empty)
    }

    for {
      pipeline <- Future {
        new Pipeline()(
          system,
          config,
          electricConsumptionPersister,
          electricBreakdownPersister,
          gasConsumptionPersister,
          gasBreakdownPersister,
          electricConsumptionFindAllByUser,
          householdApiUsersRepository,
          csrActorRef,
          findEmptyProfileById,
          mat,
          ec
        )
      }
      clientResult <- pipeline.runClient(gcConfig)
    } yield {

      withClue("client result should be correct") {
        assert(clientResult === ClientResult("greenchoice", 2, 1, 1))
      }

      withClue("electric consumption should be written") {
        val rows = session.execute("SELECT * FROM consumption_intraday_elec_by_household").all().asScala
        assert(rows.length === 1)
      }

      withClue("gas consumption should be written") {
        val rows = session.execute("SELECT * FROM consumption_intraday_gas_by_household").all().asScala
        assert(rows.length === 1)
      }

      withClue("electric breakdown should be written") {
        val rows = session.execute("SELECT * FROM daily_electric_bill_breakdown").all().asScala
        assert(rows.length === 1)
      }

      withClue("gas breakdown should be written") {
        val rows = session.execute("SELECT * FROM daily_gas_bill_breakdown").all().asScala
        assert(rows.length === 1)
      }

      withClue("service metrics should be written;") {
        val rows = session.execute("SELECT * from onzo.service_metrics").all().asScala
        withClue("two records should be written;") {
          assert(rows.length === 2)
        }
        withClue("both should be Greenchoice;") {
          assert(rows.count { row => row.getInt("organisation_id") == 2 } === 2)
        }
        withClue("both should be that one house;") {
          assert(rows.count { row => row.getString("onzo_household_id") == "55" } === 2)
        }
        withClue("one for electric meter and one for gas meter;") {
          assert(rows.map { row => row.getString("onzo_device_id") }.toSet === Set("1", "2"))
        }
        withClue("both should indicate when consumption output was produced;") {
          assert(rows.count { row => row.getString("when_consumption_output_produced") != null } === 2)
        }
        withClue("both should indicate when breakdown output was produced;") {
          assert(rows.count { row => row.getString("when_breakdown_output_produced") != null } === 2)
        }
      }
    }
  }

  /* est("Run for all clients") {
    val electricConsumptionPersister = ElectricConsumptionPersister.create(session, "onzo")
    val electricBreakdownPersister = ElectricBreakdownPersister.create(session, "onzo")
    val gasConsumptionPersister = GasConsumptionPersister.create(session, "onzo")
    val gasBreakdownPersister = GasBreakdownPersister.create(session, "onzo")
    val householdApiUsersRepository = new InMemoryHouseholdApiUsersRepository
    val findProfileById = (_: ClientId, _: HouseholdId) => HouseholdProfile.empty

    for {
      pipeline <- Future {
        new Pipeline()(
          system,
          config,
          electricConsumptionPersister,
          electricBreakdownPersister,
          gasConsumptionPersister,
          gasBreakdownPersister,
          householdApiUsersRepository,
          findProfileById,
          mat,
          ec
        )
      }
      result <- pipeline.runClients()
    } yield {
      // first client is onzo
      withClue("should get two client outputs") {
        assert(result.size == 2)
      }
      withClue("should output for greenchoice") {
        assert(result.toList(1).name == "greenchoice")
      }
      withClue("should have 1 successes for greenchoice") {
        assert(result.toList(1).electricCount == 1)
      }
    }
  }*/

  before {
    val f = for {
      // Csr
      _ <- Future { logInfo(s"Creating S3 folder $GreenchoiceCsrFolder in bucket $s3Bucket ...") }
      _ <- s3TestUtils.createFolder(s3Bucket, GreenchoiceCsrFolder)
      _ <- Future { logInfo(s"Uploading $GreenchoiceCsrFile to $GreenchoiceCsrFolder ...") }
      _ <- s3TestUtils.putFile(GreenchoiceCsrFile, s3Bucket, s"$GreenchoiceCsrFolder/$GreenchoiceCsrFile")
      // Elec
      _ <- Future { logInfo(s"Creating S3 folder $GreenchoiceElecFolder in bucket $s3Bucket ...") }
      _ <- s3TestUtils.createFolder(s3Bucket, GreenchoiceElecFolder)
      _ <- Future { logInfo(s"Uploading $GreenchoiceElecFile to $GreenchoiceElecFolder ...") }
      _ <- s3TestUtils.putFile(GreenchoiceElecFile, s3Bucket, s"$GreenchoiceElecFolder/$GreenchoiceElecFile")
      // Gas
      _ <- Future { logInfo(s"Creating S3 folder $GreenchoiceGasFolder in bucket $s3Bucket ...") }
      _ <- s3TestUtils.createFolder(s3Bucket, GreenchoiceGasFolder)
      _ <- Future { logInfo(s"Uploading $GreenchoiceGasFile to $GreenchoiceGasFolder ...") }
      _ <- s3TestUtils.putFile(GreenchoiceGasFile, s3Bucket, s"$GreenchoiceGasFolder/$GreenchoiceGasFile")
    } yield ()
    Await.result[Unit](f, 30 seconds)
  }

  override protected def afterAll(): Unit = {
    mat.shutdown()
    system.terminate()
    executor.shutdown()
    session.close()
    cluster.close()
  }

  private def s3Bucket: String = Properties.envOrElse("S3_BUCKET", "onzo-playground")  // onzo-atlas-test-(a|b|c)
}