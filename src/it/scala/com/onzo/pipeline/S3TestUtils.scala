package com.onzo.pipeline

import java.io.{ByteArrayInputStream, File}

import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.{ObjectMetadata, PutObjectRequest}

import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Future}

class S3TestUtils {

  private val s3client = AmazonS3ClientBuilder.defaultClient()

  def createFolder(bucketName: String, folderName: String)(implicit ec: ExecutionContext): Future[Unit] = Future {
    // create meta-data for your folder and set content-length to 0
    val metadata = new ObjectMetadata()
    metadata.setContentLength(0)
    // create empty content
    val emptyContent = new ByteArrayInputStream(Array.empty[Byte])
    // create a PutObjectRequest passing the folder name suffixed by /
    val putObjectRequest = new PutObjectRequest(bucketName, folderName + "/", emptyContent, metadata)
    // send request to S3 to create folder
    s3client.putObject(putObjectRequest)
  }

  def putFile(localPath: String, bucketName: String, key: String)(implicit ec: ExecutionContext): Future[Unit] = {
    val resource = this.getClass.getClassLoader.getResource(localPath)
    val file = new File(resource.toURI)
    putFile(file, bucketName, key)
  }

  def putFile(file: File, bucketName: String, key: String)(implicit ec: ExecutionContext): Future[Unit] = Future {
    s3client.putObject(new PutObjectRequest(bucketName, key, file))
  }

  def deleteFolder(bucketName: String, path: String)(implicit ec: ExecutionContext): Future[Unit] = Future {
    val fileList = s3client.listObjects(bucketName, path).getObjectSummaries.asScala
    fileList.map(_.getKey).foreach(s3client.deleteObject(bucketName, _))
    s3client.deleteObject(bucketName, path)
  }

}
