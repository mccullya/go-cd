package com.onzo.pipeline.utils

import java.time.{ZoneId, ZonedDateTime}

import com.onzo.collector.domain.ElectricSensorReading
import com.onzo.collector.domain.ElectricSensorReading.CumulativeEnergy
import com.onzo.common.domain.{ClientId, HouseholdId, OnzoDeviceId, SensorId}
import com.onzo.pipeline.domain._
import squants.energy.{WattHours, Watts}
import squants.market.GBP

import scala.util.Random

object IntervalGenerator {

  def generateRecords(numSensors: Int,
                      numDays: Int,
                      recordGenerator: (SensorId, ZonedDateTime) => ElectricSensorReading): Iterator[CumulativeEnergy] = {
    val startDate = ZonedDateTime.of(2017, 1, 1, 0, 0, 0, 0, ZoneId.of("UTC"))
    for {
      dayNum <- (0 until numDays).toIterator
      sensorId <- (0 until numSensors).toIterator
      interval <- (0 until 96).toIterator
    } yield {
      val day = startDate.plusDays(dayNum)
      val ts = day.plusMinutes(15 * interval)
      val consumption = Random.nextInt(100)
      CumulativeEnergy(SensorId(sensorId.toString), ts, consumption)
    }
  }

  def generateGoodReading(sensorId: SensorId, timestamp: ZonedDateTime): CumulativeEnergy = {
    CumulativeEnergy(sensorId, timestamp, Random.nextInt(100))
  }

  def generateWindowedElectric(numSensors: Int, numDays: Int): Iterator[WindowedElectricConsumption] = {
    val startDate = ZonedDateTime.of(2017, 1, 1, 0, 0, 0, 0, ZoneId.of("UTC"))
    for {
      dayNum <- (0 until numDays).toIterator
      sensorId <- (0 until numSensors).toIterator.map(_.toString)
    } yield {
      val day = startDate.plusDays(dayNum)
      val intervals = (0 until 96).map { interval =>
        val rawTs = startDate.plusMinutes(15 * interval)
        val consumption = Random.nextInt(100)
        (rawTs, ElectricConsumption(WattHours(consumption), Watts(consumption), GBP(consumption)))
      }.toMap

      WindowedElectricConsumption(
        ClientId(2),
        HouseholdId(sensorId),
        OnzoDeviceId(sensorId),
        RunDay(day.toLocalDate),
        Resolution.Min15,
        ZoneId.of("Europe/London"),
        WattHours(1),
        Watts(1),
        GBP(1),
        intervals
      )
    }
  }
}