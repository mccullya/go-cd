package com.onzo.pipeline.utils

import akka.actor.{Actor, ActorSystem, Props}

class TestActor(startMessage: String) extends Actor {

  override def preStart(): Unit = println(startMessage)

  override def postRestart(reason: Throwable): Unit = {
    super.postRestart(reason)
    reason.printStackTrace()
  }

  override def receive: Receive = {
    case _ => println("got message")
  }

}

object TestActor {
  def props(): Props = Props(classOf[TestActor], "started")
}

object ConsumptionGenerator {
  def main(args: Array[String]): Unit = {
    import scala.concurrent.ExecutionContext.Implicits.global
    val system = ActorSystem()
    system.actorOf(Props(classOf[TestActor], "started"))
    Thread.sleep(1000)
    system.terminate().onComplete(_ => System.exit(0))
  }
}
