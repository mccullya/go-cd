package com.onzo.pipeline.utils

import java.io.{File, PrintWriter}

/**
  * Created by toby.hobson on 13/04/2017.
  */
object CsrGenerator {

  def main(args: Array[String]): Unit = {
    val out = new PrintWriter(new File("gc-40k-csr.csv"))
    (0 until 40000).foreach { sensorId =>
      out.println(s"$sensorId,Live,Live,EUR,MAJOR,$sensorId,0,0.1875,KWH,$sensorId,0,0.6136,M3,1,Customer_Type,1221LB,NULL,NULL")
    }
    out.close()
  }

}
