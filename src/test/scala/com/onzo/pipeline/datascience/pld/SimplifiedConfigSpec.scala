package com.onzo.pipeline.datascience.pld

import java.time.{LocalDate, ZoneOffset, ZonedDateTime}

import cats.data.NonEmptyList
import com.onzo.common.Commodity.Electric
import com.onzo.common.{HouseholdProfile, Location, Place, ProfileType}
import com.onzo.common.ProfileComponent._
import com.onzo.pipeline.datascience.pld.config.SimplifiedConfig._
import com.onzo.pipeline.datascience.pld.SimplifiedConfigSpec._
import com.onzo.pipeline.domain.RunDay
import com.onzo.pipeline.TestData._
import datatypes.{Error => PldAdtError}
import jp.ne.opt.chronoscala.NamespacedImports._
import org.apache.commons.logging.LogFactory
import org.scalactic.TypeCheckedTripleEquals
import org.scalatest.{Matchers, WordSpec}
import org.slf4j.{Logger, LoggerFactory}
import pld.PipelineAdapter
import pld.PipelineInterface.{EnergyReading, PLDCategoryParameters}
import pld.data.ConsumptionCategory
import pld.data.ConsumptionCategory._
import pld.data.output.PldTopDownBreakdown.{AbsoluteAmount, AbsoluteBreakdown, TopDownSummaryAbsolute}
import time.Time._
import squants.energy.{KilowattHours, WattHours}
import squants.space.Degrees
import util.catslocal.RichNonEmptyList._

class SimplifiedConfigSpec extends WordSpec with Matchers with TypeCheckedTripleEquals {

  "a pld" when {
    /**
      * 1. Baseload Test (Simplified/Winter/elec/elec)
      */
    "processing Christmas base load only with electric profile" should {
      val runDay = runDay20161225
      val householdProfile = HouseholdProfile(Set(ElectricHeating, ElectricHotWater))
      val baseLoad = baseLoadOnly(runDay)
      val profileType = ProfileType(Electric)
      "successfully provide the electric base load breakdown" in {
        val r = for {
          electricCategoryParameters <- pldConfigurationBuilder.generateParameters(runDay, sensorLocation, householdProfile, profileType)
          result <- PipelineAdapter.run(electricCategoryParameters, pldConfigurationBuilder.redistributionCap, baseLoad)
        } yield result
        val expected: AbsoluteBreakdown = Map(AlwaysOn -> AbsoluteAmount(2244.0),
          Refrigeration -> AbsoluteAmount(396.0),
          Cooking -> AbsoluteAmount(0.0),
          Lighting -> AbsoluteAmount(0.0),
          LaundryDishwashing -> AbsoluteAmount(0.0),
          HomeEntertainment -> AbsoluteAmount(0.0),
          Other -> AbsoluteAmount(0.0),
          Heating -> AbsoluteAmount(0.0),
          WaterHeating -> AbsoluteAmount(0.0))
        r should === (Right(TopDownSummaryAbsolute(expected)))
      }
    }
    /**
      * 2. Baseload Test (Simplified/Summer/Elec/Elec)
      */
    "processing summer base load only with electric profile" should {
      val runDay = runDay20160625
      val householdProfile = HouseholdProfile(Set(ElectricHeating, ElectricHotWater))
      val baseLoad = baseLoadOnly(runDay)
      val profileType = ProfileType(Electric)
      "successfully provide the electric base load breakdown" in {
        val profileType = ProfileType(Electric)
        val r = for {
          electricCategoryParameters <- pldConfigurationBuilder.generateParameters(runDay, sensorLocation, householdProfile, profileType)
          result <- PipelineAdapter.run(electricCategoryParameters, pldConfigurationBuilder.redistributionCap, baseLoad)
        } yield result
        val expected: AbsoluteBreakdown = Map(AlwaysOn -> AbsoluteAmount(2244.0),
          Refrigeration -> AbsoluteAmount(396.0),
          Cooking -> AbsoluteAmount(0.0),
          Lighting -> AbsoluteAmount(0.0),
          LaundryDishwashing -> AbsoluteAmount(0.0),
          HomeEntertainment -> AbsoluteAmount(0.0),
          Other -> AbsoluteAmount(0.0),
          Heating -> AbsoluteAmount(0.0),
          WaterHeating -> AbsoluteAmount(0.0))
        r should === (Right(TopDownSummaryAbsolute(expected)))
      }
    }
    /**
      * 3. Baseload Test (Simplified/Summer/NoElec/NoElec)
      */
    "processing summer base load only no electric profile" should {
      val runDay = runDay20160625
      val householdProfile = HouseholdProfile(Set())
      val baseLoad = baseLoadOnly(runDay)
      val profileType = ProfileType(Electric)
      "successfully provide the electric base load breakdown" in {
        val profileType = ProfileType(Electric)
        val r = for {
          electricCategoryParameters <- pldConfigurationBuilder.generateParameters(runDay, sensorLocation, householdProfile, profileType)
          result <- PipelineAdapter.run(electricCategoryParameters, pldConfigurationBuilder.redistributionCap, baseLoad)
        } yield result
        val expected: AbsoluteBreakdown = Map(AlwaysOn -> AbsoluteAmount(2244.0),
          Refrigeration -> AbsoluteAmount(396.0),
          Cooking -> AbsoluteAmount(0.0),
          Lighting -> AbsoluteAmount(0.0),
          LaundryDishwashing -> AbsoluteAmount(0.0),
          HomeEntertainment -> AbsoluteAmount(0.0),
          Other -> AbsoluteAmount(0.0),
          Heating -> AbsoluteAmount(0.0),
          WaterHeating -> AbsoluteAmount(0.0))
        r should === (Right(TopDownSummaryAbsolute(expected)))
      }
    }
    /**
      * 5.  Gappy Baseload (Simplified/Winter/NoElec/NoElec)
      *     This test is a bit to see what Atlas does at the moment to handle gaps – so I might change the expected output depending on what it does.
      */
    "processing gappy readings" should {
      val householdProfile = HouseholdProfile(Set())
      val runDay = runDay20160625
      val baseLoad = gappyBaseLoadOnly20161225
      val profileType = ProfileType(Electric)
      "refuse to produce a breakdown" in {
        val r = for {
          electricCategoryParameters <- pldConfigurationBuilder.generateParameters(runDay, sensorLocation, householdProfile, profileType)
          result <- PipelineAdapter.run(electricCategoryParameters, pldConfigurationBuilder.redistributionCap, baseLoad)
        } yield result
        r should === (Left(PldAdtError("Pipeline adapter pld run failure : prefix uneven resolution in consumption data)", None)))
      }
    }
  }
}

object SimplifiedConfigSpec {
  implicit val logger: Logger = LoggerFactory.getLogger("SimplifiedConfigSpec")

  // note that the data from date science starts as 00:30 and finished at 00:00 inclusive so spans 2 days
  // as this is apparently what is output on classic
  val runDay20160625 = RunDay(LocalDate.of(2016, 6, 25))

  def baseLoadOnly(runDay: RunDay): NonEmptyList[EnergyReading] = {
    val n = 48
    val readings = List.fill(n)(WattHours(55.0))

    val times = halfHourStream(runDay.underlying.atStartOfDay(ZoneOffset.UTC)).take(n).toList
    val all = times zip readings map { a => EnergyReading(a._1, a._2) }
    NonEmptyList.fromListUnsafe(all)
  }

  val sensorLocation: Location = Location(Place("OnzoHQ"), north = Degrees(51.519570), east = Degrees(-0.168398))

  val gappyBaseLoadOnly20161225: NonEmptyList[EnergyReading] = {
    val gappyTime = List(ZonedDateTime.parse("2016-12-25T00:30Z"),
      ZonedDateTime.parse("2016-12-25T01:00Z"),
      ZonedDateTime.parse("2016-12-25T01:30Z"),
      ZonedDateTime.parse("2016-12-25T02:00Z"),
      ZonedDateTime.parse("2016-12-25T02:30Z"),
      ZonedDateTime.parse("2016-12-25T03:00Z"),
      ZonedDateTime.parse("2016-12-25T03:30Z"),
      ZonedDateTime.parse("2016-12-25T08:00Z"),
      ZonedDateTime.parse("2016-12-25T08:30Z"),
      ZonedDateTime.parse("2016-12-25T09:00Z"),
      ZonedDateTime.parse("2016-12-25T09:30Z"),
      ZonedDateTime.parse("2016-12-25T10:00Z"),
      ZonedDateTime.parse("2016-12-25T10:30Z"),
      ZonedDateTime.parse("2016-12-25T11:00Z"),
      ZonedDateTime.parse("2016-12-25T11:30Z"),
      ZonedDateTime.parse("2016-12-25T12:00Z"),
      ZonedDateTime.parse("2016-12-25T12:30Z"),
      ZonedDateTime.parse("2016-12-25T13:00Z"),
      ZonedDateTime.parse("2016-12-25T13:30Z"),
      ZonedDateTime.parse("2016-12-25T14:00Z"),
      ZonedDateTime.parse("2016-12-25T18:30Z"),
      ZonedDateTime.parse("2016-12-25T19:00Z"),
      ZonedDateTime.parse("2016-12-25T19:30Z"),
      ZonedDateTime.parse("2016-12-25T20:00Z"),
      ZonedDateTime.parse("2016-12-25T20:30Z"),
      ZonedDateTime.parse("2016-12-25T21:00Z"),
      ZonedDateTime.parse("2016-12-25T21:30Z"),
      ZonedDateTime.parse("2016-12-25T22:00Z"),
      ZonedDateTime.parse("2016-12-25T22:30Z"),
      ZonedDateTime.parse("2016-12-26T00:00Z"))
    val readings = gappyTime zip List.fill(gappyTime.length)(WattHours(55.0)) map { a => EnergyReading(a._1, a._2) }
    NonEmptyList.fromListUnsafe(readings)
  }
}
