package com.onzo.pipeline.datascience.pld

import java.time.ZoneOffset

import cats.data.NonEmptyList
import com.onzo.common.Commodity.Electric
import com.onzo.common.{HouseholdProfile, Location, Place, ProfileType}
import com.onzo.common.ProfileComponent._
import com.onzo.pipeline.datascience.pld.config.SimplifiedConfig._
import com.onzo.pipeline.datascience.pld.SimplifiedConfigSpec._
import com.onzo.pipeline.datascience.pld.config.ProductionPldConfig.prodPldConfigurationBuilder
import com.onzo.pipeline.domain.RunDay
import com.onzo.pipeline.TestData._
import datatypes.{Error => PldAdtError}
import jp.ne.opt.chronoscala.NamespacedImports._
import org.apache.commons.logging.LogFactory
import org.scalactic.TypeCheckedTripleEquals
import org.scalatest.{Matchers, WordSpec}
import org.slf4j.{Logger, LoggerFactory}
import pld.PipelineAdapter
import pld.PipelineInterface.{EnergyReading, PLDCategoryParameters}
import pld.data.ConsumptionCategory
import pld.data.ConsumptionCategory._
import pld.data.output.PldTopDownBreakdown.{AbsoluteAmount, AbsoluteBreakdown, TopDownSummaryAbsolute}
import time.Time._
import squants.energy.{KilowattHours, WattHours}
import squants.space.Degrees
import util.catslocal.RichNonEmptyList._

class ProductionConfigSpec extends WordSpec with Matchers with TypeCheckedTripleEquals {

  "a pld" when {
    import com.onzo.pipeline.datascience.pld.config.ProductionPldConfig._
    val region = "pld.europe"
    val profileType = ProfileType(Electric)
    s"processing the current production file for region $region and $profileType" should {
      "run a base load breakdown" in {
        val householdProfile = HouseholdProfile(Set(ElectricHeating, ElectricHotWater))
        val prodBuilder = prodPldConfigurationBuilder(region)
        val r = for {
          electricCategoryParameters <- prodBuilder.generateParameters(runDay20161225, sensorLocation, householdProfile, profileType)
          result <- PipelineAdapter.run(electricCategoryParameters, prodBuilder.redistributionCap, baseLoadOnly(runDay20161225))
        } yield result
        r shouldBe a[Right[_,_]]
      }
    }
  }
}

object ProductionConfigSpec {
  implicit val logger: Logger = LoggerFactory.getLogger(this.getClass.getName)

  // note that the data from date science starts as 00:30 and finished at 00:00 inclusive so spans 2 days
  // as this is apparently what is output on classic

  val baseLoadOnly20161225: NonEmptyList[EnergyReading] = {
    val n = 48
    val readings = List.fill(n)(WattHours(55.0))

    val times = halfHourStream(runDay20161225.underlying.atStartOfDay(ZoneOffset.UTC)).take(n).toList
    val all = times zip readings map { a => EnergyReading(a._1, a._2) }
    NonEmptyList.fromListUnsafe(all)
  }

  val sensorLocation: Location = Location(Place("OnzoHQ"), north = Degrees(51.519570), east = Degrees(-0.168398))
}
