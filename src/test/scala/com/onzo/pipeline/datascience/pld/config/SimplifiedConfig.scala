package com.onzo.pipeline.datascience.pld.config

import com.onzo.common.Location
import com.onzo.pipeline.config.pld.{CategoryLimitFromConfig, PldConfigurationBuilder, SeasonalAdjustmentFromConfig, TimeOfDayLoadFromConfig}
import com.onzo.pipeline.config.pld.SeasonalAdjustmentFromConfig._
import com.onzo.pipeline.config.pld.TimeOfDayLoadFromConfig._
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.config.ConfigFactory.parseString
import pld.data.input.SeasonalAdjustment.SeasonalCoefficient
import pld.PipelineInterface.TimeOfDayCoefficient
import pureconfig.loadConfig
import pureconfig.module.squants._

/**
  * Simplified Config from Abi Peters
  */
object SimplifiedConfig {

  /**
    * electricity_simplified_activity_min_max_loads.csv
    *
    * category,baseload,min_total_absolute,max_total_absolute,max_total_relative,max_total_percentile,max_category_absolute,max_category_relative
      always_on,85,,,,,0,
      refrigeration,15,,,,,,
      cooking,0,,,,,,
      lighting,0,,,,,,
      laundry_dishwashing,0,,,,,,
      home_entertainment,0,,,,,,
      other,0,,,,,,
      heating,0,,,,,,
      water_heating,0,,,,,,
    */
  val electricitySimplifiedActivityMinMaxLoads: Config = parseString(
    """
      |pld.categoryLimits {
      |  commoditylimit: [
      |    {
      |      profiletype: {commodity: {type: electric}},
      |      contribution: [
      |        {
      |          category: {type: alwayson},
      |          limit: [
      |            {type: baseloadpercent, v: 85},
      |            {type: maxcategoryabsolutecapraw, v: 0}
      |          ]
      |        },
      |        {
      |          category: {type: refrigeration},
      |          limit: [
      |            {type: baseloadpercent, v: 15}
      |          ]
      |        },
      |        {
      |          category: {type: cooking},
      |          limit: [
      |            {type: baseloadpercent, v: 0}
      |          ]
      |        },
      |        {
      |          category: {type: lighting},
      |          limit: [
      |            {type: baseloadpercent, v: 0}
      |          ]
      |        },
      |        {
      |          category: {type: laundrydishwashing},
      |          limit: [
      |            {type: baseloadpercent, v: 0}
      |          ]
      |        },
      |        {
      |          category: {type: homeentertainment},
      |          limit: [
      |            {type: baseloadpercent, v: 0}
      |          ]
      |        },
      |        {
      |          category: {type: other},
      |          limit: [
      |            {type: baseloadpercent, v: 0}
      |          ]
      |        },
      |        {
      |          category: {type: heating},
      |          limit: [
      |            {type: baseloadpercent, v: 0}
      |          ]
      |        },
      |        {
      |          category: {type: waterheating},
      |          limit: [
      |            {type: baseloadpercent, v: 0}
      |          ]
      |        }
      |      ]
      |    }
      |  ]
      |}""".stripMargin)

  val categoryLimitConfigLoaderA: CategoryLimitFromConfig = CategoryLimitFromConfig(electricitySimplifiedActivityMinMaxLoads)


  /**
    * electricity_simplified_activity_seasonal_adjustments.csv
    *
    * category,1,2,3,4,5,6,7,8,9,10,11,12
      always_on,1,1,1,1,1,1,1,1,1,1,1,1
      refrigeration,1,1,1,1,1,1,1,1,1,1,1,1
      cooking,1,1,1,1,0.5,0.5,0.5,0.5,1,1,1.5,1.5
      lighting,1.25,1.2,1,1,0.9,0.8,0.75,0.8,0.9,1,1,1.2
      laundry_dishwashing,1,1,1,1,1,1,1,1,1,1,1,1
      home_entertainment,1,1,1,1,1,1,1,1,1,1,1,1
      other,1,1,1,1,1,1,1,1,1,1,1,1
      heating,2.5,2,1,0,0,0,0,0,0,2,2.5,2.5
      water_heating,1.25,1,1,1,1,0.75,0.7,0.7,0.75,1,1.25,1.25
    */

  val electricitySimplifiedActivitySeasonalAdjustments: Config = parseString(
    """ pld.seasonalAdjustment {
      |  locationcoefficients: [
      |    {
      |      location: {
      |        place: { name: OnzoHQ },
      |        north: 51.519570°,
      |        east: -0.168398°
      |      },
      |      profiletype: {commodity: {type: electric}},
      |      coefficients: [
      |         {
      |           month : { type: january },
      |           coefficient:
      |             [
      |               { category: { type: alwayson },           coefficient : { value: 1 } },
      |               { category: { type: refrigeration },      coefficient : { value: 1 } },
      |               { category: { type: cooking },            coefficient : { value: 1 } },
      |               { category: { type: lighting },           coefficient : { value: 1.25 } },
      |               { category: { type: laundrydishwashing }, coefficient : { value: 1 } },
      |               { category: { type: homeentertainment },  coefficient : { value: 1 } },
      |               { category: { type: other },              coefficient : { value: 1 } },
      |               { category: { type: heating },            coefficient : { value: 2.5 } },
      |               { category: { type: waterheating },       coefficient : { value: 1.25 } }
      |             ]
      |         },{
      |           month : { type: february },
      |           coefficient:
      |             [
      |               { category: { type: alwayson },           coefficient : { value: 1 } },
      |               { category: { type: refrigeration },      coefficient : { value: 1 } },
      |               { category: { type: cooking },            coefficient : { value: 1 } },
      |               { category: { type: lighting },           coefficient : { value: 1.2 } },
      |               { category: { type: laundrydishwashing }, coefficient : { value: 1 } },
      |               { category: { type: homeentertainment },  coefficient : { value: 1 } },
      |               { category: { type: other },              coefficient : { value: 1 } },
      |               { category: { type: heating },            coefficient : { value: 2 } },
      |               { category: { type: waterheating },       coefficient : { value: 1 } }
      |             ]
      |         },{
      |           month : { type: march },
      |           coefficient:
      |             [
      |               { category: { type: alwayson },           coefficient : { value: 1 } },
      |               { category: { type: refrigeration },      coefficient : { value: 1 } },
      |               { category: { type: cooking },            coefficient : { value: 1 } },
      |               { category: { type: lighting },           coefficient : { value: 1 } },
      |               { category: { type: laundrydishwashing }, coefficient : { value: 1 } },
      |               { category: { type: homeentertainment },  coefficient : { value: 1 } },
      |               { category: { type: other },              coefficient : { value: 1 } },
      |               { category: { type: heating },            coefficient : { value: 1 } },
      |               { category: { type: waterheating },       coefficient : { value: 1 } }
      |             ]
      |         },{
      |           month : { type: april },
      |           coefficient:
      |             [
      |               { category: { type: alwayson },           coefficient : { value: 1 } },
      |               { category: { type: refrigeration },      coefficient : { value: 1 } },
      |               { category: { type: cooking },            coefficient : { value: 1 } },
      |               { category: { type: lighting },           coefficient : { value: 1 } },
      |               { category: { type: laundrydishwashing }, coefficient : { value: 1 } },
      |               { category: { type: homeentertainment },  coefficient : { value: 1 } },
      |               { category: { type: other },              coefficient : { value: 1 } },
      |               { category: { type: heating },            coefficient : { value: 0 } },
      |               { category: { type: waterheating },       coefficient : { value: 1 } }
      |             ]
      |         },{
      |           month : { type: may },
      |           coefficient:
      |             [
      |               { category: { type: alwayson },           coefficient : { value: 1 } },
      |               { category: { type: refrigeration },      coefficient : { value: 1 } },
      |               { category: { type: cooking },            coefficient : { value: 0.5 } },
      |               { category: { type: lighting },           coefficient : { value: 0.9 } },
      |               { category: { type: laundrydishwashing }, coefficient : { value: 1 } },
      |               { category: { type: homeentertainment },  coefficient : { value: 1 } },
      |               { category: { type: other },              coefficient : { value: 1 } },
      |               { category: { type: heating },            coefficient : { value: 0 } },
      |               { category: { type: waterheating },       coefficient : { value: 1 } }
      |             ]
      |         },{
      |           month : { type: june },
      |           coefficient:
      |             [
      |               { category: { type: alwayson },           coefficient : { value: 1 } },
      |               { category: { type: refrigeration },      coefficient : { value: 1 } },
      |               { category: { type: cooking },            coefficient : { value: 0.5 } },
      |               { category: { type: lighting },           coefficient : { value: 0.8 } },
      |               { category: { type: laundrydishwashing }, coefficient : { value: 1 } },
      |               { category: { type: homeentertainment },  coefficient : { value: 1 } },
      |               { category: { type: other },              coefficient : { value: 1 } },
      |               { category: { type: heating },            coefficient : { value: 0 } },
      |               { category: { type: waterheating },       coefficient : { value: 0.75 } }
      |             ]
      |         },{
      |           month : { type: july },
      |           coefficient:
      |             [
      |               { category: { type: alwayson },           coefficient : { value: 1 } },
      |               { category: { type: refrigeration },      coefficient : { value: 1 } },
      |               { category: { type: cooking },            coefficient : { value: 0.5 } },
      |               { category: { type: lighting },           coefficient : { value: 0.75 } },
      |               { category: { type: laundrydishwashing }, coefficient : { value: 1 } },
      |               { category: { type: homeentertainment },  coefficient : { value: 1 } },
      |               { category: { type: other },              coefficient : { value: 1 } },
      |               { category: { type: heating },            coefficient : { value: 0 } },
      |               { category: { type: waterheating },       coefficient : { value: 0.7 } }
      |             ]
      |         },{
      |           month : { type: august },
      |           coefficient:
      |             [
      |               { category: { type: alwayson },           coefficient : { value: 1 } },
      |               { category: { type: refrigeration },      coefficient : { value: 1 } },
      |               { category: { type: cooking },            coefficient : { value: 0.5 } },
      |               { category: { type: lighting },           coefficient : { value: 0.8 } },
      |               { category: { type: laundrydishwashing }, coefficient : { value: 1 } },
      |               { category: { type: homeentertainment },  coefficient : { value: 1 } },
      |               { category: { type: other },              coefficient : { value: 1 } },
      |               { category: { type: heating },            coefficient : { value: 0 } },
      |               { category: { type: waterheating },       coefficient : { value: 0.7 } }
      |             ]
      |         },{
      |           month : { type: september },
      |           coefficient:
      |             [
      |               { category: { type: alwayson },           coefficient : { value: 1 } },
      |               { category: { type: refrigeration },      coefficient : { value: 1 } },
      |               { category: { type: cooking },            coefficient : { value: 1 } },
      |               { category: { type: lighting },           coefficient : { value: 0.9 } },
      |               { category: { type: laundrydishwashing }, coefficient : { value: 1 } },
      |               { category: { type: homeentertainment },  coefficient : { value: 1 } },
      |               { category: { type: other },              coefficient : { value: 1 } },
      |               { category: { type: heating },            coefficient : { value: 0 } },
      |               { category: { type: waterheating },       coefficient : { value: 0.75 } }
      |             ]
      |         },{
      |           month : { type: october },
      |           coefficient:
      |             [
      |               { category: { type: alwayson },           coefficient : { value: 1 } },
      |               { category: { type: refrigeration },      coefficient : { value: 1 } },
      |               { category: { type: cooking },            coefficient : { value: 1 } },
      |               { category: { type: lighting },           coefficient : { value: 1 } },
      |               { category: { type: laundrydishwashing }, coefficient : { value: 1 } },
      |               { category: { type: homeentertainment },  coefficient : { value: 1 } },
      |               { category: { type: other },              coefficient : { value: 1 } },
      |               { category: { type: heating },            coefficient : { value: 2 } },
      |               { category: { type: waterheating },       coefficient : { value: 1 } }
      |             ]
      |         },{
      |           month : { type: november },
      |           coefficient:
      |             [
      |               { category: { type: alwayson },           coefficient : { value: 1 } },
      |               { category: { type: refrigeration },      coefficient : { value: 1 } },
      |               { category: { type: cooking },            coefficient : { value: 1.5 } },
      |               { category: { type: lighting },           coefficient : { value: 1 } },
      |               { category: { type: laundrydishwashing }, coefficient : { value: 1 } },
      |               { category: { type: homeentertainment },  coefficient : { value: 1 } },
      |               { category: { type: other },              coefficient : { value: 1 } },
      |               { category: { type: heating },            coefficient : { value: 2.5 } },
      |               { category: { type: waterheating },       coefficient : { value: 1.25 } }
      |             ]
      |         },{
      |           month : { type: december },
      |           coefficient:
      |             [
      |               { category: { type: alwayson },           coefficient : { value: 1 } },
      |               { category: { type: refrigeration },      coefficient : { value: 1 } },
      |               { category: { type: cooking },            coefficient : { value: 1.5 } },
      |               { category: { type: lighting },           coefficient : { value: 1.2 } },
      |               { category: { type: laundrydishwashing }, coefficient : { value: 1 } },
      |               { category: { type: homeentertainment },  coefficient : { value: 1 } },
      |               { category: { type: other },              coefficient : { value: 1 } },
      |               { category: { type: heating },            coefficient : { value: 2.5 } },
      |               { category: { type: waterheating },       coefficient : { value: 1.25 } }
      |             ]
      |         }
      |      ]
      |    }
      |  ]
      | }""".stripMargin)

  val seasonalAdjustmentConfigLoader: SeasonalAdjustmentFromConfig =
    SeasonalAdjustmentFromConfig(electricitySimplifiedActivitySeasonalAdjustments)

  /**
    * electricity_simplified_activity_summer_load_distributions.csv
    *
    category,00:00,00:30,01:00,01:30,02:00,02:30,03:00,03:30,04:00,04:30,05:00,05:30,06:00,06:30,07:00,07:30,08:00,08:30,09:00,09:30,10:00,10:30,11:00,11:30,12:00,12:30,13:00,13:30,14:00,14:30,15:00,15:30,16:00,16:30,17:00,17:30,18:00,18:30,19:00,19:30,20:00,20:30,21:00,21:30,22:00,22:30,23:00,23:30
    always_on,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021
    refrigeration,0.021,0.021,0.02,0.019,0.019,0.018,0.018,0.017,0.017,0.017,0.017,0.017,0.018,0.019,0.02,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.022,0.022,0.023,0.023,0.024,0.025,0.024,0.023,0.023,0.022,0.022,0.022,0.022,0.021,0.021,0.02
    cooking,0.013709199,0.012294943,0.010738844,0.009363131,0.008869075,0.008406966,0.008182792,0.008159946,0.007982347,0.007983996,0.008373369,0.009006387,0.011018365,0.014797028,0.018962738,0.022456989,0.025642631,0.025855263,0.024633623,0.025973713,0.02505165,0.025508215,0.027044607,0.026599301,0.027154752,0.02622243,0.026369626,0.023980959,0.023185499,0.022450923,0.023239254,0.023875314,0.024498764,0.026039533,0.030465145,0.030161821,0.03052297,0.029928487,0.029761526,0.031579428,0.029706965,0.027209714,0.025882623,0.024016793,0.022241726,0.020431867,0.01844067,0.016018096
    lighting,0.017500275,0.010771248,0.009806648,0.009806648,0.009806648,0.009806648,0.009806648,0.009806648,0.009806648,0.009806648,0.009806648,0.009806648,0.009806648,0.01225831,0.015935802,0.019613295,0.022064958,0.024516619,0.025497284,0.02235969,0.016021642,0.011132329,0.009806648,0.009806648,0.009806648,0.009806648,0.009806648,0.009806648,0.009806648,0.009806648,0.018387465,0.020839126,0.022064958,0.022064958,0.02574245,0.028194113,0.042291169,0.04903324,0.04903324,0.04903324,0.04903324,0.04903324,0.046581578,0.037265262,0.031381273,0.027213448,0.024516619,0.020593961
    laundry_dishwashing,0.011013781,0.008945781,0.007228153,0.005115331,0.003648045,0.003140322,0.003034513,0.002834233,0.002896345,0.002965036,0.002883102,0.002874721,0.004017385,0.006289147,0.010619906,0.013813085,0.016340206,0.023378921,0.026071691,0.028695675,0.032906144,0.039862066,0.040312679,0.03806125,0.035182104,0.03592732,0.03315707,0.036263055,0.032676572,0.032689877,0.030282791,0.030734925,0.030925403,0.031013723,0.027359888,0.025100999,0.025144795,0.025079944,0.02479239,0.02507441,0.026033867,0.025659585,0.024979418,0.024819698,0.024869697,0.023000866,0.019065464,0.013218619
    home_entertainment,0.009,0.007,0.005,0.002,0.001,0,0,0,0,0,0.002,0.005,0.007,0.01,0.013,0.017,0.024,0.032,0.034,0.036,0.034,0.032,0.028,0.024,0.023,0.022,0.023,0.024,0.021,0.018,0.018,0.017,0.018,0.018,0.021,0.024,0.033,0.043,0.052,0.061,0.055,0.049,0.038,0.027,0.024,0.022,0.016,0.01
    other,0.009,0.007,0.005,0.002,0.001,0,0,0,0,0,0.002,0.005,0.007,0.01,0.013,0.017,0.024,0.032,0.034,0.036,0.034,0.032,0.028,0.024,0.023,0.022,0.023,0.024,0.021,0.018,0.018,0.017,0.018,0.018,0.021,0.024,0.033,0.043,0.052,0.061,0.055,0.049,0.038,0.027,0.024,0.022,0.016,0.01
    heating,0.007,0.007,0.006,0.006,0.005,0.005,0.004,0.003,0.003,0.002,0.002,0.002,0.003,0.003,0.003,0.003,0.005,0.007,0.008,0.009,0.013,0.017,0.019,0.022,0.023,0.025,0.027,0.029,0.034,0.04,0.042,0.045,0.047,0.05,0.053,0.055,0.053,0.05,0.048,0.046,0.039,0.032,0.027,0.023,0.017,0.011,0.009,0.007
    water_heating,0.012,0.011,0.009,0.007,0.006,0.006,0.006,0.006,0.006,0.005,0.008,0.01,0.017,0.024,0.03,0.036,0.037,0.037,0.036,0.035,0.034,0.033,0.031,0.029,0.026,0.024,0.023,0.022,0.02,0.019,0.018,0.018,0.019,0.02,0.022,0.024,0.025,0.026,0.027,0.029,0.027,0.025,0.024,0.022,0.02,0.019,0.016,0.013
   */
  val electricitySimplifiedActivitySummerLoadDistributions: String =
    """
      |  summer: {
      |    loaddistributions: [
      |      {
      |        category: {type: alwayson}
      |        sourceloads: [
      |          {from: "00:00", coefficient: {underlying: 0.021}}
      |        ]
      |      }
      |      {
      |        category: {type: refrigeration}
      |        sourceloads: [
      |          {from: "00:00", coefficient: {underlying: 0.021}}
      |          {from: "00:30", coefficient: {underlying: 0.021}}
      |          {from: "01:00", coefficient: {underlying: 0.02}}
      |          {from: "01:30", coefficient: {underlying: 0.019}}
      |          {from: "02:30", coefficient: {underlying: 0.018}}
      |          {from: "03:30", coefficient: {underlying: 0.017}}
      |          {from: "06:00", coefficient: {underlying: 0.018}}
      |          {from: "06:30", coefficient: {underlying: 0.019}}
      |          {from: "07:00", coefficient: {underlying: 0.02}}
      |          {from: "07:30", coefficient: {underlying: 0.021}}
      |          {from: "16:00", coefficient: {underlying: 0.022}}
      |          {from: "17:00", coefficient: {underlying: 0.023}}
      |          {from: "18:00", coefficient: {underlying: 0.024}}
      |          {from: "18:30", coefficient: {underlying: 0.025}}
      |          {from: "19:00", coefficient: {underlying: 0.024}}
      |          {from: "19:30", coefficient: {underlying: 0.023}}
      |          {from: "20:30", coefficient: {underlying: 0.022}}
      |          {from: "22:30", coefficient: {underlying: 0.021}}
      |          {from: "23:30", coefficient: {underlying: 0.02}}
      |        ]
      |      }
      |      {
      |        category: {type: cooking}
      |        sourceloads: [
      |          {from: "00:00", coefficient: {underlying: 0.013709199}}
      |          {from: "00:30", coefficient: {underlying: 0.012294943}}
      |          {from: "01:00", coefficient: {underlying: 0.010738844}}
      |          {from: "01:30", coefficient: {underlying: 0.009363131}}
      |          {from: "02:00", coefficient: {underlying: 0.008869075}}
      |          {from: "02:30", coefficient: {underlying: 0.008406966}}
      |          {from: "03:00", coefficient: {underlying: 0.008182792}}
      |          {from: "03:30", coefficient: {underlying: 0.008159946}}
      |
      |          {from: "04:00", coefficient: {underlying: 0.007982347}}
      |          {from: "04:30", coefficient: {underlying: 0.007983996}}
      |          {from: "05:00", coefficient: {underlying: 0.008373369}}
      |          {from: "05:30", coefficient: {underlying: 0.009006387}}
      |          {from: "06:00", coefficient: {underlying: 0.011018365}}
      |          {from: "06:30", coefficient: {underlying: 0.014797028}}
      |          {from: "07:00", coefficient: {underlying: 0.018962738}}
      |          {from: "07:30", coefficient: {underlying: 0.022456989}}
      |
      |          {from: "08:00", coefficient: {underlying: 0.025642631}}
      |          {from: "08:30", coefficient: {underlying: 0.025855263}}
      |          {from: "09:00", coefficient: {underlying: 0.024633623}}
      |          {from: "09:30", coefficient: {underlying: 0.025973713}}
      |          {from: "10:00", coefficient: {underlying: 0.02505165}}
      |          {from: "10:30", coefficient: {underlying: 0.025508215}}
      |          {from: "11:00", coefficient: {underlying: 0.027044607}}
      |          {from: "11:30", coefficient: {underlying: 0.026599301}}
      |
      |          {from: "12:00", coefficient: {underlying: 0.027154752}}
      |          {from: "12:30", coefficient: {underlying: 0.02622243}}
      |          {from: "13:00", coefficient: {underlying: 0.026369626}}
      |          {from: "13:30", coefficient: {underlying: 0.023980959}}
      |          {from: "14:00", coefficient: {underlying: 0.023185499}}
      |          {from: "14:30", coefficient: {underlying: 0.022450923}}
      |          {from: "15:00", coefficient: {underlying: 0.023239254}}
      |          {from: "15:30", coefficient: {underlying: 0.023875314}}
      |
      |          {from: "16:00", coefficient: {underlying: 0.024498764}}
      |          {from: "16:30", coefficient: {underlying: 0.026039533}}
      |          {from: "17:00", coefficient: {underlying: 0.030465145}}
      |          {from: "17:30", coefficient: {underlying: 0.030161821}}
      |          {from: "18:00", coefficient: {underlying: 0.03052297}}
      |          {from: "18:30", coefficient: {underlying: 0.029928487}}
      |          {from: "19:00", coefficient: {underlying: 0.029761526}}
      |          {from: "19:30", coefficient: {underlying: 0.031579428}}
      |
      |          {from: "20:00", coefficient: {underlying: 0.029706965}}
      |          {from: "20:30", coefficient: {underlying: 0.027209714}}
      |          {from: "21:00", coefficient: {underlying: 0.025882623}}
      |          {from: "21:30", coefficient: {underlying: 0.024016793}}
      |          {from: "22:00", coefficient: {underlying: 0.022241726}}
      |          {from: "22:30", coefficient: {underlying: 0.020431867}}
      |          {from: "23:00", coefficient: {underlying: 0.01844067}}
      |          {from: "23:30", coefficient: {underlying: 0.016018096}}
      |        ]
      |      }
      |      {
      |        category: {type: lighting}
      |        sourceloads: [
      |          {from: "00:00", coefficient: {underlying: 0.017500275}}
      |          {from: "00:30", coefficient: {underlying: 0.010771248}}
      |          {from: "01:00", coefficient: {underlying: 0.009806648}}
      |          {from: "01:30", coefficient: {underlying: 0.009806648}}
      |          {from: "02:00", coefficient: {underlying: 0.009806648}}
      |          {from: "02:30", coefficient: {underlying: 0.009806648}}
      |          {from: "03:00", coefficient: {underlying: 0.009806648}}
      |          {from: "03:30", coefficient: {underlying: 0.009806648}}
      |
      |          {from: "04:00", coefficient: {underlying: 0.009806648}}
      |          {from: "04:30", coefficient: {underlying: 0.009806648}}
      |          {from: "05:00", coefficient: {underlying: 0.009806648}}
      |          {from: "05:30", coefficient: {underlying: 0.009806648}}
      |          {from: "06:00", coefficient: {underlying: 0.009806648}}
      |          {from: "06:30", coefficient: {underlying: 0.01225831}}
      |          {from: "07:00", coefficient: {underlying: 0.015935802}}
      |          {from: "07:30", coefficient: {underlying: 0.019613295}}
      |
      |          {from: "08:00", coefficient: {underlying: 0.022064958}}
      |          {from: "08:30", coefficient: {underlying: 0.024516619}}
      |          {from: "09:00", coefficient: {underlying: 0.025497284}}
      |          {from: "09:30", coefficient: {underlying: 0.02235969}}
      |          {from: "10:00", coefficient: {underlying: 0.016021642}}
      |          {from: "10:30", coefficient: {underlying: 0.011132329}}
      |          {from: "11:00", coefficient: {underlying: 0.009806648}}
      |          {from: "11:30", coefficient: {underlying: 0.009806648}}
      |
      |          {from: "12:00", coefficient: {underlying: 0.009806648}}
      |          {from: "12:30", coefficient: {underlying: 0.009806648}}
      |          {from: "13:00", coefficient: {underlying: 0.009806648}}
      |          {from: "13:30", coefficient: {underlying: 0.009806648}}
      |          {from: "14:00", coefficient: {underlying: 0.009806648}}
      |          {from: "14:30", coefficient: {underlying: 0.009806648}}
      |          {from: "15:00", coefficient: {underlying: 0.018387465}}
      |          {from: "15:30", coefficient: {underlying: 0.020839126}}
      |
      |          {from: "16:00", coefficient: {underlying: 0.022064958}}
      |          {from: "16:30", coefficient: {underlying: 0.022064958}}
      |          {from: "17:00", coefficient: {underlying: 0.02574245}}
      |          {from: "17:30", coefficient: {underlying: 0.028194113}}
      |          {from: "18:00", coefficient: {underlying: 0.042291169}}
      |          {from: "18:30", coefficient: {underlying: 0.04903324}}
      |          {from: "19:00", coefficient: {underlying: 0.04903324}}
      |          {from: "19:30", coefficient: {underlying: 0.04903324}}
      |
      |          {from: "20:00", coefficient: {underlying: 0.04903324}}
      |          {from: "20:30", coefficient: {underlying: 0.04903324}}
      |          {from: "21:00", coefficient: {underlying: 0.046581578}}
      |          {from: "21:30", coefficient: {underlying: 0.037265262}}
      |          {from: "22:00", coefficient: {underlying: 0.031381273}}
      |          {from: "22:30", coefficient: {underlying: 0.027213448}}
      |          {from: "23:00", coefficient: {underlying: 0.024516619}}
      |          {from: "23:30", coefficient: {underlying: 0.020593961}}
      |        ]
      |      }
      |      {
      |        category: {type: laundrydishwashing}
      |        sourceloads: [
      |          {from: "00:00", coefficient: {underlying: 0.011013781}}
      |          {from: "00:30", coefficient: {underlying: 0.008945781}}
      |          {from: "01:00", coefficient: {underlying: 0.007228153}}
      |          {from: "01:30", coefficient: {underlying: 0.005115331}}
      |          {from: "02:00", coefficient: {underlying: 0.003648045}}
      |          {from: "02:30", coefficient: {underlying: 0.003140322}}
      |          {from: "03:00", coefficient: {underlying: 0.003034513}}
      |          {from: "03:30", coefficient: {underlying: 0.002834233}}
      |
      |          {from: "04:00", coefficient: {underlying: 0.002896345}}
      |          {from: "04:30", coefficient: {underlying: 0.002965036}}
      |          {from: "05:00", coefficient: {underlying: 0.002883102}}
      |          {from: "05:30", coefficient: {underlying: 0.002874721}}
      |          {from: "06:00", coefficient: {underlying: 0.004017385}}
      |          {from: "06:30", coefficient: {underlying: 0.006289147}}
      |          {from: "07:00", coefficient: {underlying: 0.010619906}}
      |          {from: "07:30", coefficient: {underlying: 0.013813085}}
      |
      |          {from: "08:00", coefficient: {underlying: 0.016340206}}
      |          {from: "08:30", coefficient: {underlying: 0.023378921}}
      |          {from: "09:00", coefficient: {underlying: 0.026071691}}
      |          {from: "09:30", coefficient: {underlying: 0.028695675}}
      |          {from: "10:00", coefficient: {underlying: 0.032906144}}
      |          {from: "10:30", coefficient: {underlying: 0.039862066}}
      |          {from: "11:00", coefficient: {underlying: 0.040312679}}
      |          {from: "11:30", coefficient: {underlying: 0.03806125}}
      |
      |          {from: "12:00", coefficient: {underlying: 0.035182104}}
      |          {from: "12:30", coefficient: {underlying: 0.03592732}}
      |          {from: "13:00", coefficient: {underlying: 0.03315707}}
      |          {from: "13:30", coefficient: {underlying: 0.036263055}}
      |          {from: "14:00", coefficient: {underlying: 0.032676572}}
      |          {from: "14:30", coefficient: {underlying: 0.032689877}}
      |          {from: "15:00", coefficient: {underlying: 0.030282791}}
      |          {from: "15:30", coefficient: {underlying: 0.030734925}}
      |
      |          {from: "16:00", coefficient: {underlying: 0.030925403}}
      |          {from: "16:30", coefficient: {underlying: 0.031013723}}
      |          {from: "17:00", coefficient: {underlying: 0.027359888}}
      |          {from: "17:30", coefficient: {underlying: 0.025100999}}
      |          {from: "18:00", coefficient: {underlying: 0.025144795}}
      |          {from: "18:30", coefficient: {underlying: 0.025079944}}
      |          {from: "19:00", coefficient: {underlying: 0.02479239}}
      |          {from: "19:30", coefficient: {underlying: 0.02507441}}
      |
      |          {from: "20:00", coefficient: {underlying: 0.026033867}}
      |          {from: "20:30", coefficient: {underlying: 0.025659585}}
      |          {from: "21:00", coefficient: {underlying: 0.024979418}}
      |          {from: "21:30", coefficient: {underlying: 0.024819698}}
      |          {from: "22:00", coefficient: {underlying: 0.024869697}}
      |          {from: "22:30", coefficient: {underlying: 0.023000866}}
      |          {from: "23:00", coefficient: {underlying: 0.019065464}}
      |          {from: "23:30", coefficient: {underlying: 0.013218619}}
      |        ]
      |      }
      |      {
      |        category: {type: homeentertainment}
      |        sourceloads: [
      |          {from: "00:00", coefficient: {underlying: 0.009}}
      |          {from: "00:30", coefficient: {underlying: 0.007}}
      |          {from: "01:00", coefficient: {underlying: 0.005}}
      |          {from: "01:30", coefficient: {underlying: 0.002}}
      |          {from: "02:00", coefficient: {underlying: 0.001}}
      |          {from: "02:30", coefficient: {underlying: 0}}
      |
      |          {from: "05:00", coefficient: {underlying: 0.002}}
      |          {from: "05:30", coefficient: {underlying: 0.005}}
      |          {from: "06:00", coefficient: {underlying: 0.007}}
      |          {from: "06:30", coefficient: {underlying: 0.01}}
      |          {from: "07:00", coefficient: {underlying: 0.013}}
      |          {from: "07:30", coefficient: {underlying: 0.017}}
      |
      |          {from: "08:00", coefficient: {underlying: 0.024}}
      |          {from: "08:30", coefficient: {underlying: 0.032}}
      |          {from: "09:00", coefficient: {underlying: 0.034}}
      |          {from: "09:30", coefficient: {underlying: 0.036}}
      |          {from: "10:00", coefficient: {underlying: 0.034}}
      |          {from: "10:30", coefficient: {underlying: 0.032}}
      |          {from: "11:00", coefficient: {underlying: 0.028}}
      |          {from: "11:30", coefficient: {underlying: 0.024}}
      |
      |          {from: "12:00", coefficient: {underlying: 0.023}}
      |          {from: "12:30", coefficient: {underlying: 0.022}}
      |          {from: "13:00", coefficient: {underlying: 0.023}}
      |          {from: "13:30", coefficient: {underlying: 0.024}}
      |          {from: "14:00", coefficient: {underlying: 0.021}}
      |          {from: "14:30", coefficient: {underlying: 0.018}}
      |          {from: "15:30", coefficient: {underlying: 0.017}}
      |
      |          {from: "16:00", coefficient: {underlying: 0.018}}
      |          {from: "17:00", coefficient: {underlying: 0.021}}
      |          {from: "17:30", coefficient: {underlying: 0.024}}
      |          {from: "18:00", coefficient: {underlying: 0.033}}
      |          {from: "18:30", coefficient: {underlying: 0.043}}
      |          {from: "19:00", coefficient: {underlying: 0.052}}
      |          {from: "19:30", coefficient: {underlying: 0.061}}
      |
      |          {from: "20:00", coefficient: {underlying: 0.055}}
      |          {from: "20:30", coefficient: {underlying: 0.049}}
      |          {from: "21:00", coefficient: {underlying: 0.038}}
      |          {from: "21:30", coefficient: {underlying: 0.027}}
      |          {from: "22:00", coefficient: {underlying: 0.024}}
      |          {from: "22:30", coefficient: {underlying: 0.022}}
      |          {from: "23:00", coefficient: {underlying: 0.016}}
      |          {from: "23:30", coefficient: {underlying: 0.01}}
      |        ]
      |      }
      |      {
      |        category: {type: other}
      |        sourceloads: [
      |          {from: "00:00", coefficient: {underlying: 0.009}}
      |          {from: "00:30", coefficient: {underlying: 0.007}}
      |          {from: "01:00", coefficient: {underlying: 0.005}}
      |          {from: "01:30", coefficient: {underlying: 0.002}}
      |          {from: "02:00", coefficient: {underlying: 0.001}}
      |          {from: "02:30", coefficient: {underlying: 0}}
      |
      |          {from: "05:00", coefficient: {underlying: 0.002}}
      |          {from: "05:30", coefficient: {underlying: 0.005}}
      |          {from: "06:00", coefficient: {underlying: 0.007}}
      |          {from: "06:30", coefficient: {underlying: 0.01}}
      |          {from: "07:00", coefficient: {underlying: 0.013}}
      |          {from: "07:30", coefficient: {underlying: 0.017}}
      |
      |          {from: "08:00", coefficient: {underlying: 0.024}}
      |          {from: "08:30", coefficient: {underlying: 0.032}}
      |          {from: "09:00", coefficient: {underlying: 0.034}}
      |          {from: "09:30", coefficient: {underlying: 0.036}}
      |          {from: "10:00", coefficient: {underlying: 0.034}}
      |          {from: "10:30", coefficient: {underlying: 0.032}}
      |          {from: "11:00", coefficient: {underlying: 0.028}}
      |          {from: "11:30", coefficient: {underlying: 0.024}}
      |
      |          {from: "12:00", coefficient: {underlying: 0.023}}
      |          {from: "12:30", coefficient: {underlying: 0.022}}
      |          {from: "13:00", coefficient: {underlying: 0.023}}
      |          {from: "13:30", coefficient: {underlying: 0.024}}
      |          {from: "14:00", coefficient: {underlying: 0.021}}
      |          {from: "14:30", coefficient: {underlying: 0.018}}
      |          {from: "15:30", coefficient: {underlying: 0.017}}
      |
      |          {from: "16:00", coefficient: {underlying: 0.018}}
      |          {from: "17:00", coefficient: {underlying: 0.021}}
      |          {from: "17:30", coefficient: {underlying: 0.024}}
      |          {from: "18:00", coefficient: {underlying: 0.033}}
      |          {from: "18:30", coefficient: {underlying: 0.043}}
      |          {from: "19:00", coefficient: {underlying: 0.052}}
      |          {from: "19:30", coefficient: {underlying: 0.061}}
      |
      |          {from: "20:00", coefficient: {underlying: 0.055}}
      |          {from: "20:30", coefficient: {underlying: 0.049}}
      |          {from: "21:00", coefficient: {underlying: 0.038}}
      |          {from: "21:30", coefficient: {underlying: 0.027}}
      |          {from: "22:00", coefficient: {underlying: 0.024}}
      |          {from: "22:30", coefficient: {underlying: 0.022}}
      |          {from: "23:00", coefficient: {underlying: 0.016}}
      |          {from: "23:30", coefficient: {underlying: 0.01}}
      |        ]
      |      }
      |      {
      |        category: {type: heating}
      |        sourceloads: [
      |          {from: "00:00", coefficient: {underlying: 0.007}}
      |          {from: "01:00", coefficient: {underlying: 0.006}}
      |          {from: "02:00", coefficient: {underlying: 0.005}}
      |          {from: "03:00", coefficient: {underlying: 0.004}}
      |          {from: "03:30", coefficient: {underlying: 0.003}}
      |
      |          {from: "04:30", coefficient: {underlying: 0.002}}
      |          {from: "06:00", coefficient: {underlying: 0.003}}
      |
      |          {from: "08:00", coefficient: {underlying: 0.005}}
      |          {from: "08:30", coefficient: {underlying: 0.007}}
      |          {from: "09:00", coefficient: {underlying: 0.008}}
      |          {from: "09:30", coefficient: {underlying: 0.009}}
      |          {from: "10:00", coefficient: {underlying: 0.013}}
      |          {from: "10:30", coefficient: {underlying: 0.017}}
      |          {from: "11:00", coefficient: {underlying: 0.019}}
      |          {from: "11:30", coefficient: {underlying: 0.022}}
      |
      |          {from: "12:00", coefficient: {underlying: 0.023}}
      |          {from: "12:30", coefficient: {underlying: 0.025}}
      |          {from: "13:00", coefficient: {underlying: 0.027}}
      |          {from: "13:30", coefficient: {underlying: 0.029}}
      |          {from: "14:00", coefficient: {underlying: 0.034}}
      |          {from: "14:30", coefficient: {underlying: 0.04}}
      |          {from: "15:00", coefficient: {underlying: 0.042}}
      |          {from: "15:30", coefficient: {underlying: 0.045}}
      |
      |          {from: "16:00", coefficient: {underlying: 0.047}}
      |          {from: "16:30", coefficient: {underlying: 0.05}}
      |          {from: "17:00", coefficient: {underlying: 0.053}}
      |          {from: "17:30", coefficient: {underlying: 0.055}}
      |          {from: "18:00", coefficient: {underlying: 0.053}}
      |          {from: "18:30", coefficient: {underlying: 0.05}}
      |          {from: "19:00", coefficient: {underlying: 0.048}}
      |          {from: "19:30", coefficient: {underlying: 0.046}}
      |
      |          {from: "20:00", coefficient: {underlying: 0.039}}
      |          {from: "20:30", coefficient: {underlying: 0.032}}
      |          {from: "21:00", coefficient: {underlying: 0.027}}
      |          {from: "21:30", coefficient: {underlying: 0.023}}
      |          {from: "22:00", coefficient: {underlying: 0.017}}
      |          {from: "22:30", coefficient: {underlying: 0.011}}
      |          {from: "23:00", coefficient: {underlying: 0.009}}
      |          {from: "23:30", coefficient: {underlying: 0.007}}
      |        ]
      |      }
      |      {
      |        category: {type: waterheating}
      |        sourceloads: [
      |          {from: "00:00", coefficient: {underlying: 0.012}}
      |          {from: "00:30", coefficient: {underlying: 0.011}}
      |          {from: "01:00", coefficient: {underlying: 0.009}}
      |          {from: "01:30", coefficient: {underlying: 0.007}}
      |          {from: "02:00", coefficient: {underlying: 0.006}}
      |
      |          {from: "04:30", coefficient: {underlying: 0.005}}
      |          {from: "05:00", coefficient: {underlying: 0.008}}
      |          {from: "05:30", coefficient: {underlying: 0.01}}
      |          {from: "06:00", coefficient: {underlying: 0.017}}
      |          {from: "06:30", coefficient: {underlying: 0.024}}
      |          {from: "07:00", coefficient: {underlying: 0.03}}
      |          {from: "07:30", coefficient: {underlying: 0.036}}
      |
      |          {from: "08:00", coefficient: {underlying: 0.037}}
      |          {from: "09:00", coefficient: {underlying: 0.036}}
      |          {from: "09:30", coefficient: {underlying: 0.035}}
      |          {from: "10:00", coefficient: {underlying: 0.034}}
      |          {from: "10:30", coefficient: {underlying: 0.033}}
      |          {from: "11:00", coefficient: {underlying: 0.031}}
      |          {from: "11:30", coefficient: {underlying: 0.029}}
      |
      |          {from: "12:00", coefficient: {underlying: 0.026}}
      |          {from: "12:30", coefficient: {underlying: 0.024}}
      |          {from: "13:00", coefficient: {underlying: 0.023}}
      |          {from: "13:30", coefficient: {underlying: 0.022}}
      |          {from: "14:00", coefficient: {underlying: 0.02}}
      |          {from: "14:30", coefficient: {underlying: 0.019}}
      |          {from: "15:00", coefficient: {underlying: 0.018}}
      |
      |          {from: "16:00", coefficient: {underlying: 0.019}}
      |          {from: "16:30", coefficient: {underlying: 0.02}}
      |          {from: "17:00", coefficient: {underlying: 0.022}}
      |          {from: "17:30", coefficient: {underlying: 0.024}}
      |          {from: "18:00", coefficient: {underlying: 0.025}}
      |          {from: "18:30", coefficient: {underlying: 0.026}}
      |          {from: "19:00", coefficient: {underlying: 0.027}}
      |          {from: "19:30", coefficient: {underlying: 0.029}}
      |
      |          {from: "20:00", coefficient: {underlying: 0.027}}
      |          {from: "20:30", coefficient: {underlying: 0.025}}
      |          {from: "21:00", coefficient: {underlying: 0.024}}
      |          {from: "21:30", coefficient: {underlying: 0.022}}
      |          {from: "22:00", coefficient: {underlying: 0.02}}
      |          {from: "22:30", coefficient: {underlying: 0.019}}
      |          {from: "23:00", coefficient: {underlying: 0.016}}
      |          {from: "23:30", coefficient: {underlying: 0.013}}
      |        ]
      |      }
      |    ]
      |  }
      """.stripMargin


  /**
    * electricity_simplified_activity_winter_load_distributions.csv
    *
    * category,00:00,00:30,01:00,01:30,02:00,02:30,03:00,03:30,04:00,04:30,05:00,05:30,06:00,06:30,07:00,07:30,08:00,08:30,09:00,09:30,10:00,10:30,11:00,11:30,12:00,12:30,13:00,13:30,14:00,14:30,15:00,15:30,16:00,16:30,17:00,17:30,18:00,18:30,19:00,19:30,20:00,20:30,21:00,21:30,22:00,22:30,23:00,23:30
      always_on,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021,0.021
      refrigeration,0.019554013,0.020046101,0.020279195,0.019256171,0.020033151,0.018233146,0.019554013,0.018323794,0.019787107,0.018168398,0.019191422,0.018725234,0.018323794,0.020356893,0.019981352,0.019644661,0.020577038,0.020848981,0.021263371,0.020033151,0.019709409,0.019968403,0.020240346,0.02048639,0.020810132,0.020589987,0.020667685,0.022040351,0.020538189,0.020784232,0.021056176,0.021004377,0.02127632,0.021794307,0.023840356,0.023607262,0.022985678,0.022998627,0.023024527,0.022765533,0.023879205,0.023218772,0.023192872,0.02248064,0.021030276,0.02169071,0.021043226,0.021095025
      cooking,0.004823636,0.003147422,0.004558336,0.003159481,0.002279168,0.004172445,0.004003618,0.005390413,0.006306904,0.008139885,0.007163099,0.007597226,0.007862526,0.013723244,0.017039494,0.022405788,0.026554115,0.02622852,0.027458547,0.026698824,0.026771179,0.02078987,0.023370516,0.025806452,0.028411215,0.027772083,0.03523666,0.029351824,0.02490202,0.025384383,0.024323184,0.029170938,0.032559542,0.041832982,0.044992463,0.048393126,0.051962617,0.046692795,0.035043714,0.030087428,0.026216461,0.023069038,0.019764848,0.014398553,0.010925535,0.009176967,0.007971058,0.006909858
      lighting,0.030983734,0.0232378,0.015491867,0.007745933,0.000774593,0.000774593,0.000774593,0.000774593,0.000774593,0.000774593,0.000774593,0.019364833,0.038729667,0.038729667,0.038729667,0.038729667,0.030983734,0.0232378,0.015491867,0.007745933,0.000774593,0.000774593,0.000774593,0.000774593,0.000774593,0.000774593,0.000774593,0.000774593,0.000774593,0.007745933,0.015491867,0.0232378,0.030983734,0.038729667,0.038729667,0.038729667,0.038729667,0.038729667,0.038729667,0.038729667,0.038729667,0.038729667,0.038729667,0.038729667,0.038729667,0.038729667,0.038729667,0.038729667
      laundry_dishwashing,0.007073397,0.004878298,0.002990875,0.002524181,0.001543927,0.001758888,0.001749185,0.001176952,0.001776234,0.001484057,0.001249005,0.001556541,0.002724749,0.004918535,0.008104539,0.009664915,0.015394118,0.022084107,0.025671802,0.028643813,0.034278192,0.040366743,0.045644713,0.047459791,0.045789197,0.041126691,0.038736952,0.03538695,0.031781642,0.036847812,0.033405794,0.029270989,0.028403115,0.026248341,0.026776616,0.031435015,0.025917103,0.024845937,0.025886779,0.02733871,0.026674604,0.02592678,0.026529876,0.023011073,0.021250605,0.020291782,0.0184854,0.013914675
      home_entertainment,0.009,0.007,0.005,0.002,0.001,0,0,0,0,0,0.002,0.005,0.007,0.01,0.013,0.017,0.024,0.032,0.034,0.036,0.034,0.032,0.028,0.024,0.023,0.022,0.023,0.024,0.021,0.018,0.018,0.017,0.018,0.018,0.021,0.024,0.033,0.043,0.052,0.061,0.055,0.049,0.038,0.027,0.024,0.022,0.016,0.01
      other,0.017148632,0.015338194,0.014708727,0.013848264,0.012831878,0.012609543,0.012303472,0.013267884,0.012655742,0.012413196,0.012958926,0.01337472,0.014131235,0.016097596,0.01544503,0.015384393,0.017217931,0.020833033,0.020131379,0.021453837,0.020922544,0.020209341,0.020602036,0.021710821,0.0232585,0.021982242,0.022270988,0.021826319,0.023864867,0.023105465,0.022608821,0.024479896,0.024739768,0.026925576,0.029997834,0.030269256,0.030191294,0.030159532,0.029264419,0.029576265,0.029489641,0.028588753,0.027696528,0.027679203,0.024329748,0.021641522,0.019906158,0.018549051
      heating,0.016,0.016,0.017,0.018,0.019,0.019,0.02,0.02,0.021,0.021,0.024,0.026,0.031,0.036,0.038,0.04,0.038,0.036,0.033,0.029,0.026,0.023,0.021,0.02,0.018,0.016,0.015,0.015,0.014,0.014,0.014,0.015,0.016,0.018,0.019,0.02,0.02,0.019,0.017,0.016,0.016,0.016,0.016,0.016,0.016,0.016,0.016,0.016
      water_heating,0.012,0.011,0.009,0.007,0.006,0.006,0.006,0.006,0.006,0.005,0.008,0.01,0.017,0.024,0.03,0.036,0.037,0.037,0.036,0.035,0.034,0.033,0.031,0.029,0.026,0.024,0.023,0.022,0.02,0.019,0.018,0.018,0.019,0.02,0.022,0.024,0.025,0.026,0.027,0.029,0.027,0.025,0.024,0.022,0.02,0.019,0.016,0.013
    */
  val electricitySimplifiedActivityWinterLoadDistributions: String =
    """
      |  winter: {
      |    loaddistributions: [
      |      {
      |        category: {type: alwayson}
      |        sourceloads: [
      |          {from: "00:00", coefficient: {underlying: 0.021}}
      |        ]
      |      }
      |      {
      |        category: {type: refrigeration}
      |        sourceloads: [
      |          {from: "00:00", coefficient: {underlying: 0.019554013}}
      |          {from: "00:30", coefficient: {underlying: 0.020046101}}
      |          {from: "01:00", coefficient: {underlying: 0.020279195}}
      |          {from: "01:30", coefficient: {underlying: 0.019256171}}
      |          {from: "02:00", coefficient: {underlying: 0.020033151}}
      |          {from: "02:30", coefficient: {underlying: 0.018233146}}
      |          {from: "03:00", coefficient: {underlying: 0.019554013}}
      |          {from: "03:30", coefficient: {underlying: 0.018323794}}
      |
      |
      |          {from: "04:00", coefficient: {underlying: 0.019787107}}
      |          {from: "04:30", coefficient: {underlying: 0.018168398}}
      |          {from: "05:00", coefficient: {underlying: 0.019191422}}
      |          {from: "05:30", coefficient: {underlying: 0.018725234}}
      |          {from: "06:00", coefficient: {underlying: 0.018323794}}
      |          {from: "06:30", coefficient: {underlying: 0.020356893}}
      |          {from: "07:00", coefficient: {underlying: 0.019981352}}
      |          {from: "07:30", coefficient: {underlying: 0.019644661}}
      |
      |          {from: "08:00", coefficient: {underlying: 0.020577038}}
      |          {from: "08:30", coefficient: {underlying: 0.020848981}}
      |          {from: "09:00", coefficient: {underlying: 0.021263371}}
      |          {from: "09:30", coefficient: {underlying: 0.020033151}}
      |          {from: "10:00", coefficient: {underlying: 0.019709409}}
      |          {from: "10:30", coefficient: {underlying: 0.019968403}}
      |          {from: "11:00", coefficient: {underlying: 0.020240346}}
      |          {from: "11:30", coefficient: {underlying: 0.02048639}}
      |
      |          {from: "12:00", coefficient: {underlying: 0.020810132}}
      |          {from: "12:30", coefficient: {underlying: 0.020589987}}
      |          {from: "13:00", coefficient: {underlying: 0.020667685}}
      |          {from: "13:30", coefficient: {underlying: 0.022040351}}
      |          {from: "14:00", coefficient: {underlying: 0.020538189}}
      |          {from: "14:30", coefficient: {underlying: 0.020784232}}
      |          {from: "15:00", coefficient: {underlying: 0.021056176}}
      |          {from: "15:30", coefficient: {underlying: 0.021004377}}
      |
      |          {from: "16:00", coefficient: {underlying: 0.02127632}}
      |          {from: "16:30", coefficient: {underlying: 0.021794307}}
      |          {from: "17:00", coefficient: {underlying: 0.023840356}}
      |          {from: "17:30", coefficient: {underlying: 0.023607262}}
      |          {from: "18:00", coefficient: {underlying: 0.022985678}}
      |          {from: "18:30", coefficient: {underlying: 0.022998627}}
      |          {from: "19:00", coefficient: {underlying: 0.023024527}}
      |          {from: "19:30", coefficient: {underlying: 0.022765533}}
      |
      |          {from: "20:00", coefficient: {underlying: 0.023879205}}
      |          {from: "20:30", coefficient: {underlying: 0.023218772}}
      |          {from: "21:00", coefficient: {underlying: 0.023192872}}
      |          {from: "21:30", coefficient: {underlying: 0.02248064}}
      |          {from: "22:00", coefficient: {underlying: 0.021030276}}
      |          {from: "22:30", coefficient: {underlying: 0.02169071}}
      |          {from: "23:00", coefficient: {underlying: 0.021043226}}
      |          {from: "23:30", coefficient: {underlying: 0.021095025}}
      |        ]
      |      }
      |      {
      |        category: {type: cooking}
      |        sourceloads: [
      |          {from: "00:00", coefficient: {underlying: 0.004823636}}
      |          {from: "00:30", coefficient: {underlying: 0.003147422}}
      |          {from: "01:00", coefficient: {underlying: 0.004558336}}
      |          {from: "01:30", coefficient: {underlying: 0.003159481}}
      |          {from: "02:00", coefficient: {underlying: 0.002279168}}
      |          {from: "02:30", coefficient: {underlying: 0.004172445}}
      |          {from: "03:00", coefficient: {underlying: 0.004003618}}
      |          {from: "03:30", coefficient: {underlying: 0.005390413}}
      |
      |          {from: "04:00", coefficient: {underlying: 0.006306904}}
      |          {from: "04:30", coefficient: {underlying: 0.008139885}}
      |          {from: "05:00", coefficient: {underlying: 0.007163099}}
      |          {from: "05:30", coefficient: {underlying: 0.007597226}}
      |          {from: "06:00", coefficient: {underlying: 0.007862526}}
      |          {from: "06:30", coefficient: {underlying: 0.013723244}}
      |          {from: "07:00", coefficient: {underlying: 0.017039494}}
      |          {from: "07:30", coefficient: {underlying: 0.022405788}}
      |
      |          {from: "08:00", coefficient: {underlying: 0.026554115}}
      |          {from: "08:30", coefficient: {underlying: 0.02622852}}
      |          {from: "09:00", coefficient: {underlying: 0.027458547}}
      |          {from: "09:30", coefficient: {underlying: 0.026698824}}
      |          {from: "10:00", coefficient: {underlying: 0.026771179}}
      |          {from: "10:30", coefficient: {underlying: 0.02078987}}
      |          {from: "11:00", coefficient: {underlying: 0.023370516}}
      |          {from: "11:30", coefficient: {underlying: 0.025806452}}
      |
      |          {from: "12:00", coefficient: {underlying: 0.028411215}}
      |          {from: "12:30", coefficient: {underlying: 0.027772083}}
      |          {from: "13:00", coefficient: {underlying: 0.03523666}}
      |          {from: "13:30", coefficient: {underlying: 0.029351824}}
      |          {from: "14:00", coefficient: {underlying: 0.02490202}}
      |          {from: "14:30", coefficient: {underlying: 0.025384383}}
      |          {from: "15:00", coefficient: {underlying: 0.024323184}}
      |          {from: "15:30", coefficient: {underlying: 0.029170938}}
      |
      |          {from: "16:00", coefficient: {underlying: 0.032559542}}
      |          {from: "16:30", coefficient: {underlying: 0.041832982}}
      |          {from: "17:00", coefficient: {underlying: 0.044992463}}
      |          {from: "17:30", coefficient: {underlying: 0.048393126}}
      |          {from: "18:00", coefficient: {underlying: 0.051962617}}
      |          {from: "18:30", coefficient: {underlying: 0.046692795}}
      |          {from: "19:00", coefficient: {underlying: 0.035043714}}
      |          {from: "19:30", coefficient: {underlying: 0.030087428}}
      |
      |          {from: "20:00", coefficient: {underlying: 0.026216461}}
      |          {from: "20:30", coefficient: {underlying: 0.023069038}}
      |          {from: "21:00", coefficient: {underlying: 0.019764848}}
      |          {from: "21:30", coefficient: {underlying: 0.014398553}}
      |          {from: "22:00", coefficient: {underlying: 0.010925535}}
      |          {from: "22:30", coefficient: {underlying: 0.009176967}}
      |          {from: "23:00", coefficient: {underlying: 0.007971058}}
      |          {from: "23:30", coefficient: {underlying: 0.006909858}}
      |        ]
      |      }
      |      {
      |        category: {type: lighting}
      |        sourceloads: [
      |          {from: "00:00", coefficient: {underlying: 0.030983734}}
      |          {from: "00:30", coefficient: {underlying: 0.0232378}}
      |          {from: "01:00", coefficient: {underlying: 0.015491867}}
      |          {from: "01:30", coefficient: {underlying: 0.007745933}}
      |          {from: "02:00", coefficient: {underlying: 0.000774593}}
      |
      |          {from: "05:30", coefficient: {underlying: 0.019364833}}
      |          {from: "06:00", coefficient: {underlying: 0.038729667}}
      |
      |          {from: "08:00", coefficient: {underlying: 0.030983734}}
      |          {from: "08:30", coefficient: {underlying: 0.0232378}}
      |          {from: "09:00", coefficient: {underlying: 0.015491867}}
      |          {from: "09:30", coefficient: {underlying: 0.007745933}}
      |          {from: "10:00", coefficient: {underlying: 0.000774593}}
      |
      |          {from: "15:00", coefficient: {underlying: 0.015491867}}
      |          {from: "15:30", coefficient: {underlying: 0.0232378}}
      |
      |          {from: "16:00", coefficient: {underlying: 0.030983734}}
      |          {from: "16:30", coefficient: {underlying: 0.038729667}}
      |        ]
      |      }
      |      {
      |        category: {type: laundrydishwashing}
      |        sourceloads: [
      |          {from: "00:00", coefficient: {underlying: 0.007073397}}
      |          {from: "00:30", coefficient: {underlying: 0.004878298}}
      |          {from: "01:00", coefficient: {underlying: 0.002990875}}
      |          {from: "01:30", coefficient: {underlying: 0.002524181}}
      |          {from: "02:00", coefficient: {underlying: 0.001543927}}
      |          {from: "02:30", coefficient: {underlying: 0.001758888}}
      |          {from: "03:00", coefficient: {underlying: 0.001749185}}
      |          {from: "03:30", coefficient: {underlying: 0.001176952}}
      |
      |          {from: "04:00", coefficient: {underlying: 0.001776234}}
      |          {from: "04:30", coefficient: {underlying: 0.001484057}}
      |          {from: "05:00", coefficient: {underlying: 0.001249005}}
      |          {from: "05:30", coefficient: {underlying: 0.001556541}}
      |          {from: "06:00", coefficient: {underlying: 0.002724749}}
      |          {from: "06:30", coefficient: {underlying: 0.004918535}}
      |          {from: "07:00", coefficient: {underlying: 0.008104539}}
      |          {from: "07:30", coefficient: {underlying: 0.009664915}}
      |
      |          {from: "08:00", coefficient: {underlying: 0.015394118}}
      |          {from: "08:30", coefficient: {underlying: 0.022084107}}
      |          {from: "09:00", coefficient: {underlying: 0.025671802}}
      |          {from: "09:30", coefficient: {underlying: 0.028643813}}
      |          {from: "10:00", coefficient: {underlying: 0.034278192}}
      |          {from: "10:30", coefficient: {underlying: 0.040366743}}
      |          {from: "11:00", coefficient: {underlying: 0.045644713}}
      |          {from: "11:30", coefficient: {underlying: 0.047459791}}
      |
      |          {from: "12:00", coefficient: {underlying: 0.045789197}}
      |          {from: "12:30", coefficient: {underlying: 0.041126691}}
      |          {from: "13:00", coefficient: {underlying: 0.038736952}}
      |          {from: "13:30", coefficient: {underlying: 0.03538695}}
      |          {from: "14:00", coefficient: {underlying: 0.031781642}}
      |          {from: "14:30", coefficient: {underlying: 0.036847812}}
      |          {from: "15:00", coefficient: {underlying: 0.033405794}}
      |          {from: "15:30", coefficient: {underlying: 0.029270989}}
      |
      |          {from: "16:00", coefficient: {underlying: 0.028403115}}
      |          {from: "16:30", coefficient: {underlying: 0.026248341}}
      |          {from: "17:00", coefficient: {underlying: 0.026776616}}
      |          {from: "17:30", coefficient: {underlying: 0.031435015}}
      |          {from: "18:00", coefficient: {underlying: 0.025917103}}
      |          {from: "18:30", coefficient: {underlying: 0.024845937}}
      |          {from: "19:00", coefficient: {underlying: 0.025886779}}
      |          {from: "19:30", coefficient: {underlying: 0.02733871}}
      |
      |          {from: "20:00", coefficient: {underlying: 0.026674604}}
      |          {from: "20:30", coefficient: {underlying: 0.02592678}}
      |          {from: "21:00", coefficient: {underlying: 0.026529876}}
      |          {from: "21:30", coefficient: {underlying: 0.023011073}}
      |          {from: "22:00", coefficient: {underlying: 0.021250605}}
      |          {from: "22:30", coefficient: {underlying: 0.020291782}}
      |          {from: "23:00", coefficient: {underlying: 0.0184854}}
      |          {from: "23:30", coefficient: {underlying: 0.013914675}}
      |        ]
      |      }
      |      {
      |        category: {type: homeentertainment}
      |        sourceloads: [
      |          {from: "00:00", coefficient: {underlying: 0.009}}
      |          {from: "00:30", coefficient: {underlying: 0.007}}
      |          {from: "01:00", coefficient: {underlying: 0.005}}
      |          {from: "01:30", coefficient: {underlying: 0.002}}
      |          {from: "02:00", coefficient: {underlying: 0.001}}
      |          {from: "02:30", coefficient: {underlying: 0}}
      |
      |          {from: "05:00", coefficient: {underlying: 0.002}}
      |          {from: "05:30", coefficient: {underlying: 0.005}}
      |          {from: "06:00", coefficient: {underlying: 0.007}}
      |          {from: "06:30", coefficient: {underlying: 0.01}}
      |          {from: "07:00", coefficient: {underlying: 0.013}}
      |          {from: "07:30", coefficient: {underlying: 0.017}}
      |
      |          {from: "08:00", coefficient: {underlying: 0.024}}
      |          {from: "08:30", coefficient: {underlying: 0.032}}
      |          {from: "09:00", coefficient: {underlying: 0.034}}
      |          {from: "09:30", coefficient: {underlying: 0.036}}
      |          {from: "10:00", coefficient: {underlying: 0.034}}
      |          {from: "10:30", coefficient: {underlying: 0.032}}
      |          {from: "11:00", coefficient: {underlying: 0.028}}
      |          {from: "11:30", coefficient: {underlying: 0.024}}
      |
      |          {from: "12:00", coefficient: {underlying: 0.023}}
      |          {from: "12:30", coefficient: {underlying: 0.022}}
      |          {from: "13:00", coefficient: {underlying: 0.023}}
      |          {from: "13:30", coefficient: {underlying: 0.024}}
      |          {from: "14:00", coefficient: {underlying: 0.021}}
      |          {from: "14:30", coefficient: {underlying: 0.018}}
      |          {from: "15:30", coefficient: {underlying: 0.017}}
      |
      |          {from: "16:00", coefficient: {underlying: 0.018}}
      |          {from: "17:00", coefficient: {underlying: 0.021}}
      |          {from: "17:30", coefficient: {underlying: 0.024}}
      |          {from: "18:00", coefficient: {underlying: 0.033}}
      |          {from: "18:30", coefficient: {underlying: 0.043}}
      |          {from: "19:00", coefficient: {underlying: 0.052}}
      |          {from: "19:30", coefficient: {underlying: 0.061}}
      |
      |          {from: "20:00", coefficient: {underlying: 0.055}}
      |          {from: "20:30", coefficient: {underlying: 0.049}}
      |          {from: "21:00", coefficient: {underlying: 0.038}}
      |          {from: "21:30", coefficient: {underlying: 0.027}}
      |          {from: "22:00", coefficient: {underlying: 0.024}}
      |          {from: "22:30", coefficient: {underlying: 0.022}}
      |          {from: "23:00", coefficient: {underlying: 0.016}}
      |          {from: "23:30", coefficient: {underlying: 0.01}}
      |        ]
      |      }
      |      {
      |        category: {type: other}
      |        sourceloads: [
      |          {from: "00:00", coefficient: {underlying: 0.017148632}}
      |          {from: "00:30", coefficient: {underlying: 0.015338194}}
      |          {from: "01:00", coefficient: {underlying: 0.014708727}}
      |          {from: "01:30", coefficient: {underlying: 0.013848264}}
      |          {from: "02:00", coefficient: {underlying: 0.012831878}}
      |          {from: "02:30", coefficient: {underlying: 0.012609543}}
      |          {from: "03:00", coefficient: {underlying: 0.012303472}}
      |          {from: "03:30", coefficient: {underlying: 0.013267884}}
      |
      |          {from: "04:00", coefficient: {underlying: 0.012655742}}
      |          {from: "04:30", coefficient: {underlying: 0.012413196}}
      |          {from: "05:00", coefficient: {underlying: 0.012958926}}
      |          {from: "05:30", coefficient: {underlying: 0.01337472}}
      |          {from: "06:00", coefficient: {underlying: 0.014131235}}
      |          {from: "06:30", coefficient: {underlying: 0.016097596}}
      |          {from: "07:00", coefficient: {underlying: 0.01544503}}
      |          {from: "07:30", coefficient: {underlying: 0.015384393}}
      |
      |          {from: "08:00", coefficient: {underlying: 0.017217931}}
      |          {from: "08:30", coefficient: {underlying: 0.020833033}}
      |          {from: "09:00", coefficient: {underlying: 0.020131379}}
      |          {from: "09:30", coefficient: {underlying: 0.021453837}}
      |          {from: "10:00", coefficient: {underlying: 0.020922544}}
      |          {from: "10:30", coefficient: {underlying: 0.020209341}}
      |          {from: "11:00", coefficient: {underlying: 0.020602036}}
      |          {from: "11:30", coefficient: {underlying: 0.021710821}}
      |
      |          {from: "12:00", coefficient: {underlying: 0.0232585}}
      |          {from: "12:30", coefficient: {underlying: 0.021982242}}
      |          {from: "13:00", coefficient: {underlying: 0.022270988}}
      |          {from: "13:30", coefficient: {underlying: 0.021826319}}
      |          {from: "14:00", coefficient: {underlying: 0.023864867}}
      |          {from: "14:30", coefficient: {underlying: 0.023105465}}
      |          {from: "15:00", coefficient: {underlying: 0.022608821}}
      |          {from: "15:30", coefficient: {underlying: 0.024479896}}
      |
      |          {from: "16:00", coefficient: {underlying: 0.024739768}}
      |          {from: "16:30", coefficient: {underlying: 0.026925576}}
      |          {from: "17:00", coefficient: {underlying: 0.029997834}}
      |          {from: "17:30", coefficient: {underlying: 0.030269256}}
      |          {from: "18:00", coefficient: {underlying: 0.030191294}}
      |          {from: "18:30", coefficient: {underlying: 0.030159532}}
      |          {from: "19:00", coefficient: {underlying: 0.029264419}}
      |          {from: "19:30", coefficient: {underlying: 0.029576265}}
      |
      |          {from: "20:00", coefficient: {underlying: 0.029489641}}
      |          {from: "20:30", coefficient: {underlying: 0.028588753}}
      |          {from: "21:00", coefficient: {underlying: 0.027696528}}
      |          {from: "21:30", coefficient: {underlying: 0.027679203}}
      |          {from: "22:00", coefficient: {underlying: 0.024329748}}
      |          {from: "22:30", coefficient: {underlying: 0.021641522}}
      |          {from: "23:00", coefficient: {underlying: 0.019906158}}
      |          {from: "23:30", coefficient: {underlying: 0.018549051}}
      |        ]
      |      }
      |      {
      |        category: {type: heating}
      |        sourceloads: [
      |          {from: "00:00", coefficient: {underlying: 0.016}}
      |          {from: "01:00", coefficient: {underlying: 0.017}}
      |          {from: "01:30", coefficient: {underlying: 0.018}}
      |          {from: "02:00", coefficient: {underlying: 0.019}}
      |          {from: "03:00", coefficient: {underlying: 0.02}}
      |
      |          {from: "04:00", coefficient: {underlying: 0.021}}
      |          {from: "05:00", coefficient: {underlying: 0.024}}
      |          {from: "05:30", coefficient: {underlying: 0.026}}
      |          {from: "06:00", coefficient: {underlying: 0.031}}
      |          {from: "06:30", coefficient: {underlying: 0.036}}
      |          {from: "07:00", coefficient: {underlying: 0.038}}
      |          {from: "07:30", coefficient: {underlying: 0.04}}
      |
      |          {from: "08:00", coefficient: {underlying: 0.038}}
      |          {from: "08:30", coefficient: {underlying: 0.036}}
      |          {from: "09:00", coefficient: {underlying: 0.033}}
      |          {from: "09:30", coefficient: {underlying: 0.029}}
      |          {from: "10:00", coefficient: {underlying: 0.026}}
      |          {from: "10:30", coefficient: {underlying: 0.023}}
      |          {from: "11:00", coefficient: {underlying: 0.021}}
      |          {from: "11:30", coefficient: {underlying: 0.02}}
      |
      |          {from: "12:00", coefficient: {underlying: 0.018}}
      |          {from: "12:30", coefficient: {underlying: 0.016}}
      |          {from: "13:00", coefficient: {underlying: 0.015}}
      |          {from: "14:00", coefficient: {underlying: 0.014}}
      |          {from: "15:30", coefficient: {underlying: 0.015}}
      |
      |          {from: "16:00", coefficient: {underlying: 0.016}}
      |          {from: "16:30", coefficient: {underlying: 0.018}}
      |          {from: "17:00", coefficient: {underlying: 0.019}}
      |          {from: "17:30", coefficient: {underlying: 0.02}}
      |          {from: "18:30", coefficient: {underlying: 0.019}}
      |          {from: "19:00", coefficient: {underlying: 0.017}}
      |          {from: "19:30", coefficient: {underlying: 0.016}}
      |        ]
      |      }
      |      {
      |        category: {type: waterheating}
      |        sourceloads: [
      |          {from: "00:00", coefficient: {underlying: 0.012}}
      |          {from: "00:30", coefficient: {underlying: 0.011}}
      |          {from: "01:00", coefficient: {underlying: 0.009}}
      |          {from: "01:30", coefficient: {underlying: 0.007}}
      |          {from: "02:00", coefficient: {underlying: 0.006}}
      |
      |          {from: "04:30", coefficient: {underlying: 0.005}}
      |          {from: "05:00", coefficient: {underlying: 0.008}}
      |          {from: "05:30", coefficient: {underlying: 0.01}}
      |          {from: "06:00", coefficient: {underlying: 0.017}}
      |          {from: "06:30", coefficient: {underlying: 0.024}}
      |          {from: "07:00", coefficient: {underlying: 0.03}}
      |          {from: "07:30", coefficient: {underlying: 0.036}}
      |
      |          {from: "08:00", coefficient: {underlying: 0.037}}
      |          {from: "09:00", coefficient: {underlying: 0.036}}
      |          {from: "09:30", coefficient: {underlying: 0.035}}
      |          {from: "10:00", coefficient: {underlying: 0.034}}
      |          {from: "10:30", coefficient: {underlying: 0.033}}
      |          {from: "11:00", coefficient: {underlying: 0.031}}
      |          {from: "11:30", coefficient: {underlying: 0.029}}
      |
      |          {from: "12:00", coefficient: {underlying: 0.026}}
      |          {from: "12:30", coefficient: {underlying: 0.024}}
      |          {from: "13:00", coefficient: {underlying: 0.023}}
      |          {from: "13:30", coefficient: {underlying: 0.022}}
      |          {from: "14:00", coefficient: {underlying: 0.02}}
      |          {from: "14:30", coefficient: {underlying: 0.019}}
      |          {from: "15:00", coefficient: {underlying: 0.018}}
      |
      |          {from: "16:00", coefficient: {underlying: 0.019}}
      |          {from: "16:30", coefficient: {underlying: 0.02}}
      |          {from: "17:00", coefficient: {underlying: 0.022}}
      |          {from: "17:30", coefficient: {underlying: 0.024}}
      |          {from: "18:00", coefficient: {underlying: 0.025}}
      |          {from: "18:30", coefficient: {underlying: 0.026}}
      |          {from: "19:00", coefficient: {underlying: 0.027}}
      |          {from: "19:30", coefficient: {underlying: 0.029}}
      |
      |          {from: "20:00", coefficient: {underlying: 0.027}}
      |          {from: "20:30", coefficient: {underlying: 0.025}}
      |          {from: "21:00", coefficient: {underlying: 0.024}}
      |          {from: "21:30", coefficient: {underlying: 0.022}}
      |          {from: "22:00", coefficient: {underlying: 0.02}}
      |          {from: "22:30", coefficient: {underlying: 0.019}}
      |          {from: "23:00", coefficient: {underlying: 0.016}}
      |          {from: "23:30", coefficient: {underlying: 0.013}}
      |        ]
      |      }
      |    ]
      |  }
      """.stripMargin

  val summerTemp: String = """
  |  summer: {
  |  loaddistributions: [
  |    {
  |      category: {type: alwayson}
  |      sourceloads: [
  |        {from: "00:00", coefficient: {underlying: 2.083333333}}
  |      ]
  |    }
  |    {
  |      category: {type: refrigeration}
  |      sourceloads: [
  |        {from: "00:00", coefficient: {underlying: 0.02054312}}
  |        {from: "00:30", coefficient: {underlying: 0.020242535}}
  |      ]
  |    }
  |  ]
  |  }
  """.stripMargin
  val timeOfDayConfig: Config = parseString(
    """
      |pld.timeOfDay {
      |  distributions: [
      |    {
      |      profiletype: {commodity: {type: electric}},
      |
      |    """.stripMargin +
            electricitySimplifiedActivitySummerLoadDistributions +
            electricitySimplifiedActivityWinterLoadDistributions +
    """
      |      summerallocation: {
      |        january: { percent: 10 }
      |        february: { percent: 25 }
      |        march: { percent: 50 }
      |        april: { percent: 75 }
      |        may: { percent: 90 }
      |        june: { percent: 100 }
      |        july: { percent: 90 }
      |        august: { percent: 75 }
      |        september: { percent: 50 }
      |        october: { percent: 25 }
      |        november: { percent: 10 }
      |        december: { percent: 0 }
      |      }
      |    }
      |  ]
      |}""".stripMargin)

  val timeOfDayConfigLoader: TimeOfDayLoadFromConfig = TimeOfDayLoadFromConfig(timeOfDayConfig)

  /**
    * electricity_simplified_activity.csv
      category,elecHeating_elecHotWater,elecHeating_noElecHotWater,noElecHeating_elecHotWater,noElecHeating_noElecHotWater
      always_on, 0, 0, 0, 0
      refrigeration, 2.857142857, 3.333333333, 4, 5
      cooking, 11.42857143, 13.33333333, 16, 20
      lighting, 8.571428571, 10, 12, 15
      laundry_dishwashing, 11.42857143, 13.33333333, 16, 20
      home_entertainment, 11.42857143, 13.33333333, 16, 20
      other, 11.42857143, 13.33333333, 16, 20
      heating, 28.57142857, 33.33333333, 0, 0
      water_heating, 14.28571429, 0, 20, 0
    */

  val electricitySimplifiedActivity: Config = parseString(
    """
      |pld {
      |  simplified {
      |    defaultWeights: [
      |     {
      |      profileType: {commodity: {type: electric}}
      |      allowHas: [
      |          {type: electricheating}
      |          {type: electrichotwater}
      |        ]
      |      profile: [
      |        {
      |          has: [
      |            {type: electricheating}
      |            {type: electrichotwater}
      |          ]
      |          categoryWeights: [
      |            {category: {type: alwayson}, default: {underlying: 0}}
      |            {category: {type: refrigeration}, default: {underlying: 2.857142857}}
      |            {category: {type: cooking}, default: {underlying: 11.42857143}}
      |            {category: {type: lighting}, default: {underlying: 8.571428571}}
      |            {category: {type: laundrydishwashing}, default: {underlying: 11.42857143}}
      |            {category: {type: homeentertainment}, default: {underlying: 11.42857143}}
      |            {category: {type: other}, default: {underlying: 11.42857143}}
      |            {category: {type: heating}, default: {underlying: 28.57142857}}
      |            {category: {type: waterheating}, default: {underlying: 14.28571429}}
      |          ]
      |        }
      |        {
      |          has: [
      |            {type: electricheating}
      |          ]
      |          categoryWeights: [
      |            {category: {type: alwayson}, default: {underlying: 0}}
      |            {category: {type: refrigeration}, default: {underlying: 3.333333333}}
      |            {category: {type: cooking}, default: {underlying: 13.33333333}}
      |            {category: {type: lighting}, default: {underlying: 10}}
      |            {category: {type: laundrydishwashing}, default: {underlying: 13.33333333}}
      |            {category: {type: homeentertainment}, default: {underlying: 13.33333333}}
      |            {category: {type: other}, default: {underlying: 13.33333333}}
      |            {category: {type: heating}, default: {underlying: 33.33333333}}
      |            {category: {type: waterheating}, default: {underlying: 0}}
      |          ]
      |        }
      |        {
      |          has: [
      |            {type: electrichotwater}
      |          ]
      |          categoryWeights: [
      |            {category: {type: alwayson}, default: {underlying: 0}}
      |            {category: {type: refrigeration}, default: {underlying: 4}}
      |            {category: {type: cooking}, default: {underlying: 16}}
      |            {category: {type: lighting}, default: {underlying: 12}}
      |            {category: {type: laundrydishwashing}, default: {underlying: 16}}
      |            {category: {type: homeentertainment}, default: {underlying: 16}}
      |            {category: {type: other}, default: {underlying: 16}}
      |            {category: {type: heating}, default: {underlying: 0}}
      |            {category: {type: waterheating}, default: {underlying: 20}}
      |          ]
      |        }
      |        {
      |          has: [
      |          ]
      |          categoryWeights: [
      |            {category: {type: alwayson}, default: {underlying: 0}}
      |            {category: {type: refrigeration}, default: {underlying: 5}}
      |            {category: {type: cooking}, default: {underlying: 20}}
      |            {category: {type: lighting}, default: {underlying: 15}}
      |            {category: {type: laundrydishwashing}, default: {underlying: 20}}
      |            {category: {type: homeentertainment}, default: {underlying: 20}}
      |            {category: {type: other}, default: {underlying: 20}}
      |            {category: {type: heating}, default: {underlying: 0}}
      |            {category: {type: waterheating}, default: {underlying: 0}}
      |          ]
      |        }
      |      ]
      |     }
      |    ]
      |  }
      |}
      |
    """.stripMargin)

  val config: Config =
    electricitySimplifiedActivitySeasonalAdjustments
      .withFallback(electricitySimplifiedActivityMinMaxLoads)
      .withFallback(timeOfDayConfig)

  lazy val pldConfigurationBuilder: PldConfigurationBuilder =
    PldConfigurationBuilder(electricitySimplifiedActivity.getConfig("pld.simplified"), config)
}
