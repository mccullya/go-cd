package com.onzo.pipeline.datascience.pld

import java.io.File

import com.onzo.pipeline.config.Parser
import com.onzo.pipeline.config.pld._
import com.typesafe.config.ConfigFactory
import org.scalactic.TypeCheckedTripleEquals
import org.scalatest.{Matchers, WordSpec}

class ScottishPowerConfigSpec extends WordSpec with Matchers with TypeCheckedTripleEquals {

  val basePath = "src/test/resources/datascience/scottishpower/"

  "The pipeline config parser" when {
    "parsing SP data from config files" should {

      "parse the Category Limits config" in {
        val config = ConfigFactory.parseFile(new File(basePath, "electricity_eu_activity_min_max_loads.conf"))
        val fromConfig = CategoryLimitFromConfig(config)

        fromConfig.categoryLimitConfig shouldBe a[Right[_, _]]
      }

      "parse the elec seasonal adjustments config" in {
        import pureconfig.module.squants._ // don't remove, implicit needed for parsing

        val config = ConfigFactory.parseFile(new File(basePath, "electricity_eu_activity_seasonal_adjustments.conf"))
        val fromConfig = SeasonalAdjustmentFromConfig(config)

        fromConfig.seasonalConfig shouldBe a[Right[_, _]]
      }

      "parse the summer time-of-day Load Distributions config" in {
        import TimeOfDayLoadFromConfig._ // implicit needed for parsing
        val config = ConfigFactory.parseFile(new File(basePath, "electricity_eu_activity_load_distributions.conf"))
        val fromConfig = TimeOfDayLoadFromConfig(config)

        fromConfig.timeOfDayConfig shouldBe a[Right[_, _]]
      }

      "parse the Default Weights config" in {
        import com.onzo.pipeline.config.Parser.camelCaseHint // implicit needed for parsing

        val config = ConfigFactory.parseFile(new File(basePath, "electricity_eu_default_weights.conf")).getConfig("pld.europe")
        val fromConfig = Parser.parse[DefaultWeightConfigurationNelWrapUnsafe](config).unwrap

        fromConfig shouldBe a[Right[_, _]]
      }

    }
  }
}
