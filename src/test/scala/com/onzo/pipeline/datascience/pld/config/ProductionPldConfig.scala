package com.onzo.pipeline.datascience.pld.config

import java.io.File

import com.onzo.pipeline.config.pld.PldConfigurationBuilder
import com.typesafe.config.{Config, ConfigFactory}

/**
  * Created by toby.hobson on 26/04/2017.
  */
object ProductionPldConfig {

  val prodPldConf: Config = ConfigFactory.parseFile(new File("src/main/resources/pld.conf"))

  def prodPldConfigurationBuilder(region: String): PldConfigurationBuilder =
    PldConfigurationBuilder(prodPldConf.getConfig(region), prodPldConf)
}
