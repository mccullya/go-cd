package com.onzo.pipeline

import com.onzo.pipeline.config.Parser.camelCaseHint
import com.onzo.pipeline.config._
import org.scalatest.{BeforeAndAfterAll, Inside, WordSpec}

/**
  * Created by toby.hobson on 17/03/2017.
  */
class ClientConfigSpec extends WordSpec with BeforeAndAfterAll with Inside {

  "given a client config file for onzo and greenchoice" when {

    "the config is loaded it" should {
      val config = ModularConfiguration.clientConfig
      val clients = Pipeline.getClients(config)

      "return config for 2 clients" in {
        assert(clients.length == 2)
      }

    }

    "the onzo config" should {
      val config = ModularConfiguration.clientConfig
      val clients = Pipeline.getClients(config)

      "include a valid id, name, and timestamp pattern" in {
        val maybeOnzo = clients.find(_.name == "onzo")
        assert(maybeOnzo.nonEmpty)

        inside(maybeOnzo) {
          case Some(ClientConfig(id, name, timestampPattern, _, _)) =>
            withClue("id") {
              assert(id == 1)
            }
            withClue("name") {
              assert(name == "onzo")
            }
            withClue("timestampPattern") {
              assert(timestampPattern == "yyyy-MM-dd'T'HH:mm:ssZ")
            }
        }
      }

      "include column mappings" in {
        val maybeOnzo = clients.find(_.name == "onzo")
        assert(maybeOnzo.map(_.columnMappings).nonEmpty)
      }

      "the column mappings should include a valid electric consumption mapping" in {
        val maybeElectricMapping = clients.find(_.name == "onzo").map(_.columnMappings).map(_.elecConsumption)
        withClue("electric mapping is present") { assert(maybeElectricMapping.nonEmpty) }

        inside(maybeElectricMapping) {
          case Some(ElecConsumptionColumnMapping(sensorId, timestamp, cumulativeEnergy)) =>
            withClue("sensor id index == A") { assert("A" == sensorId) }
            withClue("timestamp index == B") { assert("B" == timestamp) }
            withClue("cumulative energy index == C") { assert("C" == cumulativeEnergy) }
        }
      }

      "the column mappings should include a valid gas consumption mapping" in {
        val maybeGasMapping = clients.find(_.name == "onzo").map(_.columnMappings).map(_.gasConsumption)
        withClue("gas mapping is present") { assert(maybeGasMapping.nonEmpty) }

        inside(maybeGasMapping) {
          case Some(GasConsumptionColumnMapping(sensorId, timestamp, cumulativeVolume)) =>
            withClue("sensor id index == A") { assert("A" == sensorId) }
            withClue("timestamp index == B") { assert("B" == timestamp) }
            withClue("cumulative volume index == C") { assert("C" == cumulativeVolume) }
        }
      }

      "the column mappings should include a valid electric csr mapping" in {
        val maybeCsrMapping = clients.find(_.name == "onzo").map(_.columnMappings).flatMap(_.electricCsr)
        withClue("electric csr mapping is present") { assert(maybeCsrMapping.nonEmpty) }

        inside(maybeCsrMapping) {
          case Some(ElectricCsrColumnMapping(
            householdId, sensorId, status,
            standingCharge, unitRate, tags, zipPostalCode, latitude, longitude)) =>
            withClue("household id index == A") { assert("A" == householdId) }
            withClue("sensor id index == F") { assert("F" == sensorId) }
            withClue("status == B") { assert("B" == status) }
            withClue("standingCharge == G") { assert("G" == standingCharge) }
            withClue("unitRate == H") { assert("H" == unitRate) }
            withClue("tags == O") { assert("O" == tags) }
            withClue("zipPostalCode == Some(P)") { assert(zipPostalCode.contains("P")) }
            withClue("latitude ==None") { assert(latitude.isEmpty) }
            withClue("longitude == None") { assert(longitude.isEmpty) }
        }
      }

      "the column mappings should include a valid gas csr mapping" in {
        val maybeCsrMapping = clients.find(_.name == "onzo").map(_.columnMappings).flatMap(_.gasCsr)
        withClue("gas csr mapping is present") { assert(maybeCsrMapping.nonEmpty) }

        inside(maybeCsrMapping) {
          case Some(GasCsrColumnMapping(
          householdId, sensorId, status, standingCharge, unitRate,
          calorificValue, tags, zipPostalCode, latitude, longitude)) =>
            withClue("household id index == A") { assert("A" == householdId) }
            withClue("sensor id index == J") { assert("J" == sensorId) }
            withClue("status == C") { assert("C" == status) }
            withClue("standingCharge == K") { assert("K" == standingCharge) }
            withClue("unitRate == L") { assert("L" == unitRate) }
            withClue("calorificValue == N") { assert("N" == calorificValue) }
            withClue("tags == O") { assert("O" == tags) }
            withClue("zipPostalCode == Some(P)") { assert(zipPostalCode.contains("P")) }
            withClue("latitude ==None") { assert(latitude.isEmpty) }
            withClue("longitude == None") { assert(longitude.isEmpty) }
        }
      }
    }
  }
}
