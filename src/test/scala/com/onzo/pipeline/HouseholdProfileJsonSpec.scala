package com.onzo.pipeline

import com.onzo.pipeline.domain.{HouseholdProfile, HouseholdProfileJsonProtocol}
import org.scalatest.FunSuite
import spray.json._

/**
  * Created by toby.hobson on 24/03/2017.
  */
class HouseholdProfileJsonSpec extends FunSuite with HouseholdProfileJsonProtocol {

  test("marshall a complete profile") {
    val profile = HouseholdProfile(
      houseType = Some("detached"),
      numberOfPeople = Some(1),
      peoplePlus = Some(false),
      numberOfBedrooms = Some(2),
      bedroomsPlus = Some(false),
      // Electric
      electricHeating = Some(false),
      topUpElectricHeating = Some(true),
      electricHotWater = Some(false),
      electricCooking = Some(true),
      topUpElectricCooking = Some(false),
      // Gas
      gasHeating = Some(true),
      gasCooking = Some(false),
      gasHotWater = Some(true),
      // Cooling
      compressiveCooling = Some(false),
      fanCooling = Some(false)
    )
    val json = profile.toJson
    val text = json.compactPrint
    assert(text == """{"gas_hot_water":true,"top_up_electric_heating":true,"house_type":"detached","number_of_bedrooms":2,"bedrooms_plus":false,"top_up_electric_cooking":false,"people_plus":false,"gas_cooking":false,"electric_cooking":true,"electric_heating":false,"compressive_cooling":false,"fan_cooling":false,"gas_heating":true,"electric_hot_water":false,"number_of_people":1}""")
  }

  test("un-marshall a complete profile") {
    val expectedProfile = HouseholdProfile(
      houseType = Some("detached"),
      numberOfPeople = Some(1),
      peoplePlus = Some(false),
      numberOfBedrooms = Some(2),
      bedroomsPlus = Some(false),
      // Electric
      electricHeating = Some(false),
      topUpElectricHeating = Some(true),
      electricHotWater = Some(false),
      electricCooking = Some(true),
      topUpElectricCooking = Some(false),
      // Gas
      gasHeating = Some(true),
      gasCooking = Some(false),
      gasHotWater = Some(true),
      // Cooling
      compressiveCooling = Some(false),
      fanCooling = Some(false)
    )

    val text = """{"gas_hot_water":true,"top_up_electric_heating":true,"house_type":"detached","number_of_bedrooms":2,"bedrooms_plus":false,"top_up_electric_cooking":false,"people_plus":false,"gas_cooking":false,"electric_cooking":true,"electric_heating":false,"compressive_cooling":false,"fan_cooling":false,"gas_heating":true,"electric_hot_water":false,"number_of_people":1}"""
    val json = text.parseJson
    val actualProfile = json.convertTo[HouseholdProfile]
    assert(actualProfile == expectedProfile)
  }

  test("marshall a partial profile") {
    val profile = HouseholdProfile(
      houseType = Some("detached"),
      numberOfPeople = None,
      peoplePlus = None,
      numberOfBedrooms = None,
      bedroomsPlus = None,
      // Electric
      electricHeating = None,
      topUpElectricHeating = None,
      electricHotWater = None,
      electricCooking = None,
      topUpElectricCooking = None,
      // Gas
      gasHeating = None,
      gasCooking = None,
      gasHotWater = None,
      // Cooling
      compressiveCooling = None,
      fanCooling = None
    )
    val json = profile.toJson
    val text = json.compactPrint
    assert(text == """{"house_type":"detached"}""")
  }

  test("un-marshall a partial profile") {
    val expectedProfile = HouseholdProfile(
      houseType = Some("detached"),
      numberOfPeople = None,
      peoplePlus = None,
      numberOfBedrooms = None,
      bedroomsPlus = None,
      // Electric
      electricHeating = None,
      topUpElectricHeating = None,
      electricHotWater = None,
      electricCooking = None,
      topUpElectricCooking = None,
      // Gas
      gasHeating = None,
      gasCooking = None,
      gasHotWater = None,
      // Cooling
      compressiveCooling = None,
      fanCooling = None
    )
    val text = """{"house_type":"detached"}"""
    val json = text.parseJson
    val actualProfile = json.convertTo[HouseholdProfile]
    assert(actualProfile == expectedProfile)
  }

}
