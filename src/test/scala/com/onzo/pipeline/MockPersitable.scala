package com.onzo.pipeline

import java.util.concurrent.atomic.AtomicInteger

import com.onzo.pipeline.persistence.Persistable

import scala.util.{Failure, Success, Try}

class MockPersitable[T](simulateSuccess: Boolean) extends Persistable[T] {
  private val _successCount = new AtomicInteger(0)
  private val _failureCount = new AtomicInteger(0)

  override def persist(t: T): Try[Unit] = {
    if (simulateSuccess) {
      _successCount.addAndGet(1)
      Success()
    } else {
      _failureCount.addAndGet(1)
      Failure(new Exception("BOOM!"))
    }
  }

  def successCount: Int = _successCount.intValue()

  def failureCount: Int = _failureCount.intValue()
}
