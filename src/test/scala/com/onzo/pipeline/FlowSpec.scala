package com.onzo.pipeline

import java.util.concurrent.Executors

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, ActorMaterializerSettings}
import akka.stream.scaladsl.Source
import com.onzo.collector.domain.ElectricSensorReading
import com.onzo.collector.parser.ParseError
import com.onzo.common.domain.{ClientId, HouseholdId}
import com.onzo.pipeline.TestData._
import com.onzo.pipeline.config.pld.PldConfigSpec.pldConfigurationBuilder
import com.onzo.pipeline.config.{ClientColumnMappings, ClientConfig, ElecConsumptionColumnMapping, GasConsumptionColumnMapping}
import com.onzo.pipeline.domain._
import com.onzo.pipeline.utils.IntervalGenerator
import org.scalatest.{AsyncFunSuite, BeforeAndAfterAll}

import scala.concurrent.ExecutionContext

class FlowSpec extends AsyncFunSuite with BeforeAndAfterAll with ExceptionLoggingDecider {

  private implicit val system = ActorSystem("EndToEndSpec")
  private implicit val mat = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(decider))
  // Multi threaded tests are notoriously difficult and non deterministic
  private val executor = Executors.newSingleThreadScheduledExecutor()
  private implicit val ec: ExecutionContext = ExecutionContext.fromExecutor(executor)

  implicit val clientConfig = ClientConfig(
    2,
    "greenchoice",
    "yyyy-MM-dd'T'HH:mm:ssZ",
    ClientColumnMappings(
      1,
      ElecConsumptionColumnMapping("A", "B", "C"),
      GasConsumptionColumnMapping("A", "B", "C"),
      None,
      None
    ),
    pldConfigurationBuilder
  )

  ignore("10 sensors should result in a successful count of 10") {
    val it = IntervalGenerator.generateRecords(10, 1, IntervalGenerator.generateGoodReading)
    val source = mockedSource(it)
    val consumptionPersister = new MockPersitable[WindowedElectricConsumption](true)
    val breakdownPersister = new MockPersitable[DailyElectricBreakdown](true)
    val findProfileById = (_: ClientId, _: HouseholdId) =>  HouseholdProfileEntity(clientZero, dummyHouseholdId, HouseholdProfile.empty.copy(electricHeating = Some(true), electricHotWater = Some(true)))
    val mockCsrActor = new CsrActorRef(system.actorOf(MockCsrActor.props, "MockCsrActor1"))
    val mockEtlActor = new EtlActorRef(system.actorOf(MockEtlActor.props, "MockEtlActor1"))
    val etlStatusActorRef = new EtlStatusActorRef(system.actorOf(EtlStatusActor.props))
    val flow = Flows.electricConsumptionGraph()(clientConfig, ec, system.scheduler, consumptionPersister, breakdownPersister, findProfileById, mockCsrActor, mockEtlActor, etlStatusActorRef)
    val eventualCount = source.via(flow).runFold(0)(_ + _)
    eventualCount.map { count =>
      // Nasty, but we need to wait for the persisters to finish their work
      val totalConsumptionSaves = consumptionPersister.successCount
      val totalBreakdownsSaves = breakdownPersister.successCount
      withClue("Total successfully processed")(assert(count == 10))
      withClue("Total consumptions saved")(assert(totalConsumptionSaves == 10))
      withClue("Total breakdowns saved")(assert(totalBreakdownsSaves == 10))
    }
  }

 ignore("100 days historic days for 10 users") {
    val it = IntervalGenerator.generateRecords(10, 100, IntervalGenerator.generateGoodReading)
    val source = mockedSource(it)
    val consumptionPersister = new MockPersitable[WindowedElectricConsumption](true)
    val breakdownPersister = new MockPersitable[DailyElectricBreakdown](true)
    val findProfileById = (_: ClientId, _: HouseholdId) =>  HouseholdProfileEntity(clientZero, dummyHouseholdId, HouseholdProfile.empty.copy(electricHeating = Some(true), electricHotWater = Some(true)))
    val mockCsrActor = new CsrActorRef(system.actorOf(MockCsrActor.props))
    val mockEtlActor = new EtlActorRef(system.actorOf(MockEtlActor.props))
    val etlStatusActorRef = new EtlStatusActorRef(system.actorOf(EtlStatusActor.props))
    val flow = Flows.electricConsumptionGraph()(clientConfig, ec, system.scheduler, consumptionPersister, breakdownPersister, findProfileById, mockCsrActor, mockEtlActor, etlStatusActorRef)
    val eventualCount = source.via(flow).runFold(0)(_ + _)
    eventualCount.map { count =>
      val totalConsumptionSaves = consumptionPersister.successCount
      val totalBreakdownsSaves = breakdownPersister.successCount
      withClue("Total successfully processed")(assert(count == 1000))
      withClue("Total consumptions saved")(assert(totalConsumptionSaves == 1000))
      withClue("Total breakdowns saved")(assert(totalBreakdownsSaves == 1000))
    }
  }

  // TODO ATLAS-310 now ONZO-1056 & ONZO-1087 Gas
  test("Rerun and electric PLD for a specific user") {
    val source = Source.fromIterator(() => IntervalGenerator.generateWindowedElectric(1, 50))
    val breakdownPersister = new MockPersitable[DailyElectricBreakdown](true)
    val findProfileById = (_: ClientId, _: HouseholdId) =>  HouseholdProfileEntity(clientZero, dummyHouseholdId, HouseholdProfile.empty.copy(electricHeating = Some(true), electricHotWater = Some(true)))
    val flow = Flows.pldElectricGraph()(clientConfig, ec, breakdownPersister, findProfileById)
    val eventualCount = source.via(flow).runFold(0)(_ + _)
    eventualCount.map { count =>
      val totalBreakdownsSaves = breakdownPersister.successCount
      withClue("Total successfully processed")(assert(count == 50))
      withClue("Total breakdowns saved")(assert(totalBreakdownsSaves == 50))
    }
  }

  override protected def afterAll(): Unit = {
    mat.shutdown()
    system.terminate()
    executor.shutdown()
  }

  def mockedSource(it: Iterator[ElectricSensorReading]): Source[Either[ParseError, ElectricSensorReading], NotUsed] = {
    Source.fromIterator(() => it.map(Right.apply))
  }
}
