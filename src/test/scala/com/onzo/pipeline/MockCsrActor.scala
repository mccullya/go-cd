package com.onzo.pipeline

import akka.actor.{Actor, Props, Status}
import com.onzo.etl.persistence.csr.CsrActor.SaveCsr

object MockCsrActor {
  def props: Props = Props[MockCsrActor]
}

class MockCsrActor extends Actor {

  override def receive: Receive = {
    case SaveCsr(_, _) => sender() ! ()
    case _ => logError("MockCsrActor invalid message"); sender() ! Status.Failure(new Exception("Invalid record received"))
  }

}
