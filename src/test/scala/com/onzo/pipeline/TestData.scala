package com.onzo.pipeline

import java.time.LocalDate

import com.onzo.common.Commodity.Electric
import com.onzo.common.domain.{ClientId, HouseholdId}
import com.onzo.common.{Location, Place, ProfileType}
import com.onzo.pipeline.domain.HouseholdProfileEntity.GetProfileIdFn
import com.onzo.pipeline.domain._
import squants.space.Degrees

object TestData {
  object TestLocations {
    object OnzoHQ extends Location(Place("OnzoHQ"), Degrees(51.519570), Degrees(-0.168398))
  }
  val electricHouseholdProfile = ProfileType(Electric)
  val gasHouseholdProfile = ProfileType(Electric)

  val clientZero: ClientId = ClientId(0)
  val dummyHouseholdId: HouseholdId = HouseholdId("dummy test household Id")
  val findEmptyProfileById: GetProfileIdFn = (_: ClientId, _: HouseholdId) => HouseholdProfileEntity(clientZero, dummyHouseholdId, HouseholdProfile.empty)

  val runDay20161225 = RunDay(LocalDate.of(2016, 12, 25))
  val runDay20170227 = RunDay(LocalDate.of(2017, 2, 27))
}
