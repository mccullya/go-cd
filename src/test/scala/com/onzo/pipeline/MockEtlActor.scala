package com.onzo.pipeline

import akka.actor.{Actor, Props, Status}
import com.onzo.collector.domain.SensorReading
import com.onzo.common.domain._
import com.onzo.etl.core.Etl.EtlResult
import com.onzo.etl.core.domain.ConsumptionMeasurement.Energy
import com.onzo.etl.core.domain.{AggregateConsumption, _}
import com.onzo.etl.persistence.EtlActor.{EtlClientConfig, EtlResponse, ReadingWithClientConfig}
import squants.energy.KilowattHours
import squants.market.GBP

object MockEtlActor {
  def props: Props = Props[MockEtlActor]
}

class MockEtlActor extends Actor {

  private case class SensorDay(sensorId: SensorId, day: Long)

  private val state = collection.mutable.Map.empty[SensorDay, Seq[SensorReading]]

  override def receive: Receive = {
    case ReadingWithClientConfig(sensorReading: SensorReading, clientConfig: EtlClientConfig) =>
      val zone = sensorReading.timestamp.getZone
      val day = sensorReading.timestamp.toLocalDate.atStartOfDay(zone).toInstant.toEpochMilli
      val sensorDay = SensorDay(sensorReading.sensorId, day)

      val oldState = state.getOrElse(sensorDay, Seq.empty)
      val currentState = sensorReading +: oldState
      state.update(sensorDay, currentState)

      val etlResult: EtlResult = if (currentState.size == 96) {
        val sorted = currentState.sortBy(_.timestamp.toInstant.toEpochMilli)
        val windowStart = sorted.head.timestamp
        val windowEnd = sorted.last.timestamp
        val householdId = HouseholdId(sensorReading.sensorId.value)
        val consumption = currentState.map(sensorReading =>
          Consumption.Electric(
            OnzoDeviceId(sensorReading.sensorId.value),
            sensorReading.timestamp, clientConfig.resolution, Energy(KilowattHours(1)),
            GBP,
            CostPerUnit(1), DailyStandingCharge(1)
          )
        )
        val aggregateConsumption = AggregateConsumption.fromConsumption(Commodity.Electric, consumption).right.get
        val window = Window(windowStart, windowEnd)
        Seq(SensorWindow(aggregateConsumption.within(window).size, window, aggregateConsumption, householdId))
      } else Seq.empty

      sender() ! EtlResponse(etlResult)

    case _ => logError("MockEtlActor invalid message"); sender() ! Status.Failure(new Exception("Invalid record received"))
  }
}
