package com.onzo.pipeline.config.pld

import java.time.ZonedDateTime

import com.onzo.pipeline.TestData._
import com.onzo.pipeline.TestData.TestLocations.OnzoHQ
import com.onzo.common.Commodity.Electric
import com.onzo.pipeline.config.pld.SeasonalAdjustmentFromConfig._
import com.onzo.pipeline.config.pld.SeasonalAdjustmentFromConfigSpec._
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.config.ConfigFactory.parseString
import org.scalatest.{Matchers, WordSpec}
import com.onzo.common.{Location, ProfileType}
import com.onzo.common.Month.{August, February, January}
import com.onzo.pipeline.config.ModularConfiguration
import com.onzo.pipeline.domain.RunDay
import pld.data.ConsumptionCategory.{Heating, Lighting}
import pld.data.input.SeasonalAdjustment.SeasonalCoefficient
import pureconfig.loadConfig
import pureconfig.module.squants._

class SeasonalAdjustmentFromConfigSpec extends WordSpec with Matchers {

  "a seasonal adjustment from config" when {
    "parsing a valid config object" should {
      "successfully decode the config with pureconfig error type" in {
        seasonalAdjustmentConfigLoader.seasonalConfig shouldBe a[Right[_, _]]
      }
      "successfully decode the config with pld error type" in {
        seasonalAdjustmentConfigLoader.pldSeasonalConfig shouldBe a[Right[_, _]]
      }
      "have retrieved the February values" in {
        seasonalAdjustmentConfigLoader.pldSeasonalConfig map { a =>
          val onzoHQCoefficients = a.locationcoefficients.find(_.location == OnzoHQ)
          onzoHQCoefficients map { b =>
            b.coefficients.find(_.month == February) shouldBe
              Some(CoefficientMonth(February,
                List(CategoryCoefficient(Lighting, SeasonalCoefficient(1.1)),
                  CategoryCoefficient(Heating, SeasonalCoefficient(2.0)))))
          }
        }
      }
      "have retrieved 12 months of data" in {
        seasonalAdjustmentConfigLoader.pldSeasonalConfig map { a =>
          a.locationcoefficients.find(_.location == OnzoHQ).
            map { b =>
              b.coefficients.size shouldBe 12
            }
        }
      }
    }
  }

  "a seasonal adjustment" when {
    "loading a valid config" should {
      val config = ModularConfiguration.clientConfig
      val seasonalAdjustmentConfigLoader = SeasonalAdjustmentFromConfig(config)

      "successfully decode the config with pureconfig error type" in {
        seasonalAdjustmentConfigLoader.seasonalConfig shouldBe a[Right[_, _]]
      }
      "successfully decode the config with pld error type" in {
        seasonalAdjustmentConfigLoader.pldSeasonalConfig shouldBe a[Right[_, _]]
      }
      "have retrieved the August values" in {
        seasonalAdjustmentConfigLoader.pldSeasonalConfig map { a =>
          a.locationcoefficients.find(_.location == OnzoHQ) map { b =>
            b.coefficients.find(_.month == August) shouldBe
              Some(CoefficientMonth(August,
                List(CategoryCoefficient(Lighting, SeasonalCoefficient(0.9)),
                  CategoryCoefficient(Heating, SeasonalCoefficient(0)))))
          }
        }
      }
      "have retrieved 12 months of data" in {
        seasonalAdjustmentConfigLoader.pldSeasonalConfig map { a =>
          a.locationcoefficients.find(_.location == OnzoHQ) map { b =>
            b.coefficients.size shouldBe 12
          }
        }
      }
    }
  }

  "the month from run day lookup" when {
    "looking up a day in February" should {
      "return February" in {
        monthFromRunDay(dayInFeb) shouldBe February
      }
    }
  }

  "the coefficients from month lookup" when {
    "looking up some coefficients from a month" should {
      "return the expected list of coefficients" in {
        coefficientsForMonth(monthA, coefficientLocationA) shouldBe coefficientMonthA
      }
    }
    "looking up some coefficients from a month when there are none defined" should {
      "return and empty list of coefficients" in {
        val month = February
        coefficientsForMonth(month, coefficientLocationA) shouldBe CoefficientMonth(month, List())
      }
    }
  }

  "the category coefficient for a month lookup" when {
    "looking up a category that has a coefficient" should {
      "return the coefficient" in {
        monthlyCoefficientForCategory(categoryA, coefficientMonthA) shouldBe
          CategoryCoefficient(categoryA, seasonalCoefficientA)
      }
    }
    "looking up a category that has no coefficient" should {
      "return a coefficient of 1" in {
        val category = Lighting
        monthlyCoefficientForCategory(category, coefficientMonthA) shouldBe
          CategoryCoefficient(category, SeasonalCoefficient(1.0))
      }
    }
  }

  "the seasonal adjustment function running on configA" when {
    "looking up heating for February" should {
      val r = seasonalAdjustmentConfigLoader.oldSeasonalAdjustment(Heating, OnzoHQ, dayInFeb, electricHouseholdProfile)
      "succeed" in {
        r shouldBe a [Right[_, _]]
      }
      "return 2" in {
        r map { a => a shouldBe CategoryCoefficient(Heating, SeasonalCoefficient(2.0)) }
      }
    }
  }

  "a simple config containing a location" when {
    "parsed as a config" should {
      "be a success" in {
        case class Foo(location: Location)
        val config = parseString(
          """{
            | location: {
            |   place: { name: OnzoHQ },
            |   north: 51.519570°,
            |   east: -0.168398°
            | }
            |}""".stripMargin)
        val c = loadConfig[Foo](config)
        c shouldBe a[Right[_, _]]
      }
    }
  }
}
object SeasonalAdjustmentFromConfigSpec {
  val locationA = OnzoHQ
  val monthA = January
  val categoryA = Heating
  val seasonalCoefficientA = SeasonalCoefficient(2.0)
  val coefficientsA = List(CategoryCoefficient(categoryA, seasonalCoefficientA))
  val coefficientMonthA = CoefficientMonth(monthA, coefficientsA)
  val electricProfile = ProfileType(Electric)
  val coefficientLocationA = CoefficientLocation(locationA, electricProfile, List(coefficientMonthA))
  val locationCoefficientsA = LocationCoefficient(List(coefficientLocationA))
  val dayInFeb = runDay20170227

  val configB: Config = parseString(
    """pld.seasonalAdjustment {
      |  locationcoefficients: [
      |    {
      |      location: {
      |        place: { name: OnzoHQ },
      |        north: 51.519570°,
      |        east: -0.168398°
      |      },
      |      profiletype: {commodity: {type: electric}},
      |      coefficients: [
      |         {
      |           month : { type: january },
      |           coefficient:
      |             [
      |               { category: { type: lighting }, coefficient : { value: 1.2 } },
      |               { category: { type: heating }, coefficient : { value: 3 } }
      |             ]
      |         },{
      |           month : { type: february },
      |           coefficient:
      |             [
      |               { category: { type: lighting }, coefficient : { value: 1.1 } },
      |               { category: { type: heating }, coefficient : { value: 2 } }
      |             ]
      |         },{
      |           month : { type: march },
      |           coefficient:
      |             [
      |               { category: { type: lighting }, coefficient : { value: 1 } },
      |               { category: { type: heating }, coefficient : { value: 0.5 } }
      |             ]
      |         },{
      |           month : { type: april },
      |           coefficient:
      |             [
      |               { category: { type: lighting }, coefficient : { value: 1 } },
      |               { category: { type: heating }, coefficient : { value: 0.25 } }
      |             ]
      |         },{
      |           month : { type: may },
      |           coefficient:
      |             [
      |               { category: { type: lighting }, coefficient : { value: 0.9 } },
      |               { category: { type: heating }, coefficient : { value: 0 } }
      |             ]
      |         },{
      |           month : { type: june },
      |           coefficient:
      |             [
      |               { category: { type: lighting }, coefficient : { value: 0.8 } },
      |               { category: { type: heating }, coefficient : { value: 0 } }
      |             ]
      |         },{
      |           month : { type: july },
      |           coefficient:
      |             [
      |               { category: { type: lighting }, coefficient : { value: 0.8 } },
      |               { category: { type: heating }, coefficient : { value: 0 } }
      |             ]
      |         },{
      |           month : { type: august },
      |           coefficient:
      |             [
      |               { category: { type: lighting }, coefficient : { value: 0.9 } },
      |               { category: { type: heating }, coefficient : { value: 0 } }
      |             ]
      |         },{
      |           month : { type: september },
      |           coefficient:
      |             [
      |               { category: { type: lighting }, coefficient : { value: 1 } },
      |               { category: { type: heating }, coefficient : { value: 0.5 } }
      |             ]
      |         },{
      |           month : { type: october },
      |           coefficient:
      |             [
      |               { category: { type: lighting }, coefficient : { value: 1 } },
      |               { category: { type: heating }, coefficient : { value: 0.75 } }
      |             ]
      |         },{
      |           month : { type: november },
      |           coefficient:
      |             [
      |               { category: { type: lighting }, coefficient : { value: 1.1 } },
      |               { category: { type: heating }, coefficient : { value: 2 } }
      |             ]
      |         },{
      |           month : { type: december },
      |           coefficient:
      |             [
      |               { category: { type: lighting }, coefficient : { value: 1.2 } },
      |               { category: { type: heating }, coefficient : { value: 3 } }
      |             ]
      |         }
      |      ]
      |    }
      |    {
      |      location: {
      |        place: { name: OnzoHQ },
      |        north: 51.519570°,
      |        east: -0.168398°
      |      },
      |      profiletype: {commodity: {type: electric}},
      |      coefficients: [
      |        {
      |          month: {type: january},
      |          coefficient:
      |            [
      |              {category: {type: heating}, coefficient: {value: 3}}
      |              {category: {type: waterheating}, coefficient: {value: 1.2}}
      |              {category: {type: cooking}, coefficient: {value: 1.6}}
      |            ]
      |        },{
      |          month: {type: february},
      |          coefficient:
      |            [
      |              {category: {type: heating}, coefficient: {value: 2}}
      |              {category: {type: waterheating}, coefficient: {value: 1.1}}
      |              {category: {type: cooking}, coefficient: {value: 1.3}}
      |            ]
      |        },{
      |          month: {type: march},
      |          coefficient:
      |            [
      |              {category: {type: heating}, coefficient: {value: 0.5}}
      |              {category: {type: waterheating}, coefficient: {value: 1}}
      |              {category: {type: cooking}, coefficient: {value: 1}}
      |            ]
      |        },{
      |          month: {type: april},
      |          coefficient:
      |            [
      |              {category: {type: heating}, coefficient: {value: 0.25}}
      |              {category: {type: waterheating}, coefficient: {value: 1}}
      |              {category: {type: cooking}, coefficient: {value: 0.8}}
      |            ]
      |        },{
      |          month: {type: may},
      |          coefficient:
      |            [
      |              {category: {type: heating}, coefficient: {value: 0}}
      |              {category: {type: waterheating}, coefficient: {value: 0.9}}
      |              {category: {type: cooking}, coefficient: {value: 0.7}}
      |            ]
      |        },{
      |          month: {type: june},
      |          coefficient:
      |            [
      |              {category: {type: heating}, coefficient: {value: 0}}
      |              {category: {type: waterheating}, coefficient: {value: 0.8}}
      |              {category: {type: cooking}, coefficient: {value: 0.6}}
      |            ]
      |        },{
      |          month: {type: july},
      |          coefficient:
      |            [
      |              {category: {type: heating}, coefficient: {value: 0}}
      |              {category: {type: waterheating}, coefficient: {value: 0.8}}
      |              {category: {type: cooking}, coefficient: {value: 0.6}}
      |            ]
      |        },{
      |          month: {type: august},
      |          coefficient:
      |            [
      |              {category: {type: heating}, coefficient: {value: 0}}
      |              {category: {type: waterheating}, coefficient: {value: 0.9}}
      |              {category: {type: cooking}, coefficient: {value: 0.7}}
      |            ]
      |        },{
      |          month: {type: september},
      |          coefficient:
      |            [
      |              {category: {type: heating}, coefficient: {value: 0.5}}
      |              {category: {type: waterheating}, coefficient: {value: 1}}
      |              {category: {type: cooking}, coefficient: {value: 0.8}}
      |            ]
      |        },{
      |          month: {type: october},
      |          coefficient:
      |            [
      |              {category: {type: heating}, coefficient: {value: 0.75}}
      |              {category: {type: waterheating}, coefficient: {value: 1}}
      |              {category: {type: cooking}, coefficient: {value: 1}}
      |            ]
      |        },{
      |          month: {type: november},
      |          coefficient:
      |            [
      |              {category: {type: heating}, coefficient: {value: 2}}
      |              {category: {type: waterheating}, coefficient: {value: 1.1}}
      |              {category: {type: cooking}, coefficient: {value: 1.3}}
      |            ]
      |        },{
      |          month: {type: december},
      |          coefficient:
      |            [
      |              {category: {type: heating}, coefficient: {value: 3}}
      |              {category: {type: waterheating}, coefficient: {value: 1.2}}
      |              {category: {type: cooking}, coefficient: {value: 1.6}}
      |            ]
      |        }
      |      ]
      |    }
      |  ]
      |}""".stripMargin)

  val seasonalAdjustmentConfigLoader: SeasonalAdjustmentFromConfig = SeasonalAdjustmentFromConfig(configB)
}