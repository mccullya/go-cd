package com.onzo.pipeline.config

import com.onzo.common.Commodity.{Electric, Gas}
import com.onzo.common.ProfileType

package object pld {
  val electricConfig = ProfileType(Electric)
  val gasConfig = ProfileType(Gas)
}
