package com.onzo.pipeline.config.pld

import com.onzo.pipeline.config.MapUtils._
import org.scalatest.WordSpec

/**
  * Created by toby.hobson on 05/04/2017.
  */
class MappingUtilsSpec extends WordSpec {

  "AlpabetIndex.toInt" when {
    "passed A" should {
      "return 0" in {
        assert("A".toIndex == 0)
      }
    }
    "passed Z" should {
      "return 25" in {
        assert("Z".toIndex == 25)
      }
    }
  }
}
