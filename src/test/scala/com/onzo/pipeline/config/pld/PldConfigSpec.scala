package com.onzo.pipeline.config.pld

import com.onzo.common.ProfileComponent.ElectricHeating
import com.onzo.pipeline.config.ModularConfiguration
// import _root_.pld.data.input.Weight.DefaultWeight
import _root_.pld.data.ConsumptionCategory.AlwaysOn
import com.onzo.pipeline.config.Parser
import com.onzo.pipeline.config.Parser.camelCaseHint
import com.typesafe.config.{Config, ConfigFactory}
import org.scalactic.TypeCheckedTripleEquals
import org.scalatest.{Matchers, WordSpec}

// TODO ATLAS-187
// TODO ATLAS-188
class PldConfigSpec extends WordSpec with Matchers with TypeCheckedTripleEquals {

  import PldConfigSpec._

  "a default weight configuration parser" when {
    "parsing a valid default weight config" should {
      "return a default weight configuration" in {
        defaultWeightConfiguration shouldBe a [DefaultWeightConfiguration]
      }
    }
  }
}

object PldConfigSpec {
  val config: Config = ConfigFactory.load("pld")
  lazy val defaultWeightConfiguration: DefaultWeightConfiguration =
    Parser.parse[DefaultWeightConfigurationNelWrapUnsafe](config.getConfig("pld.holland")).unwrap.toOption.get

  val allConfig: Config = ModularConfiguration.clientConfig
  lazy val pldConfigurationBuilder: PldConfigurationBuilder =
    PldConfigurationBuilder(config.getConfig("pld.holland"), allConfig)

  val d = DefaultWeight(1.0)
  val c = CategoryDefaultWeight(category = AlwaysOn, default = d)
  val profileComponent = List(ElectricHeating)
  // val p = ProfileDefaultWeight(has = profileComponent, categoryWeights = List(c))
  // val pldConfigA = PldConfiguration(DefaultWeights(profile = List(p)))
}