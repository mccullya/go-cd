package com.onzo.pipeline.config.pld

import cats.data.NonEmptyList
import cats.implicits._
import com.onzo.common.Commodity.Electric
import com.onzo.pipeline.config.Parser.camelCaseHint
import com.typesafe.config.{Config, ConfigFactory}
import com.onzo.common.ProfileComponent.{ElectricCooking, ElectricHeating}
import com.onzo.common.ProfileType
import com.onzo.pipeline.config.Parser.camelCaseHint
import com.typesafe.config.ConfigFactory
import org.scalactic.TypeCheckedTripleEquals
import org.scalatest.{Matchers, WordSpec}
import pld.data.ConsumptionCategory.{AlwaysOn, Refrigeration}
import pureconfig._
import pureconfig.error.{ConfigReaderFailures, NoValidCoproductChoiceFound}

class DefaultWeightSpec extends WordSpec with Matchers with TypeCheckedTripleEquals {

  import DefaultWeightSpec._

  "a parser" when {
    "parsing a default weight value" should {
      "create that value" in {
        val conf = ConfigFactory.parseString(" { underlying: 1.0 } ")
        loadConfig[DefaultWeight](conf) should ===(Right(DefaultWeight(1.0)))
      }
    }
    /**
      * N.B. note that parsing category when using the default CoProductHint
      * {category: {type: alwayson}
      * could be changed to
      * { category = AlwaysOn }
      * with tha addition of CoProduct parsing strategies but low priority
      */
    "parsing a category default weight" should {
      "create that value" in {
        val conf = ConfigFactory.parseString(" { category: {type: alwayson}, default: { underlying: 2.0 } }")
        loadConfig[CategoryDefaultWeight](conf) should ===(Right(CategoryDefaultWeight(AlwaysOn, DefaultWeight(2.0))))
      }
    }
    "parsing a profile default weight with no profile components and one category" should {
      "create that value" in {
        val conf = ConfigFactory.parseString(emptyProfileDefaultWeight)
        val r = loadConfig[ProfileDefaultWeightNelWrapUnsafe](conf).map(_.unwrap)
        r should === (Right(Right(expectedEmptyProfileDefaultWeight)))
      }
    }
    "parsing a profile default weight with one profile component and one category" should {
      "create that value" in {
        val conf = ConfigFactory.parseString(singleProfileDefaultWeight)
        val r = loadConfig[ProfileDefaultWeightNelWrapUnsafe](conf).map(_.unwrap)
        r should === (Right(Right(expectedSingleProfileDefaultWeight)))
      }
    }
    "parsing a profile default weight with two profile components and two categories" should {
      "create that value" in {
        val conf = ConfigFactory.parseString(twoComponentDefaultWeight)
        loadConfig[ProfileDefaultWeightNelWrapUnsafe](conf).map(_.unwrap) should === (Right(Right(expectedDoubleDefaultWeight)))
      }
    }
    "parsing a list of default weights" should {
      "create the list" in {
        val conf = ConfigFactory.parseString(
          s"""
             |{
             |  profileType: {commodity: {type: electric}}
             |  allowHas: [
             |    {type: electricheating}
             |  ]
             |  profile: [
             |    $singleProfileDefaultWeight
             |    $twoComponentDefaultWeight
             |  ]
             |}
          """.stripMargin)
        loadConfig[DefaultWeightsNelWrapUnsafe](conf).map(_.unwrap) should
          === (Right(Right(DefaultWeights(ProfileType(Electric), Set(ElectricHeating), NonEmptyList.fromListUnsafe(List[ProfileDefaultWeight](expectedSingleProfileDefaultWeight, expectedDoubleDefaultWeight))))))
      }
    }
    "parsing a profile list based on conf file with an electric profile list" should {
      "parse ok" in {
        val conf = ConfigFactory.parseString(
          s"""
             |  {
             |    defaultWeights: [
             |      {
             |        profileType: {commodity: {type: electric}}
             |        allowHas: [
             |          {type: electricheating}
             |          {type: electrichotwater}
             |          {type: topupelectriccooking}
             |        ]
             |        profile: [
             |          {
             |            has: [
             |              {type: electricheating},
             |              {type: electrichotwater}
             |            ]
             |            categoryWeights: [
             |              {category: {type: alwayson}, default: {underlying: 0}},
             |              {category: {type: refrigeration}, default: {underlying: 4.7}}
             |            ]
             |          },
             |          {
             |            has: [
             |              {type: electricheating},
             |              {type: topupelectriccooking}
             |            ]
             |            categoryWeights: [
             |              {category: {type: alwayson}, default: {underlying: 0}},
             |              {category: {type: refrigeration}, default: {underlying: 4.9}}
             |            ]
             |          },
             |          {
             |            has: [
             |            ]
             |            categoryWeights: [
             |              {category: {type: alwayson}, default: {underlying: 0}},
             |              {category: {type: refrigeration}, default: {underlying: 9.5}}
             |            ]
             |          }
             |        ]
             |      }
             |    ]
             |  }
          """.stripMargin)
        loadConfig[DefaultWeightConfigurationNelWrapUnsafe](conf).map(_.unwrap) shouldBe a[Right[_, _]]

      }
    }
  }
}

object DefaultWeightSpec {
  val emptyProfileDefaultWeight: String =
    """
      | {
      |   has: [
      |   ],
      |   categoryWeights: [
      |     { category: {type: alwayson}, default: { underlying: 1.5 } }
      |   ]
      | } """.stripMargin
  val expectedEmptyProfileDefaultWeight: ProfileDefaultWeight =
    ProfileDefaultWeight(List(),
      NonEmptyList.fromListUnsafe(List(CategoryDefaultWeight(AlwaysOn, DefaultWeight(1.5)))))

  val singleProfileDefaultWeight: String =
    """
      | {
      |   has: [
      |     { type: electricheating }
      |   ],
      |   categoryWeights: [
      |     { category: {type: alwayson}, default: { underlying: 2.0 } }
      |   ]
      | } """.stripMargin
  val expectedSingleProfileDefaultWeight: ProfileDefaultWeight =
    ProfileDefaultWeight(List(ElectricHeating),
      NonEmptyList.fromListUnsafe(List(CategoryDefaultWeight(AlwaysOn, DefaultWeight(2.0)))))

  val twoComponentDefaultWeight: String =
    """
      | {
      |   has: [
      |     { type: electricheating }.
      |     { type: electriccooking }
      |   ],
      |   categoryWeights: [
      |     { category: {type: alwayson}, default: { underlying: 0 } },
      |     { category: {type: refrigeration}, default: { underlying: 4.7 } }
      |   ]
      | } """.stripMargin
  val expectedDoubleDefaultWeight: ProfileDefaultWeight = ProfileDefaultWeight(
    List(ElectricHeating, ElectricCooking),
    NonEmptyList.fromListUnsafe(List(
      CategoryDefaultWeight(AlwaysOn, DefaultWeight(0)),
      CategoryDefaultWeight(Refrigeration, DefaultWeight(4.7)))))
}