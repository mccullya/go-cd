package com.onzo.pipeline.config.pld

import cats.data.NonEmptyList
import com.onzo.pipeline.config.ModularConfiguration
import com.onzo.pipeline.config.pld.CategoryLimitFromConfig.{CategoryLimit, CommodityCategoryLimit, Contribution}
import com.onzo.pipeline.config.pld.CategoryLimitFromConfigSpec._
import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory.parseString
import org.scalactic.TypeCheckedTripleEquals
import org.scalatest.{Matchers, WordSpec}
import pld.data.ConsumptionCategory._
import pld.data.input.CategoryCaps.{BaseLoadPercent, MaxCategoryAbsoluteCapRaw, MinTotalAbsoluteCapRaw}

class CategoryLimitFromConfigSpec extends WordSpec with Matchers with TypeCheckedTripleEquals {

  "a category limit from config" when {
    "parsing a simple valid config object" should {
      val categoryLimitConfigLoaderA = CategoryLimitFromConfig(configA)
      "successfully parse the config" in {
        categoryLimitConfigLoaderA.categoryLimitConfig shouldBe a[Right[_, _]]
      }
      "successfully decode the simple config with the expected values" in {
        categoryLimitConfigLoaderA.categoryLimitConfig should === (Right(expectedA))
      }
    }

    "parsing a slightly fuller config object" should {
      val categoryLimitConfigLoaderB = CategoryLimitFromConfig(configB)
      "successfully parse the config" in {
        categoryLimitConfigLoaderB.categoryLimitConfig shouldBe a [Right[_, _]]
      }
      "successfully decode the slightly fuller config with the expected values" in {
        categoryLimitConfigLoaderB.categoryLimitConfig should === (Right(expectedB))
      }
    }

    "loading a valid config from file" should {
      val config = ModularConfiguration.clientConfig
      val categoryLimitConfigLoader = CategoryLimitFromConfig(config)

      "successfully decode the config" in {
        categoryLimitConfigLoader.categoryLimitConfig shouldBe a[Right[_, _]]
      }
      "successfully decode the config to the expected values" in {
        categoryLimitConfigLoader.categoryLimitConfig should === (Right(expectedFromTestPldConf))
      }
    }
  }
}
object CategoryLimitFromConfigSpec {
  val configA: Config = parseString(
    """
      |pld.categoryLimits {
      |  commoditylimit: [
      |    {
      |      profiletype: {commodity: {type: electric}},
      |      contribution: [
      |        {
      |          category: {type: alwayson},
      |          limit: [
      |            {type: baseloadpercent, v: 20}
      |          ]
      |        }
      |      ]
      |    }
      |  ]
      |}""".stripMargin)
  val expectedAElectric: CategoryLimit = CategoryLimit(electricConfig,
    List(Contribution(AlwaysOn, List(BaseLoadPercent(20)))))

  val expectedA: List[CategoryLimit] = List(expectedAElectric)

  val configB: Config = parseString(
    """
      |pld.categoryLimits {
      |  commoditylimit: [
      |    {
      |      profiletype: {commodity: {type: gas}},
      |      contribution: []
      |    }
      |    {
      |      profiletype: {commodity: {type: electric}},
      |      contribution: [
      |        {
      |          category: {type: alwayson},
      |          limit: [
      |            {type: baseloadpercent, v: 85}
      |            {type: maxcategoryabsolutecapraw, v: 0}
      |          ]
      |        },
      |        {
      |          category: {type: refrigeration},
      |          limit: [
      |            {type: baseloadpercent, v: 15}
      |            {type: maxcategoryabsolutecapraw, v: 300}
      |          ]
      |        },
      |        {
      |          category: {type: lighting},
      |          limit: [
      |            {type: maxcategoryabsolutecapraw, v: 100}
      |          ]
      |        },
      |        {
      |          category: {type: electronics},
      |          limit: [
      |            {type: baseloadpercent, v: 0}
      |            {type: maxcategoryabsolutecapraw, v: 100}
      |          ]
      |        }
      |      ]
      |    }
      |  ]
      |}""".stripMargin)
  val expectedBElectric: CategoryLimit = CategoryLimit(electricConfig, List(
    Contribution(AlwaysOn, List(BaseLoadPercent(85), MaxCategoryAbsoluteCapRaw(0))),
    Contribution(Refrigeration, List(BaseLoadPercent(15), MaxCategoryAbsoluteCapRaw(300))),
    Contribution(Lighting, List(MaxCategoryAbsoluteCapRaw(100))),
    Contribution(Electronics, List(BaseLoadPercent(0), MaxCategoryAbsoluteCapRaw(100))
  )))

  val expectedBGas: CategoryLimit = CategoryLimit(gasConfig, List())
  val expectedB: List[CategoryLimit] = List(expectedBGas, expectedBElectric)

  val expectedFromTestPldConf: List[CategoryLimit] = expectedB
}
