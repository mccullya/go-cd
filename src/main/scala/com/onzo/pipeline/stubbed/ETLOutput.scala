package com.onzo.pipeline.stubbed

case class ETLOutput(sensorId: String, daily: DailyConsumption)
