package com.onzo.pipeline.stubbed

import jp.ne.opt.chronoscala.NamespacedImports._

case class Interval(timestamp: ZonedDateTime, consumption: Double)
