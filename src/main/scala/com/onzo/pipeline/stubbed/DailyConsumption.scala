package com.onzo.pipeline.stubbed

import _root_.pld.PipelineInterface.{EnergyReading, Reading}
import cats.data.NonEmptyList
import com.onzo.common.{HouseholdProfile => CommonHouseholdProfile}
import com.onzo.common.ProfileComponent
import com.onzo.common.ProfileComponent._
import com.onzo.common.domain.{ClientId, OnzoDeviceId}
import com.onzo.pipeline.domain.{HouseholdProfileEntity, RunDay}
import squants.energy.KilowattHours
import squants.market.Money

/* Daily consumption */
case class DailyConsumption(runDay: RunDay, clientId: ClientId, sensorId: OnzoDeviceId, householdProfileEntity: HouseholdProfileEntity, dailyCost: Money, intervals: Seq[Interval]) {
  lazy val total: Double = intervals.map(_.consumption).sum

  override def toString: String = s"""DailyConsumption(sensorId=$sensorId, total=$total), intervals=${intervals.mkString("|")}"""

  lazy val asReadings: NonEmptyList[Reading] = {
    val readings = intervals.map(interval =>
      EnergyReading(interval.timestamp, KilowattHours(interval.consumption))
    ).sortBy(_.time.toInstant.toEpochMilli)
    NonEmptyList[Reading](readings.head, readings.tail.toList)
  }

  private def build(tagged: Option[Boolean], profileComponent: ProfileComponent): Set[ProfileComponent] =
    if (tagged.contains(true)) Set(profileComponent) else Set()

  private val pldProfileSet: Set[ProfileComponent] = {
    import householdProfileEntity.householdProfile._
    build(electricCooking, ElectricCooking) ++
      build(topUpElectricCooking, TopUpElectricCooking) ++
      build(electricHeating, ElectricHeating) ++
      build(topUpElectricHeating, TopUpElectricHeating) ++
      build(electricHotWater, ElectricHotWater) ++
      build(gasCooking, GasCooking) ++
      build(gasHeating, GasHeating) ++
      build(gasHotWater, GasHotWater) ++
      build(compressiveCooling, CompressiveCooling) ++
      build(fanCooling, FanCooling)
  }

  val pldProfile: CommonHouseholdProfile = CommonHouseholdProfile(pldProfileSet)
}

/* End Daily consumption */