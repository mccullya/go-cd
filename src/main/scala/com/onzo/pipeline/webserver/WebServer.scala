package com.onzo.pipeline.webserver

import java.time.LocalDateTime

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.{HttpEntity, _}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{ExceptionHandler, Route}
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.util.Timeout
import com.onzo.common.domain.HouseholdId
import com.onzo.pipeline.Pipeline.ClientResult
import com.onzo.pipeline.PipelineActor.{Run, RunAll, RunPld}
import com.onzo.pipeline._
import com.onzo.pipeline.domain.{HouseholdProfile, HouseholdProfileJsonProtocol}
import com.typesafe.config.Config

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Properties, Success}

//@formatter:off
class WebServer(pipelineActor: ActorRef)(implicit executionContext: ExecutionContext, system: ActorSystem, mat: ActorMaterializer) extends HouseholdProfileJsonProtocol {
  //@formatter:on

  def run()(implicit config: Config): Future[ServerBinding] = {
    val serverPort = getConfigValue("webserver.port", "WEBSERVER_PORT")
    Http().bindAndHandle(routes, "0.0.0.0", serverPort).andThen {
      case Success(_) => logInfo(s"Server online at http://0.0.0.0:$serverPort")
    }
  }

  private def getConfigValue(configKey: String, envVariableName: String)(implicit system: ActorSystem,
                                                                         config: Config): Int = {
    Properties.envOrNone(envVariableName).map(_.toInt).getOrElse(config.getInt(configKey))
  }

  private def routes: Route = {

    def surroundWith(tag: String)(f: => String): String = s"<$tag>$f</$tag>"

    val p = surroundWith("p") _
    implicit val timeout: Timeout = 120.minutes
    val requestTimeout = 120 minutes

    val routes = path("ping") {
      get {
        complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "pong"))
      }
    } ~ path("run" / "all") {
      post {
        withRequestTimeout(requestTimeout) {
          logInfo("Running pipeline for all clients ...")
          val start = LocalDateTime.now()
          val eventualClientResults = (pipelineActor ? RunAll(reply = true)).mapTo[Iterable[ClientResult]]

          onSuccess(eventualClientResults) { clientResults =>
            val minutes = java.time.Duration.between(start, LocalDateTime.now()).toMinutes
            val htmlMessage = s"<!DOCTYPE html>" + p(clientResults.map(_.status).mkString("<br />")) + p(s"In $minutes minutes")
            logInfo(s"Pipeline took $minutes minutes")
            complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, htmlMessage))
          }
        }
      }
    } ~ path("run" / Remaining) { clientName =>
      post {
        withRequestTimeout(requestTimeout) {
          logInfo(s"Running pipeline for $clientName ...")
          val start = LocalDateTime.now()
          val eventualClientResult = (pipelineActor ? Run(clientName, reply = true)).mapTo[ClientResult]

          onSuccess(eventualClientResult) { clientResult =>
            val minutes = java.time.Duration.between(start, LocalDateTime.now()).toMinutes
            val htmlMessage = p(s"Processed ${clientResult.name} in $minutes minutes")
            logInfo(s"Pipeline took $minutes minutes")
            complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, htmlMessage))
          }
        }
      }
    } ~ path("pld" / Segments(2)) {
      case clientIdStr :: householdId :: Nil =>
        post {
          withRequestTimeout(requestTimeout) {
            entity(as[HouseholdProfile]) { pldProfile =>
              logInfo(s"requesting a pipeline run for requested client id $clientIdStr and household id $householdId")
              val start = LocalDateTime.now()
              val clientId = clientIdStr.toInt
              val eventualPldCount = (pipelineActor ? RunPld(clientId, HouseholdId(householdId), pldProfile)).mapTo[Int]

              onSuccess(eventualPldCount) { pldCount =>
                val minutes = java.time.Duration.between(start, LocalDateTime.now()).toMinutes
                val htmlMessage = p(s"Reprocessed $pldCount sensor days for client id: $clientId household id: $householdId in $minutes minutes")
                logInfo(s"Pipeline took $minutes minutes")
                complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, htmlMessage))
              }
            }
          }
        }
    } ~ path("fail") {
      throw new Exception("BOOM!")
    }

    implicit val exceptionHandler = ExceptionHandler {
      case t: Throwable =>
        logErrorT("Webserver error", t)
        complete(StatusCodes.BadRequest, "Sorry something went wrong")
    }

    Route.seal(routes)
  }

}
