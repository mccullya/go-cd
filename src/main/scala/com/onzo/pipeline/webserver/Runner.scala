package com.onzo.pipeline.webserver

import java.util.concurrent.Executors

import akka.actor.{ActorSystem, DeadLetter}
import akka.http.scaladsl.Http.ServerBinding
import akka.stream.{ActorMaterializer, ActorMaterializerSettings}
import com.datastax.driver.core.Cluster
import com.onzo.etl.persistence.csr.CsrActor
import com.onzo.common.domain.{ClientId, HouseholdId}
import com.onzo.pipeline._
import com.onzo.pipeline.config.ModularConfiguration
import com.onzo.pipeline.domain.HouseholdProfileEntity.GetProfileIdFn
import com.onzo.pipeline.domain.{HouseholdProfile, HouseholdProfileEntity}
import com.onzo.pipeline.persistence.{CassandraHouseholdApiUsersRepository, ElectricBreakdownPersister, ElectricConsumptionFindAllByUser, ElectricConsumptionPersister, GasBreakdownPersister, GasConsumptionPersister, HouseholdProfilePersister}
import com.typesafe.config.Config
import com.onzo.pipeline.CsrActorRef

import scala.collection.JavaConverters._
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Properties, Success, Try}

object Runner extends ExceptionLoggingDecider {

  def main(args: Array[String]): Unit = {
    val keyspace = "onzo" // TODO read from config
    implicit val executorService = Executors.newFixedThreadPool(100)
    implicit val executionContext: ExecutionContext = ExecutionContext.fromExecutor(executorService)
    implicit val system = ActorSystem("Atlas")
    val listener  = system.actorOf(DeadLetterListener.props)
    system.eventStream.subscribe(listener, classOf[DeadLetter])
    implicit val materializer = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(decider))
    implicit val config = ModularConfiguration().config
    val cassandraHosts = getConfigListValue("cassandra.hosts", "CASSANDRA_HOSTS")

    val app = getClass.getResource('/' + getClass.getName.replace('.', '/') + ".class")
    logInfo(s"Runner ${getClass.getPackage.getName} $app starting")
    logDebug(s"Using cassandra hosts: ${cassandraHosts.mkString(" ")}")
    val cluster = Cluster.builder().addContactPoints(cassandraHosts: _*).build()
    val session = cluster.connect()
    val electricConsumptionPersister = ElectricConsumptionPersister.create(session, keyspace)
    val electricBreakdownPersister = ElectricBreakdownPersister.create(session, keyspace)
    val gasConsumptionPersister = GasConsumptionPersister.create(session, keyspace)
    val gasBreakdownPersister = GasBreakdownPersister.create(session, keyspace)
    val findProfileById: GetProfileIdFn = findProfileOrEmpty(HouseholdProfilePersister.create(session, keyspace).findProfileById) _
    val householdApiUsersRepository = new CassandraHouseholdApiUsersRepository(session, keyspace)
    val electricConsumptionFindAllByUser = ElectricConsumptionFindAllByUser.create(session, keyspace)

    // Disable quartz scheduler
    //    val eventualBinding: Future[(ServerBinding, QuartzSchedulerExtension)] = for {
    //      pipelineActor <- Future {
    //        system.actorOf(PipelineActor.props()(
    //          executionContext,
    //          electricConsumptionPersister,
    //          electricBreakdownPersister,
    //          gasConsumptionPersister,
    //          gasBreakdownPersister,
    //          householdApiUsersRepository,
    //          findProfileById,
    //          materializer
    //        ))
    //      }
    //      webServer = new WebServer(pipelineActor)(executionContext, system, materializer)
    //      scheduler = QuartzSchedulerExtension(system)
    //      eventualBiding <- webServer.run()
    //    } yield {
    //      scheduler.createSchedule("greenchoice", Some("Run every day at 02:00 UTC"), "0 0 2 * * ?", timezone = TimeZone.getTimeZone("UTC"))
    //      scheduler.schedule("greenchoice", pipelineActor, Run("greenchoice", reply = false))
    //      (eventualBiding, scheduler)
    //    }

    val fCsrActorRef = CsrActor.create(system, "CsrActor", householdApiUsersRepository).map(CsrActorRef)

    val eventualBinding: Future[ServerBinding] = fCsrActorRef.flatMap { csrActorRef =>
      Future {
        system.actorOf(PipelineActor.props()(
          executionContext,
          electricConsumptionPersister,
          electricBreakdownPersister,
          gasConsumptionPersister,
          gasBreakdownPersister,
          householdApiUsersRepository,
          electricConsumptionFindAllByUser,
          csrActorRef,
          findProfileById,
          materializer
        ))
      }.map(pipelineActor => new WebServer(pipelineActor)(executionContext, system, materializer)).flatMap(_.run())
    }

    Runtime.getRuntime.addShutdownHook(new Thread() {
      override def run(): Unit = {
        val f = eventualBinding
          .andThen { case Success(_) => logInfo("Shutting down ...") }
          .andThen { case Success(server) => server.unbind() }
          // Disable quartz scheduler
          // .andThen { case Success((_, scheduler)) => scheduler.shutdown(true) }
          .andThen { case Success(_) => logError("Closing cassandra connection ...") }
          .andThen { case _ => cluster.close() }
          .andThen { case Success(_) => logError("Killing Akka ...") }
          .andThen { case _ => system.terminate() } // and shutdown when done
        Await.ready(f, Duration.Inf)
      }
    })

    eventualBinding.failed.foreach { t =>
      logError(s"Unable to start server: ${t.getMessage}")
      System.exit(-1)
    }
  }

  private def getConfigListValue(configKey: String, envVariableName: String)(implicit system: ActorSystem, config: Config): Seq[String] = {
    Properties.envOrNone(envVariableName).map(_.trim.split(",").toSeq.map(_.trim)).getOrElse(
      config.getStringList(configKey).asScala
    )
  }

  private def findProfileOrEmpty(lookup: (ClientId, HouseholdId) => Try[HouseholdProfile])
                                (clientId: ClientId, onzoHouseholdId: HouseholdId): HouseholdProfileEntity =
    HouseholdProfileEntity(clientId, onzoHouseholdId, lookup(clientId, onzoHouseholdId).getOrElse(HouseholdProfile.empty))
}
