package com.onzo.pipeline

import akka.actor.{Actor, DeadLetter, Props, Status}

class DeadLetterListener extends Actor {
  def receive = {
    case d: DeadLetter => d.message match {
      case Status.Failure(t) => logErrorT("Dead letter message", t)
      case m: Any => logError("Dead letter message " + m.toString)
    }
  }
}

object DeadLetterListener {
  def props: Props = Props[DeadLetterListener]
}