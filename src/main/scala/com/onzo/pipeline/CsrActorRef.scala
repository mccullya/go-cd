package com.onzo.pipeline

import akka.actor.ActorRef

case class CsrActorRef(underlying: ActorRef) extends AnyVal
