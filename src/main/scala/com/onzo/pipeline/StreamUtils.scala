package com.onzo.pipeline

import akka.NotUsed
import akka.stream.scaladsl.{GraphDSL, Merge}
import akka.stream.{FanOutShape2, Outlet}
import com.onzo.pipeline.domain.PipelineError

object StreamUtils {

  def mergeErrors[E <: PipelineError](shapes: FanOutShape2[_, E, _]*)(implicit b: GraphDSL.Builder[NotUsed]): Outlet[E] = {
    val outlets = shapes.map(_.out0)
    mergeOutletErrors(outlets: _*)
  }

  def mergeOutletErrors[E <: PipelineError](outlets: Outlet[E]*)(implicit b: GraphDSL.Builder[NotUsed]): Outlet[E] = {
    import GraphDSL.Implicits._
    val mergeErrors = b.add(Merge[E](outlets.length))
    outlets.zipWithIndex.foreach { case (out, i) => out ~> mergeErrors.in(i) }
    mergeErrors.out
  }

}