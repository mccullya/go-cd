package com.onzo.pipeline

import akka.actor.{Actor, ActorSystem, Props}
import akka.pattern.pipe
import akka.stream.ActorMaterializer
import com.onzo.common.domain.HouseholdId
import com.onzo.etl.persistence.csr.HouseholdApiUsersRepository
import com.onzo.pipeline.PipelineActor.{Run, RunAll, RunPld}
import com.onzo.pipeline.config.ModularConfiguration
import com.onzo.pipeline.domain.{HouseholdProfile, WindowedElectricConsumption}
import com.onzo.pipeline.domain.HouseholdProfileEntity.GetProfileIdFn
import com.onzo.pipeline.persistence.{ElectricBreakdownPersister, ElectricConsumptionPersister, FindAllByUser, GasBreakdownPersister, GasConsumptionPersister}
import com.typesafe.config.Config

import scala.concurrent.ExecutionContext

object PipelineActor {
  def props()(implicit ec: ExecutionContext,
              electricConsumptionPersister: ElectricConsumptionPersister,
              electricBreakdownPersister: ElectricBreakdownPersister,
              gasConsumptionPersister: GasConsumptionPersister,
              gasBreakdownPersister: GasBreakdownPersister,
              householdApiUsersRepository: HouseholdApiUsersRepository,
              elecConsumptionFindAllByUser: FindAllByUser[WindowedElectricConsumption],
              csrActorRef: CsrActorRef,
              findProfileById: GetProfileIdFn,
              materializer: ActorMaterializer): Props = Props(new PipelineActor)

  case class Run(clientName: String, reply: Boolean)

  case class RunAll(reply: Boolean)

  case class RunPld(clientId: Int, householdId: HouseholdId, pLDProfile: HouseholdProfile)

}

class PipelineActor(implicit val ec: ExecutionContext,
                    electricConsumptionPersister: ElectricConsumptionPersister,
                    electricBreakdownPersister: ElectricBreakdownPersister,
                    gasConsumptionPersister: GasConsumptionPersister,
                    gasBreakdownPersister: GasBreakdownPersister,
                    householdApiUsersRepository: HouseholdApiUsersRepository,
                    elecConsumptionFindAllByUser: FindAllByUser[WindowedElectricConsumption],
                    csrActorRef: CsrActorRef,
                    findProfileById: GetProfileIdFn,
                    materializer: ActorMaterializer) extends Actor {

  implicit val system: ActorSystem = context.system
  implicit val config: Config = ModularConfiguration().config
  val pipeline = new Pipeline()

  override def receive: Receive = {
    case Run(clientName, reply) =>
      implicit val sys = context.system
      val senderAddress = sender()
      val result = pipeline.runClient(clientName)
      result.foreach(clientResult => logClientInfo(clientResult)(clientResult.status))
      if (reply) result pipeTo senderAddress

    case RunAll(reply) =>
      implicit val sys = context.system
      val senderAddress = sender()
      val result = pipeline.runClients()
      if (reply) result pipeTo senderAddress

    case RunPld(clientId, householdId, pldProfile) =>
      implicit val sys = context.system
      val senderAddress = sender()
      val result = pipeline.runPld(clientId, householdId, pldProfile)
      result pipeTo senderAddress
  }

}
