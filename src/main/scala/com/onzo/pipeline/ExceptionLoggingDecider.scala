package com.onzo.pipeline

import akka.pattern.AskTimeoutException
import akka.stream.Supervision

trait ExceptionLoggingDecider {
  val decider: Supervision.Decider = {
    case t: AskTimeoutException =>
      logErrorT("Pipeline exception", t)
      Supervision.Resume
    case t: Throwable =>
      logErrorT("Pipeline exception", t)
      Supervision.Stop
  }
}
