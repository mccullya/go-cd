package com.onzo.pipeline

import _root_.pld.PipelineAdapter
import _root_.pld.PipelineInterface.{Reading => PipelineReading, _}
import akka.NotUsed
import akka.actor.Scheduler
import akka.pattern.{AskTimeoutException, ask}
import akka.stream._
import akka.stream.contrib.PartitionWith
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, Merge, Sink}
import akka.util.Timeout
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.onzo.collector.domain._
import com.onzo.collector.io.{ParseResultSourceHandle, S3FileProcessor}
import com.onzo.collector.parser.ParseError.InvalidCsrRecordWarning
import com.onzo.collector.parser.{FromCsvLine, ParseError}
import com.onzo.common.Commodity.{Electric, Gas}
import com.onzo.common._
import com.onzo.common.domain.{ClientId, HouseholdId, OnzoDeviceId, SensorId}
import com.onzo.etl.core.Etl
import com.onzo.etl.core.Etl.GasEtlResult
import com.onzo.etl.core.domain.SensorWindow.{ElectricSensorWindow, GasSensorWindow}
import com.onzo.etl.core.domain.{AugmentedSensorReading, EtlError, SensorWindow}
import com.onzo.etl.persistence.EtlActor.{EtlClientConfig, EtlResponse, ReadingsWithClientConfig}
import com.onzo.etl.persistence.csr.CsrActor.SaveCsr
import com.onzo.pipeline.EtlStatusActor.{LogEtlElectricOutput, LogEtlGasOutput}
import com.onzo.pipeline.config.ClientConfig
import com.onzo.pipeline.domain.{HouseholdProfileEntity => PipelineHouseholdProfile, _}
import com.onzo.pipeline.persistence.Persistable
import com.onzo.pipeline.stubbed.{DailyConsumption, Interval}
import datatypes.{Error => PldAdtError}
import org.slf4j.{Logger, LoggerFactory}
import squants.energy.{KilowattHours, Kilowatts}
import squants.market.EUR
import squants.motion.CubicMetersPerSecond
import squants.space.{CubicMeters, Degrees}

import scala.collection.immutable
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Random, Success, Try}

object Flows {

  // Type aliases
  type PipelineGraph[T] = Graph[FlowShape[Either[ParseError, T], Int], NotUsed]

  import Etl.ElectricEtlResult

  type SingleEtlResult = Either[EtlError, SensorWindow]
  type SingleElectricEtlResult = Either[ETLError, ElectricSensorWindow]

  // Type classes for the record handling graphs
  trait GraphLike[T] {
    def graph: Graph[FlowShape[Either[ParseError, T], Int], NotUsed]
  }

  implicit def csrGraphEv(implicit clientConfig: ClientConfig, csrActorRef: CsrActorRef, ec: ExecutionContext) = new GraphLike[CsrRecord] {
    override def graph: PipelineGraph[CsrRecord] = csrGraph()
  }

  implicit def electricConsumptionGraphEv(implicit clientConfig: ClientConfig,
                                          ec: ExecutionContext,
                                          scheduler: Scheduler,
                                          cpEv: Persistable[WindowedElectricConsumption],
                                          bpEv: Persistable[DailyElectricBreakdown],
                                          findProfileById: (ClientId, HouseholdId) => PipelineHouseholdProfile,
                                          csrActorRef: CsrActorRef,
                                          etlActorRef: EtlActorRef,
                                          etlStatusActorRef: EtlStatusActorRef) = new GraphLike[ElectricSensorReading] {
    override def graph: PipelineGraph[ElectricSensorReading] = electricConsumptionGraph()
  }

  // TODO ATLAS-310 either merge with electric Graph or ensure Breakdown is a Gas one
  implicit def gasConsumptionGraphEv[T <: Breakdown](implicit clientConfig: ClientConfig,
                                                     ec: ExecutionContext,
                                                     scheduler: Scheduler,
                                                     cpEv: Persistable[WindowedGasConsumption],
                                                     bpEv: Persistable[DailyGasBreakdown],
                                                     findProfileById: (ClientId, HouseholdId) => PipelineHouseholdProfile,
                                                     csrActorRef: CsrActorRef,
                                                     etlActorRef: EtlActorRef,
                                                     etlStatusActorRef: EtlStatusActorRef) = new GraphLike[GasSensorReading] {
    override def graph: PipelineGraph[GasSensorReading] = gasConsumptionGraph()
  }

  private implicit val akkaAskTimeout: Timeout = 30 seconds

  private val numWorkers = Runtime.getRuntime.availableProcessors() * 2
  private val etlBatchInputSize = 500

  def s3Sources[T](fileType: FileType[T])(implicit bucketName: String,
                                          fromCsvLine: FromCsvLine[T],
                                          clientConfig: ClientConfig,
                                          executionContext: ExecutionContext): Future[Seq[ParseResultSourceHandle[T]]] = {
    val amazonS3Client = AmazonS3ClientBuilder.defaultClient()
    val s3FileProcessor = new S3FileProcessor(amazonS3Client)
    s3FileProcessor.processFiles(fileType, bucketName, clientConfig)
  }

  // TODO ATLAS-310 remove
  /*def pldGraph()(implicit clientConfig: ClientConfig,
                 ec: ExecutionContext,
                 bpEv: Persistable[DailyElectricBreakdown],
                 findProfileById: (ClientId, HouseholdId) => PipelineHouseholdProfile): Graph[FlowShape[WindowedElectricConsumption, Int], NotUsed] = {

    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._
      val start = builder.add(Flow[WindowedElectricConsumption].map(_.asDailyConsumption))
      val pld = builder.add(pldShape)
      val saveBreakdowns = builder.add(persistenceShape[DailyElectricBreakdown](60))
      val mapSaves = builder.add(Flow[Boolean].map { case true => 1; case _ => 0 })
      //@formatter:off
      start ~> pld.in
      pld.out1 ~> saveBreakdowns.in
      saveBreakdowns.out1 ~> mapSaves
      StreamUtils.mergeErrors(pld, saveBreakdowns) ~> builder.add(errorShape)
      //@formatter:on
      FlowShape(start.in, mapSaves.out)
    }
  }*/

  def pldElectricGraph()(implicit clientConfig: ClientConfig,
                 ec: ExecutionContext,
                 bpEv: Persistable[DailyElectricBreakdown],
                 findProfileById: (ClientId, HouseholdId) => PipelineHouseholdProfile): Graph[FlowShape[WindowedElectricConsumption, Int], NotUsed] = {

    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._
      val start = builder.add(Flow[WindowedElectricConsumption].map(_.asDailyConsumption))
      val pld = builder.add(pldShapeElectric)
      val saveBreakdowns = builder.add(persistenceShape[DailyElectricBreakdown](60))
      val mapSaves = builder.add(Flow[Boolean].map { case true => 1; case _ => 0 })
      start ~> pld.in
      pld.out1 ~> saveBreakdowns.in
      saveBreakdowns.out1 ~> mapSaves
      StreamUtils.mergeErrors(pld, saveBreakdowns) ~> builder.add(errorShape)
      FlowShape(start.in, mapSaves.out)
    }
  }
  // T WindowedElectricConsumption
  // U ElectricEnergyBreakdown
  /*def pldBothGraph[T <: WindowedConsumption, U <: Breakdown]()(implicit clientConfig: ClientConfig,
                 ec: ExecutionContext,
                 bpEv: Persistable[DailyBreakdown[U]],
                 findProfileById: (ClientId, HouseholdId) => PipelineHouseholdProfile): Graph[FlowShape[T, Int], NotUsed] = {

    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._
      val start: FlowShape[T, DailyConsumption] = builder.add(Flow[T].map(_.asDailyConsumption))
      val pld: FanOutShape2[DailyConsumption, PLDError, DailyBreakdown[U]] = builder.add(pldShape)
      val saveBreakdowns = builder.add(persistenceShape[DailyBreakdown[U]](60))
      val mapSaves = builder.add(Flow[Boolean].map { case true => 1; case _ => 0 })
      //@formatter:off
      start ~> pld.in
      pld.out1 ~> saveBreakdowns.in
      saveBreakdowns.out1 ~> mapSaves
      StreamUtils.mergeErrors(pld, saveBreakdowns) ~> builder.add(errorShape)
      //@formatter:on
      FlowShape(start.in, mapSaves.out)
    }
  }

  def pldElectricGraph()(implicit clientConfig: ClientConfig,
                        ec: ExecutionContext,
                        bpEv: Persistable[DailyBreakdown[ElectricEnergyBreakdown]],
                        findProfileById: (ClientId, HouseholdId) => PipelineHouseholdProfile): Graph[FlowShape[WindowedElectricConsumption, Int], NotUsed] =
    pldBothGraph[WindowedElectricConsumption, ElectricEnergyBreakdown]()
*/

  def electricConsumptionGraph()(implicit clientConfig: ClientConfig,
                                 ec: ExecutionContext,
                                 scheduler: Scheduler,
                                 cpEv: Persistable[WindowedElectricConsumption],
                                 bpEv: Persistable[DailyElectricBreakdown],
                                 findProfileById: (ClientId, HouseholdId) => PipelineHouseholdProfile,
                                 csrActorRef: CsrActorRef,
                                 etlActorRef: EtlActorRef,
                                 etlStatusActorRef: EtlStatusActorRef): PipelineGraph[ElectricSensorReading] = {

    def toWindowedElectricConsumption(input: ElectricSensorWindow): WindowedElectricConsumption = {
      val intervals = input.consumptionByTime.mapValues(c =>
        ElectricConsumption(
          energy = c.energy,
          power = c.power,
          cost = c.cost
        )
      )

      WindowedElectricConsumption(
        clientId = ClientId(clientConfig.id),
        onzoHouseholdId = HouseholdId(input.onzoHouseholdId.value),
        onzoDeviceId = OnzoDeviceId(input.onzoDeviceId.value),
        day = RunDay(input.window.start.toLocalDate),
        resolution = input.resolution,
        timezone = input.zoneId,
        energy = input.totalEnergy,
        averagePower = input.averagePower,
        cost = input.totalCost,
        intervals = intervals
      )
    }

    def toDailyConsumption(input: WindowedElectricConsumption): DailyConsumption = {
      val intervals = input.intervals.map(c =>
        Interval(c._1, c._2.energy.value)
      ).toSeq.sortBy(_.timestamp.toInstant.toEpochMilli)
      val householdProfile: PipelineHouseholdProfile = findProfileById(clientConfig.clientId, input.onzoHouseholdId)
      logClientDebug(clientConfig)(s"found electric profile for client ${clientConfig.clientId} and Onzo household id ${input.onzoHouseholdId}: $householdProfile")
      DailyConsumption(input.day, input.clientId, input.onzoDeviceId, householdProfile, input.cost, intervals)
    }

    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._
      val start = builder.add(PartitionWith(transformParseErrors[ElectricSensorReading]))
      val etl = builder.add(etlElectricShape)
      val toWindowedConsumption = builder.add(Flow[ElectricSensorWindow].map(toWindowedElectricConsumption))
      val windowedElectricConsumptionToDaily = builder.add(Flow[WindowedElectricConsumption].map(toDailyConsumption))
      val bcConsumption = builder.add(Broadcast[WindowedElectricConsumption](3))
      val pld = builder.add(pldShapeElectric)
      val saveBreakdowns = builder.add(persistenceShape[DailyElectricBreakdown](60))
      val saveConsumption = builder.add(persistenceShape[WindowedElectricConsumption](60))
      val takeLast = builder.add(Flow[ElectricSensorWindow].grouped(2000).map(
        _.groupBy(w => (w.onzoHouseholdId, w.onzoDeviceId, w.window.start)).mapValues(_.last).values.to[immutable.Iterable]
      ).mapConcat(identity))

      def toZero = Flow[Boolean].map(_ => 0)
      val mergeCounts = builder.add(Merge[Int](3))
      val etlStatus: FlowShape[WindowedElectricConsumption, Int] = builder.add(
        Flow[WindowedElectricConsumption]
          .map(LogEtlElectricOutput.apply)
          .mapAsync(1)(message => ask(etlStatusActorRef.underlying, message).mapTo[Int])
      )

      //@formatter:off
      start.out1 ~> etl.in
                    etl.out1 ~> takeLast ~> toWindowedConsumption ~> bcConsumption
                                                                     bcConsumption.out(0) ~> saveConsumption.in
                                                                     saveConsumption.out1 ~> toZero ~> mergeCounts.in(0)
                                                                     bcConsumption.out(1) ~> windowedElectricConsumptionToDaily ~> pld.in
                                                                     pld.out1 ~> saveBreakdowns.in
                                                                     saveBreakdowns.out1 ~> toZero ~> mergeCounts.in(1)
                                                                     bcConsumption.out(2) ~> etlStatus ~> mergeCounts.in(2)

      //@formatter:on
      StreamUtils.mergeErrors(start, etl, pld, saveConsumption, saveBreakdowns) ~> builder.add(errorShape)
      FlowShape(start.in, mergeCounts.out)
    }
  }

  // TODO ATLAS-310 either merge with electric Graph or ensure Breakdown is a Gas one
  def gasConsumptionGraph()(implicit clientConfig: ClientConfig,
                            ec: ExecutionContext,
                            scheduler: Scheduler,
                            cpEv: Persistable[WindowedGasConsumption],
                            bpEv: Persistable[DailyGasBreakdown],
                            findProfileById: (ClientId, HouseholdId) => PipelineHouseholdProfile,
                            csrActorRef: CsrActorRef,
                            etlActorRef: EtlActorRef,
                            etlStatusActorRef: EtlStatusActorRef): PipelineGraph[GasSensorReading] = {

    def toWindowedGasConsumption(input: GasSensorWindow): WindowedGasConsumption = {
      val intervals = input.consumptionByTime.mapValues(c =>
        // case class GasConsumption(energy: Energy, power: Power, volume: Volume, flowRate: VolumeFlow, cost: Money)

        GasConsumption(
          energy = c.energy.getOrElse(KilowattHours(0)), // TODO ATLAS-310 don't process Zero values where it doesn't make sense
          power = c.power.getOrElse(Kilowatts(0)),
          volume = c.volume.getOrElse(CubicMeters(0)),
          flowRate = c.flowRate.getOrElse(CubicMetersPerSecond(0)),
          cost = c.cost
        )
      )

      WindowedGasConsumption(
        clientId = ClientId(clientConfig.id),
        onzoHouseholdId = HouseholdId(input.onzoHouseholdId.value),
        onzoDeviceId = OnzoDeviceId(input.onzoDeviceId.value),
        day = RunDay(input.window.start.toLocalDate),
        resolution = input.resolution,
        timezone = input.zoneId,
        energy = KilowattHours(0),
        volume = input.totalVolume.getOrElse(CubicMeters(0)), // TODO ONZO-1056
        flowRate = input.averageFlowRate.getOrElse(CubicMetersPerSecond(0)),
        averagePower = Kilowatts(0),
        cost = input.totalCost,
        intervals = intervals
      )
    }

    def toDailyConsumption(input: WindowedGasConsumption): DailyConsumption = {
      val intervals = input.intervals.map(c =>
        Interval(c._1, c._2.volume.value)
      ).toSeq.sortBy(_.timestamp.toInstant.toEpochMilli)
      val householdProfile = findProfileById(clientConfig.clientId, input.onzoHouseholdId)
      logClientDebug(clientConfig)(s"found gas profile for client ${clientConfig.clientId} and Onzo household id ${input.onzoHouseholdId}: $householdProfile")
      DailyConsumption(input.day, input.clientId, input.onzoDeviceId, householdProfile, input.cost, intervals)
    }

    /*GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._
      val start = builder.add(PartitionWith(transformParseErrors[GasSensorReading]))
      // Simulate ETL, persistence, PLD etc
      // 24 call simulates rolling hourly data to 1 day
      val stubbedEtl = builder.add(Flow[GasSensorReading].grouped(24).map(_ => 1))

      start.out1 ~> stubbedEtl.in
      StreamUtils.mergeErrors(start) ~> builder.add(errorShape)
      FlowShape(start.in, stubbedEtl.out)
    }*/
    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._
      val start = builder.add(PartitionWith(transformParseErrors[GasSensorReading]))
      val etl = builder.add(etlGasShape)
      val toWindowedConsumption = builder.add(Flow[GasSensorWindow].map(toWindowedGasConsumption))
      val windowedGasConsumptionToDaily = builder.add(Flow[WindowedGasConsumption].map(toDailyConsumption))
      val bcConsumption = builder.add(Broadcast[WindowedGasConsumption](3))
      val pld = builder.add(pldShapeGas)
      val saveBreakdowns = builder.add(persistenceShape[DailyGasBreakdown](60))
      val saveConsumption = builder.add(persistenceShape[WindowedGasConsumption](60))
      val takeLast = builder.add(Flow[GasSensorWindow].grouped(2000).map(
        _.groupBy(w => (w.onzoHouseholdId, w.onzoDeviceId, w.window.start)).mapValues(_.last).values.to[immutable.Iterable]
      ).mapConcat(identity))

      def toZero = Flow[Boolean].map(_ => 0)
      val mergeCounts = builder.add(Merge[Int](3))
      val etlStatus = builder.add(
        Flow[WindowedGasConsumption]
          .map(LogEtlGasOutput.apply)
          .mapAsync(numWorkers)(message => ask(etlStatusActorRef.underlying, message).mapTo[Int])
      )

      start.out1 ~> etl.in
      etl.out1 ~> takeLast ~> toWindowedConsumption ~> bcConsumption
      bcConsumption.out(0) ~> saveConsumption.in
      saveConsumption.out1 ~> toZero ~> mergeCounts.in(0)
      bcConsumption.out(1) ~> windowedGasConsumptionToDaily ~> pld.in
      pld.out1 ~> saveBreakdowns.in
      saveBreakdowns.out1 ~> toZero ~> mergeCounts.in(1)
      bcConsumption.out(2) ~> etlStatus ~> mergeCounts.in(2)

      StreamUtils.mergeErrors(start, etl, pld, saveConsumption, saveBreakdowns) ~> builder.add(errorShape)
      FlowShape(start.in, mergeCounts.out)
    }
  }

  def csrGraph()(implicit clientConfig: ClientConfig, csrActor: CsrActorRef, ec: ExecutionContext): PipelineGraph[CsrRecord] = {
    val etlClientId = ClientId(clientConfig.id)

    def askEtl(record: CsrRecord): Future[Int] = {
      val saveCsrMessage = SaveCsr(etlClientId, record)
      ask(csrActor.underlying, saveCsrMessage).mapTo[Unit].map(_ => 1)
    }

    def transformCsrErrors[T](i: Either[ParseError, T]): Either[PipelineError, T] = i.left.map {
      case InvalidCsrRecordWarning(parseError) => CsrRecordWarning(parseError.message)
      case parseError: ParseError => BatchCollectorError(parseError.message)
    }

    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._
      val start = builder.add(PartitionWith(transformCsrErrors[CsrRecord]))
      val etl = builder.add(Flow[CsrRecord].mapAsync(numWorkers)(askEtl))

      start.out1 ~> etl.in
      StreamUtils.mergeErrors(start) ~> builder.add(errorShape)
      FlowShape(start.in, etl.out)
    }
  }

  // TODO ATLAS-310 remove
  private def etlElectricShape(implicit clientConfig: ClientConfig,
                               etlActorRef: EtlActorRef,
                               ec: ExecutionContext,
                               scheduler: Scheduler): Graph[FanOutShape2[ElectricSensorReading, ETLError, ElectricSensorWindow], NotUsed] = {
    val clientId = ClientId(clientConfig.id)
    // TODO - Read real resolution from config
    val resolution = AugmentedSensorReading.Resolution.Minutes15
    // TODO - Read real unit of measure from config
    val unitOfMeasure = UnitOfMeasure.KilowattHour
    val etlClientConfig = EtlClientConfig(clientId, resolution, unitOfMeasure, EUR) // TODO use real currency

    def toReadingsWithClientConfig(electricSensorReadings: Seq[ElectricSensorReading]): ReadingsWithClientConfig = {
      ReadingsWithClientConfig(electricSensorReadings, etlClientConfig)
    }

    def transformEtlErrors(input: EtlError): ETLError = {
      // TODO - Use .message when it's available on EtlError
      ETLError(input.toString)
    }

    def askEtl(input: ReadingsWithClientConfig): Future[ElectricEtlResult] = {
      val askFuture = ask(etlActorRef.underlying, input).mapTo[EtlResponse].map(_.value.asInstanceOf[ElectricEtlResult])
      FutureUtils.retry(askFuture, Seq(500.milliseconds, 1.second, 5.seconds, 10.seconds))
    }

    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._
      val start = builder.add(Flow[ElectricSensorReading])
      val etl = builder.add(
        Flow[ElectricSensorReading]
          .grouped(etlBatchInputSize)
          .map(toReadingsWithClientConfig)
          .mapAsync(2)(askEtl)
      )
      // Essentially need to convert collection.Seq to collection.immutable.Seq
      val flattenResults = builder.add(Flow[ElectricEtlResult].mapConcat(_.toList))
      val transformErrors = builder.add(Flow[Either[EtlError, ElectricSensorWindow]].map(_.left.map(transformEtlErrors)))
      val end = builder.add(PartitionWith[Either[ETLError, ElectricSensorWindow], ETLError, ElectricSensorWindow](x => x))

      start.out ~> etl ~> flattenResults ~> transformErrors ~> end.in
      new FanOutShape2(start.in, end.out0, end.out1)
    }
  }

  private def etlGasShape(implicit clientConfig: ClientConfig,
                          etlActorRef: EtlActorRef,
                          ec: ExecutionContext,
                          scheduler: Scheduler): Graph[FanOutShape2[GasSensorReading, ETLError, GasSensorWindow], NotUsed] = {
    val clientId = ClientId(clientConfig.id)
    // TODO - Read real resolution from config
    val resolution = AugmentedSensorReading.Resolution.Minutes15
    // TODO - Read real unit of measure from config
    val unitOfMeasure = UnitOfMeasure.MetersCubed
    val etlClientConfig = EtlClientConfig(clientId, resolution, unitOfMeasure, EUR) // TODO use real currency

    def toReadingsWithClientConfig(gasSensorReadings: Seq[GasSensorReading]): ReadingsWithClientConfig = {
      ReadingsWithClientConfig(gasSensorReadings, etlClientConfig)
    }

    def transformEtlErrors(input: EtlError): ETLError = {
      // TODO - Use .message when it's available on EtlError
      ETLError(input.toString)
    }

    def askEtl(input: ReadingsWithClientConfig): Future[GasEtlResult] = {
      val askFuture = ask(etlActorRef.underlying, input).mapTo[EtlResponse].map(_.value.asInstanceOf[GasEtlResult])
      FutureUtils.retry(askFuture, Seq(500.milliseconds, 1.second, 5.seconds, 10.seconds))
    }

    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._
      val start: FlowShape[GasSensorReading, GasSensorReading] = builder.add(Flow[GasSensorReading])
      val etl: FlowShape[GasSensorReading, GasEtlResult] = builder.add(
        Flow[GasSensorReading]
          .grouped(etlBatchInputSize)
          .map(toReadingsWithClientConfig).mapAsync(2)(askEtl)
      )
      // Essentially need to convert collection.Seq to collection.immutable.Seq
      val flattenResults: FlowShape[GasEtlResult, Either[EtlError, GasSensorWindow]] = builder.add(Flow[GasEtlResult].mapConcat(_.toList))
      val transformErrors: FlowShape[Either[EtlError, GasSensorWindow], Either[ETLError, GasSensorWindow]] = builder.add(Flow[Either[EtlError, GasSensorWindow]].map(_.left.map(transformEtlErrors)))
      val end: FanOutShape2[Either[ETLError, GasSensorWindow], ETLError, GasSensorWindow] = builder.add(PartitionWith[Either[ETLError, GasSensorWindow], ETLError, GasSensorWindow](x => x))

      start.out ~> etl ~> flattenResults ~> transformErrors ~> end.in
      new FanOutShape2(start.in, end.out0, end.out1)
    }
  }

  // TODO ONZO-981 remove
  private def pldShapeElectric(implicit ec: ExecutionContext, clientConfig: ClientConfig): Graph[FanOutShape2[DailyConsumption, PLDError, DailyElectricBreakdown], NotUsed] = {
    val pldWrapper: DailyConsumption => Either[PLDError, DailyElectricBreakdown] = (dailyConsumption) => Try {
      // TODO get Location from dailyConsumption (not needed for v1)
      val location: Location = Location(Place("OnzoHQ"), north = Degrees(51.519570), east = Degrees(-0.168398))
      implicit val logger: Logger = LoggerFactory.getLogger("pld")
      import clientConfig.pldConfiguration._
      val categoryParameters: Either[PldAdtError, PLDCategoryParameters] =
        generateParameters(dailyConsumption.runDay, location, dailyConsumption.pldProfile, ProfileType(Electric))
      val result = for {
        p <- categoryParameters
        r <- PipelineAdapter.run(p, redistributionCap, dailyConsumption.asReadings)
          .map { result =>
            val values = result.summary.mapValues(_.v).mapValues { value =>
              // Calculate cost - NB can't divide by zero!
              if (dailyConsumption.total == 0) {
                ElectricEnergyBreakdown(KilowattHours(0), dailyConsumption.dailyCost * 0)
              } else {
                val proportion = value / dailyConsumption.total
                val cost = dailyConsumption.dailyCost * proportion
                ElectricEnergyBreakdown(KilowattHours(value), cost)
              }
            }
            DailyElectricBreakdown(
              clientId = dailyConsumption.clientId,
              onzoHouseholdId = dailyConsumption.householdProfileEntity.onzoHouseholdId,
              sensorId = SensorId(dailyConsumption.sensorId.value),
              day = dailyConsumption.runDay,
              breakdowns = values)
          }
          .left.map(e => PldAdtError(s"Flows.pldShape run failure: ${e.message}", e.throwable))
      } yield r
      result.left.map((pde: PldAdtError) => PLDError(dailyConsumption, "Flows.pldShape inner failure: " + pde.message + pde.throwable.map(a => a.getMessage + a.getStackTrace.toString).getOrElse("no Throwable")))
    }.toEither.fold(t => Left(PLDError(dailyConsumption, "Flows.pldShape outer: failure " + t.getMessage + t.getStackTrace.toString)), identity)

    GraphDSL.create() { implicit b =>
      import GraphDSL.Implicits._
      val start = b.add(Flow[DailyConsumption])
      val pld = b.add(Flow[DailyConsumption].mapAsync(numWorkers)(i => Future(pldWrapper(i))))
      // val pld = b.add(Flow[DailyConsumption].map(i => pldWrapper(i)))
      val end = b.add(PartitionWith((i: Either[PLDError, DailyElectricBreakdown]) => i))

      start.out ~> pld ~> end.in
      new FanOutShape2(start.in, end.out0, end.out1)
    }
  }

  // TODO ONZO-981 remove
  private def pldShapeGas(implicit ec: ExecutionContext, clientConfig: ClientConfig): Graph[FanOutShape2[DailyConsumption, PLDError, DailyGasBreakdown], NotUsed] = {
    val pldWrapper: DailyConsumption => Either[PLDError, DailyGasBreakdown] = (dailyConsumption) => Try {
      // TODO get Location from dailyConsumption (not needed for v1)
      val location: Location = Location(Place("OnzoHQ"), north = Degrees(51.519570), east = Degrees(-0.168398))
      implicit val logger: Logger = LoggerFactory.getLogger("pld")
      import clientConfig.pldConfiguration._
      val categoryParameters: Either[PldAdtError, PLDCategoryParameters] =
        generateParameters(dailyConsumption.runDay, location, dailyConsumption.pldProfile, ProfileType(Gas))
      val result = for {
        p <- categoryParameters
        r <- PipelineAdapter.run(p, redistributionCap, dailyConsumption.asReadings)
          .map { result =>
            val values = result.summary.mapValues(_.v).mapValues { value =>
              // Calculate cost - NB can't divide by zero!
              if (dailyConsumption.total == 0) {
                GasVolumeBreakdown(CubicMeters(0), dailyConsumption.dailyCost * 0)
              } else {
                val proportion = value / dailyConsumption.total
                val cost = dailyConsumption.dailyCost * proportion
                GasVolumeBreakdown(CubicMeters(value), cost)
              }
            }
            DailyGasBreakdown(
              clientId = dailyConsumption.clientId,
              onzoHouseholdId = dailyConsumption.householdProfileEntity.onzoHouseholdId,
              sensorId = SensorId(dailyConsumption.sensorId.value),
              day = dailyConsumption.runDay,
              breakdowns = values)
          }
          .left.map(e => PldAdtError(s"Flows.pldShape run failure: ${e.message}", e.throwable))
      } yield r
      result.left.map((pde: PldAdtError) => PLDError(dailyConsumption, "Flows.pldShape inner failure: " + pde.message + pde.throwable.map(a => a.getMessage + a.getStackTrace.toString).getOrElse("no Throwable")))
    }.toEither.fold(t => Left(PLDError(dailyConsumption, "Flows.pldShape outer: failure " + t.getMessage + t.getStackTrace.toString)), identity)

    GraphDSL.create() { implicit b =>
      import GraphDSL.Implicits._
      val start = b.add(Flow[DailyConsumption])
      val pld = b.add(Flow[DailyConsumption].mapAsync(numWorkers)(i => Future(pldWrapper(i))))
      // val pld = b.add(Flow[DailyConsumption].map(i => pldWrapper(i)))
      val end = b.add(PartitionWith((i: Either[PLDError, DailyGasBreakdown]) => i))

      start.out ~> pld ~> end.in
      new FanOutShape2(start.in, end.out0, end.out1)
    }
  }

  /*private def mapToBreakdown[T <: Breakdown](pldTopDown: TopDownSummaryAbsolute): Breakdown = {


  }
  private def pldShapeGas[T <: Breakdown](implicit ec: ExecutionContext, clientConfig: ClientConfig): Graph[FanOutShape2[DailyConsumption, PLDError, T], NotUsed] = {
    val pldWrapper: DailyConsumption => Either[PLDError, T] = (dailyConsumption) => Try {
      // TODO get Location from dailyConsumption (not needed for v1)
      val s: Set[ProfileComponent] = dailyConsumption.pldProfile
      val householdProfile: HouseholdProfile = HouseholdProfile(s)
      val location: Location = Location(Place("GreenchoiceLand"), north = Degrees(52.083333), east = Degrees(4.316667))
      // TODO actual Atlas logger
      implicit val logger: Logger = LoggerFactory.getLogger("pld")
      import clientConfig.pldConfiguration._
      val categoryParameters: Either[PldAdtError, PLDCategoryParameters] =
        generateParameters(dailyConsumption.getDay, location, householdProfile)
      val result = for {
        p <- categoryParameters
        r <- PipelineAdapter.run(p, redistributionCap, dailyConsumption.asReadings)
          .map { result =>
            val values = result.summary.mapValues(_.v).mapValues { value =>
              // Calculate cost - NB can't divide by zero!
              if (dailyConsumption.total == 0) {
                Breakdown[T] (KilowattHours(0), dailyConsumption.dailyCost * 0)
              } else {
                val proportion = value / dailyConsumption.total
                val cost = dailyConsumption.dailyCost * proportion
                EnergyBreakdown(KilowattHours(value), cost)
              }
            }
            DailyElectricBreakdown(ClientId(dailyConsumption.clientId), dailyConsumption.householdProfileEntity.onzoHouseholdId, dailyConsumption.getDay.underlying, values)
          }
          .left.map(e => PldAdtError(s"Flows.pldShape run failure: ${e.message}", e.throwable))
      } yield r
      result.left.map((pde: PldAdtError) => PLDError(dailyConsumption, "Flows.pldShape inner failure: " + pde.message + pde.throwable.map(a => a.getMessage + a.getStackTrace.toString).getOrElse("no Throwable")))
    }.toEither.fold(t => Left(PLDError(dailyConsumption, "Flows.pldShape outer: failure " + t.getMessage + t.getStackTrace.toString)), identity)

    GraphDSL.create() { implicit b =>
      import GraphDSL.Implicits._
      val start = b.add(Flow[DailyConsumption])
      val pld = b.add(Flow[DailyConsumption].mapAsync(numWorkers)(i => Future(pldWrapper(i))))
      // val pld = b.add(Flow[DailyConsumption].map(i => pldWrapper(i)))
      val end = b.add(PartitionWith((i: Either[PLDError, DailyElectricBreakdown]) => i))

      start.out ~> pld ~> end.in
      new FanOutShape2(start.in, end.out0, end.out1)
    }
  }*/

  private def persistenceShape[T](parallelism: Int)(implicit ec: ExecutionContext, persistenceEv: Persistable[T]): Graph[FanOutShape2[T, PipelineError, Boolean], NotUsed] = {
    GraphDSL.create() { implicit b =>
      import GraphDSL.Implicits._
      val start = b.add(Flow[T])
      val save = b.add(Flow[T].mapAsync(parallelism)(t => Future(implicitly[Persistable[T]].persist(t))))
      val split = b.add(PartitionWith((in: Try[Unit]) => in match {
        case Success(_) => Right(true)
        case Failure(t) => Left(GenericError(t.getMessage))
      }))
      start ~> save ~> split.in
      new FanOutShape2(start.in, split.out0, split.out1)
    }
  }

  private def errorShape(implicit clientConfig: ClientConfig): Graph[SinkShape[PipelineError], NotUsed] = {
    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._
      val start = builder.add(Flow[PipelineError])
      val sink = Sink.foreach[PipelineError] {
        case CsrRecordWarning(message) => // logClientDebug(clientConfig)(message)
        case e: PipelineError => logClientError(clientConfig)(e.message)
      }
      start ~> sink
      SinkShape(start.in)
    }
  }

  private def transformParseErrors[T](i: Either[ParseError, T]): Either[BatchCollectorError, T] = i.left.map(e => BatchCollectorError(e.message))

  implicit class RichBoolean(underlying: Boolean) {
    def toInt: Int = if (underlying) 1 else 0
  }

}