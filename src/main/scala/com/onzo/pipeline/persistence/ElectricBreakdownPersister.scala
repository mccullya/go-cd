package com.onzo.pipeline.persistence

import com.datastax.driver.core.{Session, UDTValue, UserType}
import com.onzo.pipeline.domain.{BreakdownOutputWasProduced, DailyElectricBreakdown, ElectricEnergyBreakdown}
import com.onzo.pipeline.persistence.CategoryKeys._
import pld.data.ConsumptionCategory

import scala.collection.JavaConverters._
import scala.concurrent.Future
import scala.util.Try

class ElectricBreakdownCassandraPersister private[persistence] (session: Session, val keySpace: String) extends Persistable[DailyElectricBreakdown] with CassandraConstants {

  private val TableName: String = "daily_electric_bill_breakdown"

  private val InsertCql: String =
    s"""
       |INSERT INTO $keySpace.$TableName (
       |    $organisation_id,
       |    $onzo_household_id,
       |    $day,
       |    $categories
       |) VALUES (
       |    :$organisation_id,
       |    :$onzo_household_id,
       |    :$day,
       |    :$categories
       |)
    """.stripMargin

  private val electricBreakdownCommodities: UserType = {
    session
      .getCluster
      .getMetadata
      .getKeyspace(keySpace)
      .getUserType(electric_breakdown_commodities)
  }

  private val preparedStatement = session.prepare(InsertCql)

  override def persist(t: DailyElectricBreakdown): Try[Unit] = {
    Try(preparedStatement.bind()
      .setInt(organisation_id, t.clientId.value)
      .setString(onzo_household_id, t.onzoHouseholdId.value)
      .setDate(day, t.day.underlying.asCassandraDate)
      .setMap(categories, t.breakdowns.asCassandraMap, classOf[String], classOf[UDTValue])
    ) flatMap (boundStatement => Try(session.execute(boundStatement)))
  }

  implicit class RichElectricBreakdown(underlying: Map[ConsumptionCategory, ElectricEnergyBreakdown]) {
    def asCassandraMap: java.util.Map[String, UDTValue] = {
      underlying.map {
        case (category, breakdown) =>
          val key = implicitly[KeyLike[ConsumptionCategory]].asKey(category)
          val value = electricBreakdownCommodities
            .newValue()
            .setDecimal(energy_wh, BigDecimal(breakdown.energy.toWattHours).bigDecimal)
            .setDecimal(cost, BigDecimal(breakdown.cost.value).bigDecimal)
          key -> value
      }.asJava
    }
  }
}

object ElectricBreakdownPersister {
  def create(session: Session, keySpace: String): ElectricBreakdownPersister = {
    new ElectricBreakdownCassandraPersister(session, keySpace)
  } andThen {
    ServiceMetricsCassandraPersister.create(session, keySpace).contramap { breakdown =>
      BreakdownOutputWasProduced(
        clientId = breakdown.clientId,
        householdId = breakdown.onzoHouseholdId,
        sensorId = breakdown.sensorId
      )
    }
  }
}
