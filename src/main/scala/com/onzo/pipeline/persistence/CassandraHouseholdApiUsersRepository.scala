package com.onzo.pipeline.persistence

import com.datastax.driver.core.querybuilder.QueryBuilder
import com.datastax.driver.core.{LocalDate, Session, UserType}
import com.onzo.common.domain.HouseholdId
import com.onzo.etl.persistence.csr.HouseholdApiUsersRepository
import com.onzo.etl.persistence.csr.HouseholdApiUsersRepository._

import scala.collection.JavaConverters._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class CassandraHouseholdApiUsersRepository(session: Session, keySpace: String)
  extends HouseholdApiUsersRepository with CassandraConstants {

  private val TableName: String = "household_api_users"

  private val InsertCql: String =
    s"""
       |INSERT INTO $keySpace.$TableName (
       |    $organisation_id,
       |    $user_id,
       |    $active,
       |    $onzo_household_id,
       |    $roles,
       |    $valid_from
       |) VALUES (
       |    :$organisation_id,
       |    :$user_id,
       |    :$active,
       |    :$onzo_household_id,
       |    :$roles,
       |    :$valid_from
       |)
    """.stripMargin

  private val FetchCql: String =
    s"""
       |SELECT *
       |FROM   $keySpace.$TableName
       |WHERE  $organisation_id = ?
       |AND    $user_id = ?
     """.stripMargin

  private val insertPreparedStatement = session.prepare(InsertCql)
  private val fetchPreparedStatement = session.prepare(FetchCql)

  private val cassandraRole: UserType =
    session
      .getCluster
      .getMetadata
      .getKeyspace(keySpace)
      .getUserType(cassandra_role)

  override def fetchByPrimaryKey(primaryKey: (OrganisationId, UserId)): Future[Option[HouseholdApiUsers]] = {
    val fResult =
      Future(
        session.execute(
          fetchPreparedStatement
            .bind()
            .setInt(organisation_id, primaryKey._1.value)
            .setString(user_id, primaryKey._2.value)
        ).all.asScala
      )

    fResult.map { result =>
      if (result.size != 1)
        None
      else {
        val record = result.head
        val householdApiUsers =
          HouseholdApiUsers(
            OrganisationId(record.getInt(organisation_id)),
            HouseholdId(record.getString(onzo_household_id)),
            UserId(record.getString(user_id)),
            active = record.getBool(active),
            ValidFrom(record.getDate(valid_from))
          )
        Some(householdApiUsers)
      }
    }
  }

  override def save(householdApiUsers: HouseholdApiUsers): Future[Unit] = {
    val hardcodedRole =
      Set(cassandraRole.newValue().setString("name", "user")).asJava

    Future {
      session.execute(
        insertPreparedStatement
          .bind()
          .setInt(organisation_id, householdApiUsers.organisationId.value)
          .setString(user_id, householdApiUsers.userId.value)
          .setBool(active, householdApiUsers.active)
          .setString(onzo_household_id, householdApiUsers.onzoHouseholdId.value)
          .setSet(roles, hardcodedRole)
          .setDate(valid_from, householdApiUsers.validFrom)
      )
    }
  }


  private implicit def toCassandraTimestamp(underlying: ValidFrom): LocalDate = {
    val jLocalDate = underlying.value
    val year = jLocalDate.getYear
    val month = jLocalDate.getMonthValue
    val day = jLocalDate.getDayOfMonth
    LocalDate.fromYearMonthDay(year, month, day)
  }

  private implicit def toJavaLocalDate(underlying: LocalDate): java.time.LocalDate = {
    val year = underlying.getYear
    val month = underlying.getMonth
    val day = underlying.getDay
    java.time.LocalDate.of(year, month, day)
  }
}
