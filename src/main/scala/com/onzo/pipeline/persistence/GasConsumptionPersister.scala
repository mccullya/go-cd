package com.onzo.pipeline.persistence

import java.time._
import java.util.Date

import com.datastax.driver.core.{Session, UDTValue, UserType}
import com.onzo.common.domain.SensorId
import com.onzo.pipeline.domain.{ConsumptionOutputWasProduced, GasConsumption, WindowedGasConsumption}
import squants.time.Time

import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

class GasConsumptionCassandraPersister private[persistence] (session: Session, val keySpace: String, cleanDatabase: Boolean = false)
  extends Persistable[WindowedGasConsumption] with CassandraConstants {

  private val TableName: String = "consumption_intraday_gas_by_household"

  private val InsertCql: String =
    s"""
       |INSERT INTO $keySpace.$TableName (
       |    $organisation_id,
       |    $onzo_household_id,
       |    $onzo_device_id,
       |    $day,
       |    $resolution_seconds,
       |    $timezone,
       |    $consumption,
       |    $energy_wh,
       |    $volume_cm3,
       |    $flow_rate_cm3_per_minute,
       |    $power_w,
       |    $cost
       |) VALUES (
       |    :$organisation_id,
       |    :$onzo_household_id,
       |    :$onzo_device_id,
       |    :$day,
       |    :$resolution_seconds,
       |    :$timezone,
       |    :$consumption,
       |    :$energy_wh,
       |    :$volume_cm3,
       |    :$flow_rate_cm3_per_minute,
       |    :$power_w,
       |    :$cost
       |)
    """.stripMargin

  private val gasConsumptionUDT: UserType = session
    .getCluster
    .getMetadata
    .getKeyspace(keySpace)
    .getUserType(gas_consumption)

  private val preparedStatement = session.prepare(InsertCql)

  override def persist(t: WindowedGasConsumption): Try[Unit] = {
    Try(preparedStatement.bind()
      .setInt(organisation_id, t.clientId.value)
      .setString(onzo_household_id, t.onzoHouseholdId.value)
      .setString(onzo_device_id, t.onzoDeviceId.value)
      .setDate(day, t.day.underlying.asCassandraDate)
      .setInt(resolution_seconds, t.resolution.asSeconds)
      .setString(timezone, t.timezone.getId)
      .setMap(consumption, t.intervals.asCassandraMap, classOf[Date], classOf[UDTValue])
      .setDecimal(energy_wh, BigDecimal(t.energy.toWattHours).bigDecimal)
      .setDecimal(volume_cm3, BigDecimal(t.volume.toCubicMeters).bigDecimal)
      .setDecimal(flow_rate_cm3_per_minute, BigDecimal(t.flowRate.toCubicMetersPerSecond * Time.SecondsPerMinute).bigDecimal)
      .setDecimal(power_w, BigDecimal(t.averagePower.toWatts).bigDecimal)
      .setDecimal(cost, BigDecimal(t.cost.value).bigDecimal)
    ) flatMap (boundStatement => Try(session.execute(boundStatement)))
  }

  implicit class RichGasConsumption(underlying: Map[ZonedDateTime, GasConsumption]) {
    def asCassandraMap: java.util.Map[Date, UDTValue] = {
      underlying.map {
        case (timestamp, ec) =>
          val key = Date.from(timestamp.toInstant)
          val value = gasConsumptionUDT
            .newValue()
            .setDecimal(energy_wh, BigDecimal(ec.energy.toWattHours).bigDecimal)
            .setDecimal(power_w, BigDecimal(ec.power.toWatts).bigDecimal)
            .setDecimal(volume_cm3, BigDecimal(ec.volume.toCubicMeters).bigDecimal)
            .setDecimal(flow_rate_cm3_per_minute, BigDecimal(ec.flowRate.toCubicMetersPerSecond * Time.SecondsPerMinute).bigDecimal)
            .setDecimal(cost, BigDecimal(ec.cost.value).bigDecimal)
          key -> value
      }.asJava
    }
  }
}

object GasConsumptionPersister {

  def create(session: Session, keySpace: String)(implicit executionContext: ExecutionContext): GasConsumptionPersister = {
    new GasConsumptionCassandraPersister(session, keySpace)
  } andThen {
    ServiceMetricsCassandraPersister.create(session, keySpace).contramap { consumption =>
      ConsumptionOutputWasProduced(
        clientId = consumption.clientId,
        householdId = consumption.onzoHouseholdId,
        sensorId = SensorId(consumption.onzoDeviceId.value)
      )
    }
  }
}

