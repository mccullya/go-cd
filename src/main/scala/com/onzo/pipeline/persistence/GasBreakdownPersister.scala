package com.onzo.pipeline.persistence

import com.datastax.driver.core.{Session, UDTValue, UserType, LocalDate => CassandraLocalDate}
import com.onzo.pipeline.domain._
import com.onzo.pipeline.persistence.CategoryKeys._
import pld.data.ConsumptionCategory

import scala.collection.JavaConverters._
import scala.concurrent.Future
import scala.util.Try

class GasBreakdownCassandraPersister private[persistence] (session: Session, val keySpace: String) extends Persistable[DailyGasBreakdown] with CassandraConstants {

  private val TableName: String = "daily_gas_bill_breakdown"

  private val InsertCql: String =
    s"""
       |INSERT INTO $keySpace.$TableName (
       |    $organisation_id,
       |    $onzo_household_id,
       |    $day,
       |    $categories
       |) VALUES (
       |    :$organisation_id,
       |    :$onzo_household_id,
       |    :$day,
       |    :$categories
       |)
    """.stripMargin

  private val gasBreakdownCommodities: UserType = {
    session
      .getCluster
      .getMetadata
      .getKeyspace(keySpace)
      .getUserType(gas_breakdown_commodities)
  }

  private val preparedStatement = session.prepare(InsertCql)

  override def persist(t: DailyGasBreakdown): Try[Unit] = {
    Try(preparedStatement.bind()
      .setInt(organisation_id, t.clientId.value)
      .setString(onzo_household_id, t.onzoHouseholdId.value)
      .setDate(day, t.day.underlying.asCassandraDate)
      .setMap(categories, t.breakdowns.asCassandraMap, classOf[String], classOf[UDTValue])
    ) flatMap (boundStatement => Try(session.execute(boundStatement)))
  }

  implicit class RichGasBreakdown(underlying: Map[ConsumptionCategory, Breakdown]) {
    def asCassandraMap: java.util.Map[String, UDTValue] = {
      underlying.map {
        case (category, breakdown) =>
          val commodity = breakdown match {
            case GasEnergyBreakdown(energy, _) =>
              gasBreakdownCommodities
                .newValue()
                .setDecimal(energy_wh, BigDecimal(energy.toWattHours).bigDecimal)
                .setToNull(volume_cm3)
            case GasVolumeBreakdown(volume, _) =>
              gasBreakdownCommodities
                .newValue()
                .setDecimal(volume_cm3, BigDecimal(volume.toCubicMeters).bigDecimal)
                .setToNull(energy_wh)
          }

          val key = implicitly[KeyLike[ConsumptionCategory]].asKey(category)
          val value = commodity.setDecimal(cost, breakdown.cost.amount.bigDecimal)
          key -> value
      }.asJava
    }
  }
}

object GasBreakdownPersister {
  def create(session: Session, keySpace: String): GasBreakdownPersister = {
    new GasBreakdownCassandraPersister(session, keySpace)
  } andThen {
    ServiceMetricsCassandraPersister.create(session, keySpace).contramap { breakdown =>
      BreakdownOutputWasProduced(
        clientId = breakdown.clientId,
        householdId = breakdown.onzoHouseholdId,
        sensorId = breakdown.sensorId
      )
    }
  }
}
