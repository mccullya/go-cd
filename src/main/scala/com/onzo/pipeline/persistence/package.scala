package com.onzo.pipeline

import java.time._
import java.util.TimeZone

import com.onzo.pipeline.domain.{DailyElectricBreakdown, DailyGasBreakdown, WindowedElectricConsumption, WindowedGasConsumption}
// import jp.ne.opt.chronoscala.Imports._
import com.datastax.driver.core.{LocalDate => CassandraLocalDate}

package object persistence {

  type GasBreakdownPersister = Persistable[DailyGasBreakdown]
  type ElectricBreakdownPersister = Persistable[DailyElectricBreakdown]

  type GasConsumptionPersister = Persistable[WindowedGasConsumption]
  type ElectricConsumptionPersister = Persistable[WindowedElectricConsumption]

  implicit class RichLocalDate(underlying: LocalDate) {
    def asCassandraDate: CassandraLocalDate = {
      val utc = ZoneId.of("UTC")
      CassandraLocalDate.fromMillisSinceEpoch(
        ZonedDateTime.of(underlying, LocalTime.of(0, 0), utc).toInstant.toEpochMilli
      )
    }
  }

  implicit class RichCassandraLocalDate(underlying: CassandraLocalDate) {

    def asLocalDate(): LocalDate = {
      val day = underlying.getDay
      val month = underlying.getMonth
      val year = underlying.getYear
      LocalDate.of(year, month, day)
    }

    def asZonedDateTime(timeZone: TimeZone):ZonedDateTime = {
      val timestamp = underlying.getMillisSinceEpoch
      Instant.ofEpochMilli(timestamp).atZone(timeZone.toZoneId)
    }
  }

  implicit class RichZonedDateTime(underlying: ZonedDateTime) {
    def asCassandraDate: CassandraLocalDate = {
      CassandraLocalDate.fromMillisSinceEpoch(underlying.toEpochSecond * 1000)
    }
  }
}
