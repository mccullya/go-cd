package com.onzo.pipeline.persistence

import com.onzo.common.domain.{ClientId, HouseholdId}


trait FindAllByUser[T] {

  def findAllByUser(clientId: ClientId, onzoHouseholdId: HouseholdId): Iterator[T]

}
