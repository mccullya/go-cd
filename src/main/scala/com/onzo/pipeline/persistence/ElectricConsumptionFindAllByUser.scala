package com.onzo.pipeline.persistence

import java.time.ZoneId
import java.util.Date

import com.datastax.driver.core.{Session, UDTValue}
import com.onzo.common.domain.{ClientId, HouseholdId, OnzoDeviceId}
import com.onzo.pipeline.domain.{ElectricConsumption, Resolution, RunDay, WindowedElectricConsumption}
import squants.energy.{WattHours, Watts}
import squants.market.EUR

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext

class ElectricConsumptionFindAllByUser private(session: Session, val keySpace: String) extends FindAllByUser[WindowedElectricConsumption] with CassandraConstants {

  def findAllByUser(clientId: ClientId, onzoHouseholdId: HouseholdId): Iterator[WindowedElectricConsumption] = {
    val query = s"SELECT * FROM $keySpace.$consumption_intraday_elec_by_household_Table WHERE $organisation_id = ? AND $onzo_household_id = ?"
    val resultSet = session.execute(query, Int.box(clientId.value), onzoHouseholdId.value)
    val iterator = resultSet.iterator().asScala
    iterator.map { row =>
      val cassandraIntervals = row.getMap(consumption, classOf[Date], classOf[UDTValue]).asScala.toMap
      val intervals = cassandraIntervals.map {
        case (timestamp, udt) =>
          val energy = WattHours(udt.getDecimal(energy_wh).doubleValue())
          val power = Watts(udt.getDecimal(power_w).doubleValue())
          // TODO use real currency from database
          val costGbp = EUR(udt.getDecimal(cost).doubleValue())
          // TODO use real timezone from database
          val utcTime = timestamp.toInstant.atZone(ZoneId.of("CET"))
          val consumption = ElectricConsumption(energy, power, costGbp)
          (utcTime, consumption)
      }

      WindowedElectricConsumption(
        clientId = clientId,
        onzoHouseholdId = onzoHouseholdId,
        onzoDeviceId = OnzoDeviceId(row.getString(onzo_device_id)),
        day = RunDay(row.getDate(day).asLocalDate()),
        resolution = Resolution.Min15, // TODO replace hard coded values
        timezone = ZoneId.of(row.getString(timezone)),
        energy = WattHours(row.getDecimal(energy_wh).doubleValue()),
        averagePower = Watts(row.getDecimal(power_w).doubleValue()),
        // TODO use real currency from database
        cost = EUR(row.getDecimal(cost).doubleValue()),
        intervals = intervals
      )
    }
  }

}

object ElectricConsumptionFindAllByUser {
  def create(session: Session, keySpace: String)(implicit executionContext: ExecutionContext): ElectricConsumptionFindAllByUser = {
    new ElectricConsumptionFindAllByUser(session, keySpace)
  }
}
