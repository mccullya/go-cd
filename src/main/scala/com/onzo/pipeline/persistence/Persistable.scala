package com.onzo.pipeline.persistence

import scala.util.Try

trait Persistable[T] {

  def persist(t: T): Try[Unit]

  def contramap[U](f: U => T): Persistable[U] = {
    val original = this
    new Persistable[U] {
      override def persist(u: U): Try[Unit] = Try(f(u)).flatMap(original.persist)
    }
  }

  def andThen(second: Persistable[T]): Persistable[T] = {
    val first = this
    new Persistable[T] {
      override def persist(t: T): Try[Unit] = first.persist(t).flatMap { _ => second.persist(t) }
    }
  }
}

