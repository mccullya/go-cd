package com.onzo.pipeline.persistence

trait CassandraConstants {

  // Consider using AnyVal

  // Common
  protected val organisation_id = "organisation_id"
  protected val onzo_household_id = "onzo_household_id"
  protected val onzo_device_id = "onzo_device_id"
  protected val day = "day"
  protected val resolution_seconds = "resolution_seconds"
  protected val consumption = "consumption"
  protected val timezone = "timezone"

  // Elec
  protected val energy_wh = "energy_wh"
  protected val power_w = "power_w"
  protected val cost = "cost"
  protected val electric_consumption = "electric_consumption"
  protected val consumption_intraday_elec_by_household_Table = "consumption_intraday_elec_by_household"

  // Gas
  protected val volume_cm3 = "volume_cm3"
  protected val flow_rate_cm3_per_minute = "flow_rate_cm3_per_minute"
  protected val gas_consumption = "gas_consumption"

  // Breakdowns
  protected val electric_breakdown_commodities = "electric_breakdown_commodities"
  protected val gas_breakdown_commodities = "gas_breakdown_commodities"
  protected val categories = "categories"

  // CSR
  protected val status = "status"
  protected val currency = "currency"
  protected val currency_type = "currency_type"
  protected val daily_standing_charge = "daily_standing_charge"
  protected val cost_per_unit = "cost_per_unit"
  protected val unit_of_measure = "unit_of_measure"
  protected val tags = "tags"
  protected val zip_postal_code = "zip_postal_code"
  protected val latitude = "latitude"
  protected val longitude = "longitude"

  // PLD Household profile
  protected val house_type = "house_type"
  protected val number_of_people = "number_of_people"
  protected val people_plus = "people_plus"
  protected val number_of_bedrooms = "number_of_bedrooms"
  protected val bedrooms_plus = "bedrooms_plus"
  protected val electric_heating = "electric_heating"
  protected val top_up_electric_heating = "top_up_electric_heating"
  protected val electric_hot_water = "electric_hot_water"
  protected val electric_cooking = "electric_cooking"
  protected val top_up_electric_cooking = "top_up_electric_cooking"
  protected val gas_heating = "gas_heating"
  protected val gas_cooking = "gas_cooking"
  protected val gas_hot_water = "gas_hot_water"
  protected val compressive_cooling = "compressive_cooling"
  protected val fan_cooling = "fan_cooling"

  // household_api_users
  protected val user_id = "user_id"
  protected val active = "active"
  protected val roles = "roles"
  protected val valid_from = "valid_from"
  protected val cassandra_role = "cassandra_role"

  // service_metrics
  protected val household_state = "household_state"
  protected val when_household_state_changed = "when_household_state_changed"
  protected val when_csr_input_processed = "when_csr_input_processed"
  protected val when_consumption_input_processed = "when_consumption_input_processed"
  protected val when_consumption_output_produced = "when_consumption_output_produced"
  protected val when_breakdown_output_produced = "when_breakdown_output_produced"
}
