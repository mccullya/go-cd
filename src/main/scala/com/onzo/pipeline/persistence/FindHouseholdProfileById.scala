package com.onzo.pipeline.persistence

import com.onzo.common.domain.{ClientId, HouseholdId}
import com.onzo.pipeline.domain.HouseholdProfile

import scala.util.Try

/**
  * Created by toby.hobson on 24/04/2017.
  */
trait FindHouseholdProfileById {

  def findProfileById(clientId: ClientId, onzoHouseholdId: HouseholdId): Try[HouseholdProfile]

}
