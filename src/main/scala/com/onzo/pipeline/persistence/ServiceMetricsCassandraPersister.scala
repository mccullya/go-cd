package com.onzo.pipeline.persistence

import com.datastax.driver.core.{BoundStatement, Session}
import com.onzo.pipeline.domain.{BreakdownOutputWasProduced, ConsumptionOutputWasProduced, ServiceMetricsData}

import scala.util.Try

class ServiceMetricsCassandraPersister private[persistence](session: Session, val keySpace: String) extends Persistable[ServiceMetricsData] with CassandraConstants {

  private val TableName: String = "service_metrics"

  private val preparedBreakdownOutputWasProduced = session.prepare {
    s"""
       |INSERT INTO $keySpace.$TableName (
       |    $organisation_id,
       |    $onzo_household_id,
       |    $onzo_device_id,
       |    $when_breakdown_output_produced
       |) VALUES (
       |    :$organisation_id,
       |    :$onzo_household_id,
       |    :$onzo_device_id,
       |    :$when_breakdown_output_produced
       |)
     """.stripMargin
  }

  private val preparedConsumptionOutputWasProduced = session.prepare {
    s"""
       |INSERT INTO $keySpace.$TableName (
       |    $organisation_id,
       |    $onzo_household_id,
       |    $onzo_device_id,
       |    $when_consumption_output_produced
       |) VALUES (
       |    :$organisation_id,
       |    :$onzo_household_id,
       |    :$onzo_device_id,
       |    :$when_consumption_output_produced
       |)
     """.stripMargin
  }

  private def bindStatement(data: ServiceMetricsData): BoundStatement = data match {
    case the: BreakdownOutputWasProduced =>
      preparedBreakdownOutputWasProduced
        .bind()
        .setInt(organisation_id, the.clientId.value)
        .setString(onzo_household_id, the.householdId.value)
        .setString(onzo_device_id, the.sensorId.value)
        .setString(when_breakdown_output_produced, java.time.format.DateTimeFormatter.ISO_INSTANT.format(the.when))

    case the: ConsumptionOutputWasProduced =>
      preparedConsumptionOutputWasProduced
        .bind()
        .setInt(organisation_id, the.clientId.value)
        .setString(onzo_household_id, the.householdId.value)
        .setString(onzo_device_id, the.sensorId.value)
        .setString(when_consumption_output_produced, java.time.format.DateTimeFormatter.ISO_INSTANT.format(the.when))
  }

  override def persist(data: ServiceMetricsData): Try[Unit] = Try {
    session.execute(bindStatement(data))
  }
}

object ServiceMetricsCassandraPersister {
  def create(session: Session, keySpace: String): ServiceMetricsCassandraPersister = {
    new ServiceMetricsCassandraPersister(session, keySpace)
  }
}
