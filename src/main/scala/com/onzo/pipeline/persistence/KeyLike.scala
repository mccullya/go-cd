package com.onzo.pipeline.persistence

import pld.data.ConsumptionCategory
import pld.data.ConsumptionCategory._

/**
  * Created by toby.hobson on 10/03/2017.
  */
trait KeyLike[T] {
  def asKey(t: T): String
}

object CategoryKeys {
  implicit val categoryKeyLike = new KeyLike[ConsumptionCategory] {
    override def asKey(category: ConsumptionCategory): String = category.toString
  }
}
