package com.onzo.pipeline.persistence

import java.time._
import java.util.Date

import com.datastax.driver.core.{Session, UDTValue, UserType}
import com.onzo.common.domain.{ClientId, HouseholdId, OnzoDeviceId, SensorId}
import com.onzo.pipeline.domain._
import squants.energy.{WattHours, Watts}
import squants.market.EUR

import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

class ElectricConsumptionCassandraPersister private[persistence] (session: Session, val keySpace: String)
  extends Persistable[WindowedElectricConsumption] with CassandraConstants {

  private val InsertCql: String =
    s"""
       |INSERT INTO $keySpace.$consumption_intraday_elec_by_household_Table (
       |    $organisation_id,
       |    $onzo_household_id,
       |    $onzo_device_id,
       |    $day,
       |    $resolution_seconds,
       |    $timezone,
       |    $consumption,
       |    $energy_wh,
       |    $power_w,
       |    $cost
       |) VALUES (
       |    :$organisation_id,
       |    :$onzo_household_id,
       |    :$onzo_device_id,
       |    :$day,
       |    :$resolution_seconds,
       |    :$timezone,
       |    :$consumption,
       |    :$energy_wh,
       |    :$power_w,
       |    :$cost
       |)
    """.stripMargin

  private val electricConsumptionUDT: UserType = {
    session
      .getCluster
      .getMetadata
      .getKeyspace(keySpace)
      .getUserType(electric_consumption)
  }

  private val insertPreparedStatement = session.prepare(InsertCql)

  override def persist(t: WindowedElectricConsumption): Try[Unit] = {
    Try(insertPreparedStatement.bind()
      .setInt(organisation_id, t.clientId.value)
      .setString(onzo_household_id, t.onzoHouseholdId.value)
      .setString(onzo_device_id, t.onzoDeviceId.value)
      .setDate(day, t.day.underlying.asCassandraDate)
      .setInt(resolution_seconds, t.resolution.asSeconds)
      .setString(timezone, t.timezone.getId)
      .setMap(consumption, t.intervals.asCassandraMap, classOf[Date], classOf[UDTValue])
      .setDecimal(energy_wh, BigDecimal(t.energy.toWattHours).bigDecimal)
      .setDecimal(power_w, BigDecimal(t.averagePower.toWatts).bigDecimal)
      .setDecimal(cost, BigDecimal(t.cost.value).bigDecimal)
    ) flatMap (boundStatement => Try(session.execute(boundStatement)))
  }

  implicit class RichElectricConsumption(underlying: Map[ZonedDateTime, ElectricConsumption]) {
    def asCassandraMap: java.util.Map[Date, UDTValue] = {
      underlying.map {
        case (timestamp, ec) =>
          val key = Date.from(timestamp.toInstant)
          val value = electricConsumptionUDT
            .newValue()
            .setDecimal(energy_wh, BigDecimal(ec.energy.toWattHours).bigDecimal)
            .setDecimal(power_w, BigDecimal(ec.power.toWatts).bigDecimal)
            .setDecimal(cost, BigDecimal(ec.cost.value).bigDecimal)
          key -> value
      }.asJava
    }
  }
}

object ElectricConsumptionPersister {
  def create(session: Session, keySpace: String)(implicit executionContext: ExecutionContext): ElectricConsumptionPersister = {
    new ElectricConsumptionCassandraPersister(session, keySpace)
  } andThen {
    ServiceMetricsCassandraPersister.create(session, keySpace).contramap { consumption =>
      ConsumptionOutputWasProduced(
        clientId = consumption.clientId,
        householdId = consumption.onzoHouseholdId,
        sensorId = SensorId(consumption.onzoDeviceId.value)
      )
    }
  }
}
