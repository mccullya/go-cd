package com.onzo.pipeline.persistence

import com.datastax.driver.core.{BoundStatement, Row, Session}
import com.onzo.common.domain.{ClientId, HouseholdId}
import com.onzo.pipeline.domain.{HouseholdProfile, HouseholdProfileEntity}

import scala.concurrent.ExecutionContext
import scala.util.Try

/**
  * Created by toby.hobson on 18/04/2017.
  */
class HouseholdProfilePersister private(session: Session, val keySpace: String)
  extends Persistable[HouseholdProfileEntity]
    with FindHouseholdProfileById
    with CassandraConstants {

  private val TableName: String = "household_profile"

  private val InsertCql: String =
    s"""
       |INSERT INTO $keySpace.$TableName (
       |    $organisation_id,
       |    $onzo_household_id,
       |    $house_type,
       |    $number_of_people,
       |    $people_plus,
       |    $number_of_bedrooms,
       |    $bedrooms_plus,
       |    $electric_heating,
       |    $top_up_electric_heating,
       |    $electric_hot_water,
       |    $electric_cooking,
       |    $top_up_electric_cooking,
       |    $gas_heating,
       |    $gas_cooking,
       |    $gas_hot_water,
       |    $compressive_cooling,
       |    $fan_cooling
       |) VALUES (
       |    :$organisation_id,
       |    :$onzo_household_id,
       |    :$house_type,
       |    :$number_of_people,
       |    :$people_plus,
       |    :$number_of_bedrooms,
       |    :$bedrooms_plus,
       |    :$electric_heating,
       |    :$top_up_electric_heating,
       |    :$electric_hot_water,
       |    :$electric_cooking,
       |    :$top_up_electric_cooking,
       |    :$gas_heating,
       |    :$gas_cooking,
       |    :$gas_hot_water,
       |    :$compressive_cooling,
       |    :$fan_cooling
       |)
    """.stripMargin

  private val SelectCql: String = s"SELECT * FROM $keySpace.$TableName WHERE $organisation_id = :$organisation_id AND $onzo_household_id = :$onzo_household_id"

  private val insertPreparedStatement = session.prepare(InsertCql)

  private val selectPreparedStatement = session.prepare(SelectCql)

  override def persist(t: HouseholdProfileEntity): Try[Unit] = {
    Try {
      insertPreparedStatement.bind()
        .setInt(organisation_id, t.clientId.value)
        .setString(onzo_household_id, t.onzoHouseholdId.value)
        .setOptionalString(house_type, t.householdProfile.houseType)
        .setOptionalInt(number_of_people, t.householdProfile.numberOfPeople)
        .setOptionalBoolean(people_plus, t.householdProfile.peoplePlus)
        .setOptionalInt(number_of_bedrooms, t.householdProfile.numberOfBedrooms)
        .setOptionalBoolean(bedrooms_plus, t.householdProfile.bedroomsPlus)
        .setOptionalBoolean(electric_heating, t.householdProfile.electricHeating)
        .setOptionalBoolean(top_up_electric_heating, t.householdProfile.topUpElectricHeating)
        .setOptionalBoolean(electric_hot_water, t.householdProfile.electricHotWater)
        .setOptionalBoolean(electric_cooking, t.householdProfile.electricCooking)
        .setOptionalBoolean(top_up_electric_cooking, t.householdProfile.topUpElectricCooking)
        .setOptionalBoolean(gas_heating, t.householdProfile.gasHeating)
        .setOptionalBoolean(gas_cooking, t.householdProfile.gasCooking)
        .setOptionalBoolean(gas_hot_water, t.householdProfile.gasHotWater)
        .setOptionalBoolean(compressive_cooling, t.householdProfile.compressiveCooling)
        .setOptionalBoolean(fan_cooling, t.householdProfile.fanCooling)
    } flatMap (boundStatement => Try(session.execute(boundStatement)))
  }

  override def findProfileById(clientId: ClientId, onzoHouseholdId: HouseholdId): Try[HouseholdProfile] = {
    Try(
      selectPreparedStatement
        .bind()
        .setInt(organisation_id, clientId.value)
        .setString(onzo_household_id, onzoHouseholdId.value)
    ).flatMap(boundStatement => Try(session.execute(boundStatement))).map(_.one()).map { row =>
      val readBool = readOptionalBoolean(row, _: String)
      HouseholdProfile(
        houseType = Option(row.getString(house_type)),
        numberOfPeople = Option(row.getInt(number_of_people)),
        peoplePlus = Option(row.getBool(people_plus)),
        numberOfBedrooms = Option(row.getInt(number_of_bedrooms)),
        bedroomsPlus = Option(row.getBool(bedrooms_plus)),
        // Electric
        electricHeating = readBool(electric_heating),
        topUpElectricHeating = Option(row.getBool(top_up_electric_heating)),
        electricHotWater = readBool(electric_hot_water),
        electricCooking = Option(row.getBool(electric_cooking)),
        topUpElectricCooking = Option(row.getBool(top_up_electric_cooking)),
        // Gas
        gasHeating = readBool(gas_heating),
        gasCooking = readBool(gas_cooking),
        gasHotWater = readBool(gas_hot_water),
        // Cooling
        compressiveCooling = Option(row.getBool(compressive_cooling)),
        fanCooling = Option(row.getBool(fan_cooling))
      )
    }
  }

  implicit class RichBoundStatement(underlying: BoundStatement) {

    def setOptionalString(key: String, maybeA: Option[String]): BoundStatement = setOption(key, maybeA)

    def setOptionalInt(key: String, maybeA: Option[Int]): BoundStatement = setOption(key, maybeA)

    def setOptionalBoolean(key: String, maybeA: Option[Boolean]): BoundStatement = setOption(key, maybeA)

    private def setOption(key: String, maybeA: Option[Any]): BoundStatement = {
      maybeA match {
        case Some(a) => a match {
          case s: String => underlying.setString(key, s)
          case i: Int => underlying.setInt(key, i)
          case b: Boolean => underlying.setBool(key, b)
          case _ => throw new IllegalArgumentException(s"Bind setter not implemented for ${a.getClass.getSimpleName}")
        }
        case None => underlying
      }
    }
  }

  private def readOptionalBoolean(row: Row, column: String): Option[Boolean] = {
    if (row.isNull(column)) Option.empty[Boolean]
    else Some(row.getBool(column))
  }
}

object HouseholdProfilePersister {
  def create(session: Session, keySpace: String)(implicit executionContext: ExecutionContext): HouseholdProfilePersister = {
    new HouseholdProfilePersister(session, keySpace)
  }
}
