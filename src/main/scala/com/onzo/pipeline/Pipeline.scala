package com.onzo.pipeline

import java.text.NumberFormat

import akka.NotUsed
import akka.actor.{ActorSystem, Scheduler}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import com.onzo.collector.domain.FileType
import com.onzo.collector.io.ParseResultSourceHandle
import com.onzo.collector.parser.ParseError
import com.onzo.common.domain.{ClientId, HouseholdId}
import com.onzo.etl.persistence.EtlActor
import com.onzo.etl.persistence.csr.{CsrActor, HouseholdApiUsersRepository}
import com.onzo.pipeline.Flows.{GraphLike, PipelineGraph}
import com.onzo.pipeline.Pipeline.ClientResult
import com.onzo.pipeline.config.Parser.camelCaseHint
import com.onzo.pipeline.config._
import com.onzo.pipeline.config.pld.PldConfigurationBuilder
import com.onzo.pipeline.domain.HouseholdProfileEntity.GetProfileIdFn
import com.onzo.pipeline.domain._
import com.onzo.pipeline.persistence.{ElectricBreakdownPersister, ElectricConsumptionPersister, FindAllByUser, GasBreakdownPersister, GasConsumptionPersister}
import com.typesafe.config.Config
import akka.pattern.{after, ask}
import com.onzo.etl.persistence.EtlActor.KillChildren
import com.onzo.pipeline.EtlStatusActor.Reset

import scala.concurrent.duration._
import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Properties

object Pipeline {

  private val format: (Int) => String = NumberFormat.getIntegerInstance.format(_: Int)

  case class ClientResult(name: String, csrCount: Int, electricCount: Int, gasCount: Int) {
    def status: String = {
      s"Pipeline processing results:csr:$csrCount:electric:$electricCount:gas:$gasCount"
    }
  }

  /**
    * Parse the config object to return a set of ClientConfig objects
    * NOTE: key names should not include the period (.) symbol as we have
    * a hack in place that will break if we use periods
    *
    * @param config
    * @return
    */
  def getClients(config: Config): Seq[ClientConfig] = {
    // Don't remove this! its needed for the Parser.parse call
    import com.onzo.pipeline.config.Parser.camelCaseHint
    val clients = config.getConfigList("clients").asScala
    clients.map(clientConfig =>
      ClientConfig(
        id = clientConfig.getInt("id"),
        name = clientConfig.getString("name"),
        timestampPattern = clientConfig.getString("timestampPattern"),
        columnMappings = Parser.parse[ClientColumnMappings](clientConfig.getConfig("columnMappings")),
        pldConfiguration = PldConfigurationBuilder(clientConfig.getConfig("pld"), config)
      )
    )
  }
}

/**
  * Main entry point into the pipeline
  */
class Pipeline()(implicit val system: ActorSystem,
                 config: Config,
                 elecConsumptionPersister: ElectricConsumptionPersister,
                 elecBreakdownPersister: ElectricBreakdownPersister,
                 gasConsumptionPersister: GasConsumptionPersister,
                 gasBreakdownPersister: GasBreakdownPersister,
                 elecConsumptionFindAllByUser: FindAllByUser[WindowedElectricConsumption],
                 householdApiUsersRepository: HouseholdApiUsersRepository,
                 csrActorRef: CsrActorRef,
                 findProfileById: GetProfileIdFn,
                 mat: ActorMaterializer,
                 ec: ExecutionContext) {

  import Pipeline.getClients

  private val _etlActorRef = system.actorOf(EtlActor.props(csrActorRef.underlying, (readings) => readings.size >= 20))
  private val _etlStatusActorRef = system.actorOf(EtlStatusActor.props)
  implicit val etlActorRef = new EtlActorRef(_etlActorRef)
  implicit val etlStatusActorRef = new EtlStatusActorRef(_etlStatusActorRef)
  implicit val bucketName: String = getConfigString("s3.bucket", "S3_BUCKET")
  implicit val scheduler: Scheduler = system.scheduler

  def runPld(clientId: Int, onzoHouseholdId: HouseholdId, pldProfile: HouseholdProfile): Future[Int] = {
    getClients(config).find(_.id == clientId) match {
      case Some(clientConfig) =>
        val pldCount: Future[Int] = runPld(clientConfig, onzoHouseholdId, pldProfile)
        logClientInfo(clientConfig.name)(s"(client id $clientId) Processed $pldCount sensors")
        pldCount
      case None => Future.failed(new Exception(s"Unable to find client config for client id $clientId"))
    }
  }

  def runPld(clientConfig: ClientConfig, onzoHouseholdId: HouseholdId, pldProfile: HouseholdProfile): Future[Int] = {
    val source: Source[WindowedElectricConsumption, NotUsed] = Source.fromIterator(() =>
      elecConsumptionFindAllByUser.findAllByUser(ClientId(clientConfig.id), onzoHouseholdId)
    )
    // Note: we used the household profile passed instead of looking up using findProfileById, this is to avoid
    // the eventual consistency issues with cassandra
    val pldFlow = Flows.pldElectricGraph()(clientConfig, ec, elecBreakdownPersister, (a: ClientId, b: HouseholdId) => HouseholdProfileEntity(a, b, pldProfile))
    source.via(pldFlow).runFold(0)(_ + _)
  }

  def runClients(): Future[Iterable[ClientResult]] = {
    val clients = getClients(config)
    val eventualCounts = clients.map(runClient)
    Future.sequence(eventualCounts)
  }

  def runClient(clientName: String): Future[ClientResult] = {
    getClients(config).find(_.name == clientName) match {
      case Some(clientConfig) => runClient(clientConfig)
      case None => Future.failed(new Exception(s"Unable to find client config for $clientName"))
    }
  }

  def runClient(clientConfig: ClientConfig): Future[ClientResult] = {
    import com.onzo.pipeline.Flows.{csrGraphEv, electricConsumptionGraphEv, gasConsumptionGraphEv}
    implicit val iClientConfig = clientConfig

    def processStream[T](dataType: String, wrapper: ParseResultSourceHandle[T])(implicit graph: PipelineGraph[T]): Future[Int] = {
      logInfo(s"Processing $dataType: ${wrapper.toString()} ...")
      wrapper.source().via(graph).runFold(0) { (acc, count) =>
        if (acc % 1000 == 0) logInfo(s"Processed $acc $dataType records")
        acc + count
      }.flatMap { count =>
        logInfo(s"Moving $dataType: ${wrapper.toString()} to archive")
        wrapper.moveOriginToArchive().map(_ => count)
      }.recoverWith {
        case t: Throwable =>
          logError("Pipeline error: " + t.getMessage)
          wrapper.moveOriginToError().map(_ => 0)
      }
    }

    /**
      * Materialise flows sequentially for each stream passed to us by the batch collector
      * as it's much more efficient for the ETL to receive an ordered stream of Sensor Readings.
      * We wouldn't get this if we materialised the flows in parallel
      *
      * @param handles
      * @return
      */
    def loop[T: GraphLike](dataType: String, handles: List[ParseResultSourceHandle[T]]): Future[Int] = {
      implicit val graph = implicitly[GraphLike[T]].graph
      handles match {
        case (x :: xs) => processStream(dataType, x).flatMap(count => loop(dataType, xs).map(_ + count))
        case _ => Future.successful(0)
      }
    }

    for {
      _ <- ask(_etlStatusActorRef, Reset)(5.seconds)
      csrCount <- Flows.s3Sources(FileType.CsrRecord).map(_.toList).flatMap(x => loop("CSR", x))
      electricCount <- Flows.s3Sources(FileType.ElectricConsumption).map(_.toList).flatMap(x => loop("Electric consumption", x))
      gasCount <- Flows.s3Sources(FileType.GasConsumption).map(_.toList).flatMap(x => loop("Gas consumption", x))
      // Send a kill message to the etlActor to kill all the children (sensor days)
      _ = _etlActorRef ! KillChildren
      // HORRIBLE - Give the system 10 seconds to kill the actors off
      _ <- after(10.seconds, system.scheduler)(Future.successful(()))
    } yield ClientResult(clientConfig.name, csrCount, electricCount, gasCount)
  }

  private def getConfigString(configKey: String, envVariableName: String)(implicit system: ActorSystem): String = {
    Properties.envOrNone(envVariableName).getOrElse(config.getString(configKey))
  }

}
