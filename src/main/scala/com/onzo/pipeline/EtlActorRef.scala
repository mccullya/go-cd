package com.onzo.pipeline

import akka.actor.ActorRef

class EtlActorRef(val underlying: ActorRef) extends AnyVal
