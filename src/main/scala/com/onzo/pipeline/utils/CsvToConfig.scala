package com.onzo.pipeline.utils

import java.io.File
import java.time.Month

import scala.io.Source

/**
  * Rough n'ready script to convert CSV test files to Hocon config via
  * atlas case classes & Typesafe config.
  * ( see https://onzoinc.atlassian.net/browse/ONZO-1052 )
  */
object CsvToConfig {

  // Map names from the csv's category column to the conf's category type:
  private val categoryReplacements = Map(
    "always_on" -> "alwayson",
    "water_heating" -> "waterheating",
    "refrigeration" -> "refrigeration",
    "laundry_dishwashing" -> "laundrydishwashing",
    "home_entertainment" -> "homeentertainment"
  )

  private def replaceCategory(from: String) = categoryReplacements.getOrElse(from, from)

  // Map the csv's heading names to the conf's "limit" type names:
  private val headingReplacements = Map(
    "baseload" -> "baseloadpercent",
    "min_total_absolute" -> "mintotalabsolute",
    "max_category_absolute" -> "maxcategoryabsolutecapraw"
  )

  private val typeNameReplacements = Map(
    "elecHeating" -> "electricheating",
    "elecHotWater" -> "electrichotwater"
  )

  private def replaceTypeName(from: String) = typeNameReplacements.getOrElse(from, from)

  type Headings = Array[(String, Int)]

  case class ConfType(name: String, canProcess: (String) => Boolean, processLine: (List[String], Int, Headings, Headings) => Option[String],
                      iterateByRow: Boolean = true,
                      head: String, tail: String, onlyMappedHeadings: Boolean)

  val confTypes = List(
    ConfType(
      "Commodity limits",
      canProcess = _ startsWith "category,baseload,",
      processLine = processContributionsLine,
      head =
        """pld.categoryLimits {
          |  commoditylimit: [
          |    {
          |      profiletype: {commodity: {type: electric}},
          |      contribution: [
          |""".stripMargin,

      tail =
        """      ]
          |    }
          |  ]
          |}
          |""".stripMargin,
      onlyMappedHeadings = true
    ),
    ConfType(
      "Load distributions by time of day",
      canProcess = _ startsWith "category,00:00,00:30,",
      processLine = processLoadDistributionsLine,
      head =
        """pld.timeOfDay {
          |  distributions: [
          |    {
          |      profiletype: {commodity: {type: electric}},
          |      summer: {
          |        loaddistributions: [
        """.stripMargin,
      tail =
        """         ]
          |       }
          |     }
          |   ]
          |}
        """.stripMargin,
      onlyMappedHeadings = false
    ),
    ConfType(
      "Seasonal adjustments (by month)",
      canProcess = _ == "category,1,2,3,4,5,6,7,8,9,10,11,12",
      processLine = processSeasonalAdjustmentsColumn,
      iterateByRow = false,
      head =
        """
          |pld.seasonalAdjustment {
          |  locationcoefficients: [
          |    {
          |      location: {
          |        place: { name: OnzoHQ },
          |        north: 51.519570°,
          |        east: -0.168398°
          |      },
          |      profiletype: {commodity: {type: electric}},
          |      coefficients: [
          |""".stripMargin,
      tail =
        """
          |  ]
          |}
          |""".stripMargin,
      onlyMappedHeadings = false
    ),
    ConfType(
      "Default weights",
      canProcess = _ startsWith "category,elecHeating_elecHotWater,",
      processLine = processDefaultWeightsColumn,
      iterateByRow = false,
      head =
        """
          |pld {
          |  europe {
          |    defaultWeights: [
          |      {
          |        profileType: {commodity: {type: electric}}
          |        allowHas: [
          |          {type: electricheating}
          |          {type: electrichotwater}
          |        ]
          |        profile: [
          |""".stripMargin,
      tail =
        """
          |        ]
          |      }
          |    ]
          |  }
          |}
          |""".stripMargin,
      onlyMappedHeadings = false
    )
  )


  def main(args: Array[String]): Unit = {
    if (args.length != 1) {
      println("Usage:  CsvToConfig myFile.csv")
      println("Will output myFile.conf")
      System.exit(0)
    }

    val name = args(0)
    val file = new File(name)
    if (!file.exists) {
      println("File doesn't exist: " + file.getAbsolutePath)
      System.exit(1)
    }
    if (!file.getName.endsWith(".csv")) {
      println("Filename must end in .csv: " + file.getAbsolutePath)
      System.exit(1)
    }

    println("Reading " + name)
    val lines = Source.fromFile(file, "UTF-8").getLines
    val firstLine = lines.next()
    val confType = confTypes.find(_.canProcess(firstLine)).getOrElse {
      println("Sorry, I don't know how to handle this file.")
      System.exit(1)
      confTypes.head
    }
    println("Detected config type: " + confType.name)

    val headings =
      firstLine.split(",").zipWithIndex.flatMap { case (headingName, col) =>
        headingReplacements.get(headingName)
          .orElse(if (confType.onlyMappedHeadings) None else Some(headingName))
          .map(_ -> col)
      }

    val hocon =
      if (confType.iterateByRow) // process by row:
        lines.zipWithIndex.flatMap { case (line, num) =>
          confType.processLine(line.split(",").toList, num, headings, headings)
        }.mkString("\n")
      else { // process by column:
        val linesL = lines.map(_.split(",")).toList
        val categories = linesL.map(_.head).zipWithIndex.toArray
        val rowsOfCols = linesL.map(_.tail)
        val rotated = rowsOfCols.transpose
        rotated.zipWithIndex.flatMap { case (col, num) =>
          confType.processLine(col, num, headings, categories)
        }.mkString("\n")
      }

    writeToFile(name.replaceFirst("\\.csv$", ".conf"), confType.head + hocon + confType.tail)
  }

  def processContributionsLine(values: List[String], num: Int, headings: Headings, categories: Headings) = {
    val category = replaceCategory(values.head)

    val types = headings.flatMap { case (typeName, col) =>
      if (col < values.length && values(col) != null && values(col) != "")
        Some(s"""            {type: $typeName, v: ${values(col)}}""")
      else
        None
    }.mkString("\n")

    Some(
      s"""        {
         |          category: {type: $category},
         |          limit: [
         |$types
         |          ]
         |        },
         |""".stripMargin
    )
  }

  // for summer or winter. Contains time-of-day values for each category.
  // e.g. electricity_eu_activity_summer_load_distributions.csv
  def processLoadDistributionsLine(values: List[String], num: Int, headings: Headings, categories: Headings) = {
    val category = replaceCategory(values.head)

    val head =
      s"""         {
         |           category: {type: $category}
         |           sourceloads: [
         |""".stripMargin
    val tail =
      s"""
         |           ]
         |         }
         |""".stripMargin

    val inner = values.tail.zipWithIndex.flatMap { case (value, col) =>
      val col1 = col + 1
      if (col1 >= headings.length) None
      else Some(s"""             {from: "${headings(col1)._1}", coefficient: {underlying: $value}}""")
    }.mkString("\n")

    Some(head + inner + tail)
  }

  // contains monthly values (1-12) for each category.
  // e.g. electricity_eu_activity_seasonal_adjustments.csv
  // values are for each category for one month;
  // the first element in values is the month name.
  def processSeasonalAdjustmentsColumn(values: List[String], num: Int, headings: Headings, categories: Headings) = {
    val month = Month.of(num + 1).name.toLowerCase
    val head =
      s"""
         |        {
         |          month: {type: $month},
         |          coefficient:
         |            [
         |""".stripMargin
    val tail =
      """
        |            ]
        |        }
        |""".stripMargin

    val inner = values.zipWithIndex.map { case (v, i) =>
      val category = replaceCategory(categories(i)._1)
      s"""              {category: {type: $category}, coefficient: {value: $v}}"""
    }.mkString("\n")

    Some(head + inner + tail)
  }

  def processDefaultWeightsColumn(values: List[String], num: Int, headings: Headings, categories: Headings) = {
    val num1 = num + 1
    val columnName = headings(num1)._1
    val hases = columnName.split("_").filterNot(_.startsWith("no")) // expect to be size 0,1 or 2
      .map(has => s"              {type: ${replaceTypeName(has)}}").mkString("\n")

    val weights = values.zipWithIndex.map { case (v, i) =>
      val category = replaceCategory(categories(i)._1)
      s"              {category: {type: $category}, default: {underlying: $v}}"
    }.mkString("\n")

    Some(
      s"""          {
         |            has: [
         |$hases
         |            ]
         |            categoryWeights: [
         |$weights
         |            ]
         |          }
     """.stripMargin)
  }

  def writeToFile(filename: String, contents: String) = {
    import java.nio.charset.StandardCharsets
    import java.nio.file.{Files, Paths}

    Files.write(Paths.get(filename), contents.getBytes(StandardCharsets.UTF_8))
    println("Written " + filename)
  }
}
