package com.onzo.pipeline

import akka.actor.ActorRef

class EtlStatusActorRef(val underlying: ActorRef) extends AnyVal