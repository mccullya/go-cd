package com.onzo.pipeline.config

import com.onzo.pipeline._
import com.typesafe.config.Config
import pureconfig._

object Parser {

  implicit def camelCaseHint[T]: ProductHint[T] = ProductHint[T](ConfigFieldMapping(CamelCase, CamelCase))

  def parse[T](conf: Config)(implicit convert: ConfigConvert[T]): T = {
    val loaded = loadConfig[T](conf)(convert)
    loaded.left.foreach(_.toList.map(_.toString).foreach(f => logError(f)))
    loaded.right.get
  }

}
