package com.onzo.pipeline.config

import akka.actor.ActorSystem
import com.onzo.pipeline.config.pld.PldConfiguration
import com.typesafe.config.Config

class ModularConfiguration private(implicit system: ActorSystem) {
  val config: Config =
    ModularConfiguration.clientConfig
      .withFallback(system.settings.config)
}

object ModularConfiguration {
  def apply()(implicit system: ActorSystem): ModularConfiguration =
    new ModularConfiguration()

  val clientConfig: Config =
    PldConfiguration.config
      .withFallback(ClientConfig.config)
      .resolve()
}
