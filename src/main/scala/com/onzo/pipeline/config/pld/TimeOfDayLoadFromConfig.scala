package com.onzo.pipeline.config.pld

import java.time.format.DateTimeFormatter.ISO_LOCAL_TIME
import java.time.LocalTime
import java.time.Month
import java.time.Month._

import cats.data.NonEmptyList
import cats.implicits._
import com.onzo.common.{Percent, ProfileType}
import com.onzo.pipeline.config.pld.TimeOfDayLoadFromConfig._
import com.onzo.pipeline.domain.RunDay
import com.typesafe.config.Config
import datatypes.{Error => AdtError}
import jp.ne.opt.chronoscala.NamespacedImports._
import pld.PipelineInterface.{TimeOfDayCoefficient, TimeOfDayWeightAdjustmentFn}
import pld.data.ConsumptionCategory
import pureconfig._
import pureconfig.error.ConfigReaderFailures
import pureconfig.module.squants._
import pureconfiglocal.java8time.configurable._
import org.scalactic.TypeCheckedTripleEquals._
import shapeless._
import squants._
import util.catslocal.RichNonEmptyList._
import util.scalalocal.RichMap._

class TimeOfDayLoadFromConfig private (config: Config)(implicit conv: ConfigConvert[CommodityLoadDistribution]) {

  val timeOfDayConfig: Either[AdtError, List[TimeOfDayBySeason]] =
    for {
      parsed <- loadConfig[CommodityLoadDistribution] (config, configPath)
        .left.map { e => AdtError("ConfigReaderFailures: " + e.toList.mkString(" | "), None) }
      timeOfDayLoadBySeason <- parsed.distributions.map(_.unwrap).sequenceU
    } yield timeOfDayLoadBySeason

  private[pld] def atBandStart(compare: ZonedDateTime, to: LocalTime): Boolean =
    compare.getHour.equals(to.getHour) && compare.getMinute.equals(to.getMinute)

  private[pld] def isBeforeBandStart(compare: ZonedDateTime, to: LocalTime): Boolean =
    (compare.getHour < to.getHour) ||
      ( compare.getHour.equals(to.getHour) && (compare.getMinute < to.getMinute) )

  private[pld] def isAfterBandStart(compare: ZonedDateTime, to: LocalTime): Boolean =
    (compare.getHour > to.getHour) ||
      ( compare.getHour.equals(to.getHour) && (compare.getMinute > to.getMinute) )

  private[pld] def makeLookup(sourceLoads: NonEmptyList[SourceLoad]): Either[AdtError, TimeOfDayWeightAdjustmentFn] = {
    def lookup(t: ZonedDateTime): TimeOfDayCoefficient = {
      val r: Option[SourceLoad] = sourceLoads.toList.find((a: SourceLoad) => {
        val equal: Boolean = atBandStart(t, a.from)
        val after: Boolean = isAfterBandStart(t, a.from)
        equal || after
      })
      r.map(_.coefficient).getOrElse(sourceLoads.head.coefficient)
    }
    if (sourceLoads.head.from === LocalTime.MIDNIGHT) Right(lookup)
    else Left(AdtError(s"source load times must start at midnight but first is ${sourceLoads.head}", None))
  }

  def timeOfDayAdjustment(runDay: RunDay, category: ConsumptionCategory, profileType: ProfileType): Either[AdtError, TimeOfDayWeightAdjustmentFn] =
    for {
      timeOfDayLoadBySeason <- timeOfDayConfig
      commodityTimeOfDay <- timeOfDayLoadBySeason.find(a => a.profile.equals(profileType)).toRight(AdtError(s"no source loads found for commodity $profileType", None))
      weighted <- commodityTimeOfDay.applySeasonalWeight(runDay)
      categoryDistribution <- weighted.loaddistributions.find(b => b.category == category)
        .toRight(AdtError(s"no source load distribution available for $category", None))
      sorted = categoryDistribution.sourceloads.sortBy(_.from)
      loadsFromMidnight <- if (sorted.head.from == LocalTime.MIDNIGHT) Right(sorted)
        else Left(AdtError(s"source loads must start from midnight but first is ${sorted.head.coefficient}", None))
      lookup <- makeLookup(loadsFromMidnight)
    } yield lookup
}
object TimeOfDayLoadFromConfig {
  val configPath: String = "pld.timeOfDay"

  implicit val localtimeConfigConvert: ConfigConvert[LocalTime] = localTimeConfigConvert(ISO_LOCAL_TIME)
  case class SourceLoad(from: LocalTime, coefficient: TimeOfDayCoefficient) {
    def applyWeight(w: Double): SourceLoad =
      SourceLoad(from, TimeOfDayCoefficient(coefficient.underlying * w))
  }
  object SourceLoad {
    def add(a: SourceLoad, b: SourceLoad): Either[AdtError, SourceLoad] = {
      if (a.from == b.from) Right(SourceLoad(a.from, TimeOfDayCoefficient(a.coefficient.underlying + b.coefficient.underlying)))
      else Left(AdtError(s"a choice has been made not to combine unaligned TimeOfDayCoefficients ${a.from} and ${b.from}", None))
    }
  }

  case class CategoryDistribution(category: ConsumptionCategory, sourceloads: NonEmptyList[SourceLoad]) {
    def applyWeight(w: Double): CategoryDistribution =
      CategoryDistribution(category, sourceloads.map(_.applyWeight(w)))

    def loadsMap: Map[LocalTime, SourceLoad] = sourceloads.toList.map((c: SourceLoad) => (c.from, c)).toMap
  }
  object CategoryDistribution {
    def merge(a: CategoryDistribution, b: CategoryDistribution): Either[AdtError, CategoryDistribution] = {
      if (a.category == b.category) a.loadsMap.merge[Either[AdtError, SourceLoad]](b.loadsMap, SourceLoad.add).values.toList.sequenceU
        .map { x => CategoryDistribution(a.category, NonEmptyList.fromListUnsafe(x)) }
      else Left(AdtError(s"cannot merge category distribution of category ${a.category} with category distribution of category ${b.category}", None))
    }
  }
  case class CategoryDistributionNelWrapUnsafe(category: ConsumptionCategory, sourceloads: List[SourceLoad]) {
    def unwrap: Either[AdtError, CategoryDistribution] =
      for {
        nel <- NonEmptyList.fromList(sourceloads.sortBy(_.from))
          .toRight(AdtError(s"cannot build source loads from empty source load list for category $category", None))
      } yield CategoryDistribution(category, nel)
  }

  case class TimeOfDayLoad(loaddistributions: NonEmptyList[CategoryDistribution]) {
    def applyWeight(w: Double): TimeOfDayLoad =
      TimeOfDayLoad(loaddistributions.map(_.applyWeight(w)))

    def asMap: Map[ConsumptionCategory, CategoryDistribution] = loaddistributions.toList.map((c: CategoryDistribution) => (c.category, c)).toMap

    def merge(other: TimeOfDayLoad): Either[AdtError, TimeOfDayLoad] =
      asMap.merge[Either[AdtError, CategoryDistribution]](other.asMap, CategoryDistribution.merge).values.toList.sequenceU
        .map { x => TimeOfDayLoad(NonEmptyList.fromListUnsafe(x)) }
  }

  case class TimeOfDayLoadNelWrapUnsafe(loaddistributions: List[CategoryDistributionNelWrapUnsafe]) {
    def unwrap: Either[AdtError, TimeOfDayLoad] =
      for {
        tryCategoryDistributions <- NonEmptyList.fromList(loaddistributions.map(_.unwrap))
          .toRight(AdtError(s"cannot build load distributions from empty load distribution list found at configuration path $configPath", None))
        categoryDistributions <- tryCategoryDistributions.sequenceU
      } yield TimeOfDayLoad(categoryDistributions)
  }

  // TODO ATLAS-136 configure seasonal time of day weights
  case class TimeOfDayBySeason(profile: ProfileType, summer: TimeOfDayLoad, winter: TimeOfDayLoad, summerallocation: SummerPercentage) {
    private def mixLoadData(weightSummer: Percent): Either[AdtError, TimeOfDayLoad] = {
      summer.applyWeight(weightSummer.normal).merge(winter.applyWeight(weightSummer.oneMinus))
    }
    def applySeasonalWeight(runDay: RunDay): Either[AdtError, TimeOfDayLoad] = {
      val month: Month = runDay.underlying.getMonth
      month match {
        case JANUARY => mixLoadData(summerallocation.january)
        case FEBRUARY => mixLoadData(summerallocation.february)
        case MARCH => mixLoadData(summerallocation.march)
        case APRIL => mixLoadData(summerallocation.april)
        case MAY => mixLoadData(summerallocation.may)
        case JUNE => mixLoadData(summerallocation.june)
        case JULY => mixLoadData(summerallocation.july)
        case AUGUST => mixLoadData(summerallocation.august)
        case SEPTEMBER => mixLoadData(summerallocation.september)
        case OCTOBER => mixLoadData(summerallocation.october)
        case NOVEMBER => mixLoadData(summerallocation.november)
        case DECEMBER => mixLoadData(summerallocation.december)
      }
    }
  }

  case class SummerPercentage(january: Percent, february: Percent, march: Percent,
                              april: Percent, may: Percent, june: Percent,
                              july: Percent, august: Percent, september: Percent,
                              october: Percent, november: Percent, december: Percent)

  case class TimeOfDayBySeasonWrapUnsafe(profiletype: ProfileType, summer: TimeOfDayLoadNelWrapUnsafe, winter: TimeOfDayLoadNelWrapUnsafe, summerallocation: SummerPercentage) {
    def unwrap: Either[AdtError, TimeOfDayBySeason]= {
      for {
        s <- summer.unwrap
        w <- winter.unwrap
      } yield TimeOfDayBySeason(profiletype, s, w, summerallocation)
    }
  }

  case class CommodityLoadDistribution(distributions: List[TimeOfDayBySeasonWrapUnsafe])

  def apply(config: Config)(implicit conv: ConfigConvert[CommodityLoadDistribution]): TimeOfDayLoadFromConfig =
    new TimeOfDayLoadFromConfig(config)
}