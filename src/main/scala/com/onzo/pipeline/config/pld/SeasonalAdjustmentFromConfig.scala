package com.onzo.pipeline.config.pld

import java.time.{Month => JMonth}

import cats.data.{NonEmptyList => Nel}
import com.onzo.common.Month._
import com.onzo.common._
import com.onzo.common.config.PureConfigError._
import com.onzo.pipeline.config.pld.DefaultWeightConfiguration.generateDefaultWeight
import com.onzo.pipeline.config.pld.SeasonalAdjustmentFromConfig._
import com.onzo.pipeline.domain.RunDay
import com.typesafe.config.Config
import datatypes.Error._
import datatypes.{Error => AdtError}
import pld.PipelineInterface.PLDWeight
import pld.data.ConsumptionCategory
import pld.data.input.SeasonalAdjustment
import pld.data.input.SeasonalAdjustment.{SeasonalAdjustmentFn, SeasonalCoefficient}
import pureconfig.{ConfigConvert, _}
import pureconfig.error.ConfigReaderFailures

import scala.collection.immutable.Map

class SeasonalAdjustmentFromConfig private (config: Config)(implicit conv: ConfigConvert[LocationCoefficient]) {

  val seasonalConfig: Either[ConfigReaderFailures, LocationCoefficient] =
    loadConfig[LocationCoefficient](config, configPath)

  private[pld] val pldSeasonalConfig: Either[AdtError, LocationCoefficient] = {
    seasonalConfig.left.map(_.toError(s"configuration failure when looking up $configPath"))
  }

  private[pld] def adapted: (Location, RunDay, ProfileType) => SeasonalAdjustmentFn = seasonalAdjustmentAdapter(_, _, _, oldSeasonalAdjustment)

  private[pld] def seasonalAdjustmentAdapter(location: Location,
                                             runDay: RunDay,
                                             profileType: ProfileType,
                                             in: OldSeasonalAdjustmentFn): SeasonalAdjustmentFn = {
    val x: (ConsumptionCategory) => Either[AdtError, CategoryCoefficient] = {
      in(_, location, runDay, profileType)
    }
    val y: (ConsumptionCategory) => Either[AdtError, SeasonalCoefficient] =
      (c: ConsumptionCategory) => x(c).map(_.coefficient)
    y
  }

  /**
   * N.B. for the moment we use a very simple location match as per Atlas v1 Technical Debt:
   *   https://onzoinc.atlassian.net/wiki/display/ONZO/Atlas+v1+-+Technical+Debt
   */
  def oldSeasonalAdjustment: OldSeasonalAdjustmentFn =
    (category: ConsumptionCategory, location: Location, runDay: RunDay, profileType: ProfileType) =>
      for {
        a <- pldSeasonalConfig
        month = monthFromRunDay(runDay)
        coefficientLocation <- coefficientsForLocation(location, profileType, a)
        coefficientMonth = coefficientsForMonth(month, coefficientLocation)
        categoryCoefficient = monthlyCoefficientForCategory(category, coefficientMonth)
      } yield categoryCoefficient

  def prepareForPldNewStyle(categories: Nel[ConsumptionCategory],
                            location: Location,
                            runDay: RunDay,
                            profileType: ProfileType): Map[ConsumptionCategory, SeasonalCoefficient] =
    categories.map(a => (a, adapted(location, runDay, profileType)(a).right.get))
      .map { case (a, b) => (a, SeasonalCoefficient(b.value)) }
      .toList.toMap

  def commodityProfileDefaults(profileType: ProfileType, defaultWeights: DefaultWeightConfiguration): Option[DefaultWeights] =
    defaultWeights.defaultWeights.find(a => a.profileType == profileType)

  def adjustedWeightsViaOldStyle(defaultWeights: DefaultWeightConfiguration,
                                 householdProfile: HouseholdProfile,
                                 location: Location,
                                 runDay: RunDay,
                                 profileType: ProfileType): Either[AdtError, Map[ConsumptionCategory, PLDWeight]] = for {
    commodityDefaults <- commodityProfileDefaults(profileType, defaultWeights).toRight(AdtError(s"no $profileType profile found for location $location in $defaultWeights", None)) // N.B. location not currently used/implemented here
    relevantProfile =  householdProfile.filter(commodityDefaults.allowHas)
    profileDefaults <- commodityDefaults.profile.find(a => a.has.toSet.equals(relevantProfile.underlying))
      .toRight(AdtError(s"no category default weights for filtered profile $relevantProfile, available: ${commodityDefaults.profile.toList.mkString("/")}", None))
    defaultCategoryWeights = profileDefaults.categoryWeights.map(a => (a.category, a.default))
    defaultWeightFn = generateDefaultWeight(defaultCategoryWeights)
    categories = defaultCategoryWeights.map( a => a._1 )
    seasonalAdjustment = SeasonalAdjustment(defaultWeightFn, adapted(location, runDay, profileType), categories)
    adjusted <- seasonalAdjustment.normalisedCategoryWeightsFn
    pldNewStyle = adjusted.map { case (k, v) => (k, PLDWeight(v.v)) }
  } yield pldNewStyle
}
object SeasonalAdjustmentFromConfig {
  val configPath: String = "pld.seasonalAdjustment"
  case class CategoryCoefficient(category: ConsumptionCategory, coefficient: SeasonalCoefficient)
  type OldSeasonalAdjustmentFn = (ConsumptionCategory, Location, RunDay, ProfileType) => Either[AdtError, CategoryCoefficient]
  // type SeasonalAdjustmentFn = scala.Function1[pld.data.ConsumptionCategory, scala.Either[datatypes.Error, SeasonalAdjustment.SeasonalCoefficient]]


  def apply(config: Config)(implicit conv: ConfigConvert[LocationCoefficient]): SeasonalAdjustmentFromConfig =
    new SeasonalAdjustmentFromConfig(config)

  case class CoefficientMonth(month: Month, coefficient: List[CategoryCoefficient])
  case class CoefficientLocation(location: Location, profiletype: ProfileType, coefficients: List[CoefficientMonth])
  case class LocationCoefficient(locationcoefficients: List[CoefficientLocation])

  private[pld] def monthFromRunDay(d: RunDay): Month = d.underlying.getMonth match {
    case JMonth.JANUARY   => January
    case JMonth.FEBRUARY  => February
    case JMonth.MARCH     => March
    case JMonth.APRIL     => April
    case JMonth.MAY       => May
    case JMonth.JUNE      => June
    case JMonth.JULY      => July
    case JMonth.AUGUST    => August
    case JMonth.SEPTEMBER => September
    case JMonth.OCTOBER   => October
    case JMonth.NOVEMBER  => November
    case JMonth.DECEMBER  => December
  }

  private[pld] def coefficientsForLocation(l: Location, p: ProfileType, c: LocationCoefficient): Either[AdtError, CoefficientLocation] =
    c.locationcoefficients.find( a => (a.location == l) && (a.profiletype == p) ).
      toEitherError(s"No seasonal coefficients configuration found for location $l." +
        "A location must be configured even if it has no coefficients" +
        " configured in which case the default value of 1 will be used for any coefficients not configured")

  private[pld] def coefficientsForMonth(m: Month, c: CoefficientLocation): CoefficientMonth =
    c.coefficients.find(_.  month == m).getOrElse(CoefficientMonth(m, List()))

  private[pld] def monthlyCoefficientForCategory(category: ConsumptionCategory,
    coefficients: CoefficientMonth): CategoryCoefficient =
    coefficients.coefficient.find(_.category == category).
      getOrElse(CategoryCoefficient(category, SeasonalCoefficient(1.0)))
}