package com.onzo.pipeline.config.pld

import cats.data.NonEmptyList
import cats.implicits._
import com.onzo.common.{ProfileComponent, ProfileType}
import datatypes.{Error => AdtError}
import pld.data.ConsumptionCategory
import pld.data.input.Weight.{DefaultWeightFn, DefaultWeight => PldDefaultWeight}


case class DefaultWeight(underlying: Double) extends AnyVal

case class CategoryDefaultWeight(category: ConsumptionCategory, default: DefaultWeight)

// N.B. temporary List -> Nel workaround used here pending completion of pureconfig cats module

case class ProfileDefaultWeight(has: List[ProfileComponent], categoryWeights: NonEmptyList[CategoryDefaultWeight])
case class ProfileDefaultWeightNelWrapUnsafe(has: List[ProfileComponent], categoryWeights: List[CategoryDefaultWeight]) {
  def unwrap: Either[AdtError, ProfileDefaultWeight] =
    for {
      categoryDefaultWeight <- NonEmptyList.fromList(categoryWeights).toRight(AdtError(s"""cannot build profile default weights from empty category default weight list""", None))
      profileDefaultWeight =  ProfileDefaultWeight(has, categoryDefaultWeight)
    } yield profileDefaultWeight
}

case class DefaultWeights(profileType: ProfileType, allowHas: Set[ProfileComponent], profile: NonEmptyList[ProfileDefaultWeight])
case class DefaultWeightsNelWrapUnsafe(profileType: ProfileType, allowHas: Set[ProfileComponent], profile: List[ProfileDefaultWeightNelWrapUnsafe]) {
  def unwrap: Either[AdtError, DefaultWeights] = {
    for {
      tryProfileDefaultWeight <- NonEmptyList.fromList(profile.map(_.unwrap)).toRight(AdtError(s"cannot build default weights from empty profile default weights", None))
      profileDefaultWeight <- tryProfileDefaultWeight.sequenceU
      defaultWeights = DefaultWeights(profileType, allowHas, profileDefaultWeight)
    } yield defaultWeights
  }
}

case class DefaultWeightConfiguration(defaultWeights: List[DefaultWeights])
case class DefaultWeightConfigurationNelWrapUnsafe(defaultWeights: List[DefaultWeightsNelWrapUnsafe]) {
  def unwrap: Either[AdtError, DefaultWeightConfiguration] =
    for {
      defaultWeights <- defaultWeights.map(_.unwrap).sequenceU
      defaultWeightConfiguration = DefaultWeightConfiguration(defaultWeights)
    } yield defaultWeightConfiguration
}

object DefaultWeightConfiguration {
  def generateDefaultWeight(nel: NonEmptyList[(ConsumptionCategory, DefaultWeight)]): DefaultWeightFn = {
    (c: ConsumptionCategory) => nel.toList.toMap.get(c)
      .map(a => PldDefaultWeight(a.underlying))
      .toRight(AdtError("no default weight for consumption category", None))
  }
}