package com.onzo.pipeline.config.pld

import cats.data.NonEmptyList
import com.typesafe.config.{Config, ConfigFactory}
import com.onzo.common._
import com.onzo.pipeline.config.Parser
import com.onzo.pipeline.config.Parser.camelCaseHint
import com.onzo.pipeline.config.pld.PldConfiguration._
import com.onzo.pipeline.config.pld.SeasonalAdjustmentFromConfig._
import com.onzo.pipeline.config.pld.TimeOfDayLoadFromConfig._
import com.onzo.pipeline.domain.RunDay
import datatypes.{Error => AdtError}
import datatypes.Locations.{Location => PldLocation, Place => PldPlace}
import jp.ne.opt.chronoscala.NamespacedImports._
import pld.PipelineInterface._
import pld.data.ConsumptionCategory
import pld.data.ConsumptionCategory.{AlwaysOn, Refrigeration}
import pld.data.input.CategoryCaps.{BaseLoadPercent, MaxCategoryAbsoluteCapRaw, MinTotalAbsoluteCapRaw, RedistributeFromCap}
import pld.data.input.Weight.{DefaultWeightFn, DefaultWeight => PldDefaultWeight}
import com.typesafe.config.Config
import pld.data.input.SeasonalAdjustment
import pld.data.input.SeasonalAdjustment.NormalisedSeasonallyAdjustedCategoryWeight
import pureconfig._
import pureconfig.module.squants._
import squants.space._
import util.catslocal.RichNonEmptyList._
import util.scalalocal.RichEither._

import scala.util.Try

case class CategoryParameters(parameters: NonEmptyList[(ConsumptionCategory, PLDCategoryParameter)]) extends PLDCategoryParameters

/**
  * N.B. †his could be simplified as the current structure simply uses the old PLD configuration
  * to lessen the likelihood of refactoring regressions and delivery slip before go live
  * as a result of merging the configuration with the new pipeline configuration
  * Refactoring will of course require a new test suite whereas here we are simply leaning on the
  * existing ones in PLD
  */
case class PldConfigurationBuilder(client: Config,
                                   all: Config) {
  val defaultWeights: Either[AdtError, DefaultWeightConfiguration] =
    Parser.parse[DefaultWeightConfigurationNelWrapUnsafe] (client).unwrap

  val seasonalAdjustmentConfig = SeasonalAdjustmentFromConfig(all)

  val redistributionCap = Some(RedistributionCap(Refrigeration, RedistributeFromCap(2000), AlwaysOn))

  val timeOfDayConfig = TimeOfDayLoadFromConfig(all)

  val categoryLimitConfig = CategoryLimitFromConfig(all)

  /**
    * @return NonEmptyList[(ConsumptionCategory, PLDCategoryParameter)]
    *
    *   where PLDCategoryParameter(initialWeight: PLDWeight,
                                   baseLoadContribution: Option[BaseLoadPercent],
                                   cap: Option[MaxCategoryAbsoluteCapRaw],
                                   floor: Option[MinTotalAbsoluteCapRaw],
                                   timeOfDayFactor: TimeOfDayWeightAdjustmentFn)
    */
  def generateParameters(runDay: RunDay,
                         location: Location,
                         profile: HouseholdProfile,
                         commodity: ProfileType): Either[AdtError, CategoryParameters] = {
    val pldLocation: PldLocation = PldLocation(PldPlace(location.place.name), location.north, location.east)

    val parametersMap: Either[AdtError, Map[ConsumptionCategory, PLDCategoryParameter]] = for {
      defaultWeights <- defaultWeights
      adjustedWeights <- seasonalAdjustmentConfig.adjustedWeightsViaOldStyle(defaultWeights, profile, location, runDay, commodity)
      categoryParameter <- (adjustedWeights map { case (k, v) => (k, makeCategoryParameter(runDay, commodity, k, v)) }).sequence
    } yield categoryParameter

    val parametersNel: Either[AdtError, NonEmptyList[(ConsumptionCategory, PLDCategoryParameter)]] = for {
      p <- parametersMap
      nel <- NonEmptyList.fromList(p.toList).toRight(AdtError(s"no pld parameters available for categories implied from profile $profile", None))
    } yield nel
    parametersNel.map(a => CategoryParameters(a))
  }

  private[pld] def makeCategoryParameter(runDay: RunDay,
                                         commodity: ProfileType,
                                         category: ConsumptionCategory,
                                         weight: PLDWeight): Either[AdtError, PLDCategoryParameter] = Try {
    for {
      timeOfDay <- timeOfDayConfig.timeOfDayAdjustment(runDay, category, commodity)
      baseLoad = categoryLimitConfig.baseLoad(commodity, category)
      minLimit = categoryLimitConfig.minLimit(commodity, category)
      maxLimit = categoryLimitConfig.maxLimit(commodity, category)
      p = PLDCategoryParameter(weight, baseLoad, maxLimit, minLimit, timeOfDay)
    } yield p
  }.fold(t => Left(AdtError("makeCategoryParameter failure: " + t.getMessage, Some(t))), identity)
}

object PldConfiguration {
  private val source: String = ConfigurationSource("pld").source
  val config: Config = ConfigFactory.parseResources(source)

  def stubBaseLoad = Some(BaseLoadPercent(0))
  def stubCap = Some(MaxCategoryAbsoluteCapRaw(500))
  def stubFloor = None
}
