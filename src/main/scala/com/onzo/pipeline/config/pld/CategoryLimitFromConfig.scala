package com.onzo.pipeline.config.pld

import com.onzo.common.ProfileType
import com.typesafe.config.Config
import com.onzo.pipeline.config.pld.CategoryLimitFromConfig._
import datatypes.{Error => AdtError}
import pld.data.ConsumptionCategory
import pld.data.input.CategoryCaps.{BaseLoadPercent, CategoryCapValue, MaxCategoryAbsoluteCapRaw, MinTotalAbsoluteCapRaw}
import pureconfig.{ConfigConvert, loadConfig}
import org.scalactic.TypeCheckedTripleEquals._

class CategoryLimitFromConfig private (config: Config)(implicit conv: ConfigConvert[CommodityCategoryLimit]) {

  val categoryLimitConfig: Either[AdtError, List[CategoryLimit]] =
    for {
      parsed <- loadConfig[CommodityCategoryLimit](config, configPath)
        .left.map { e => AdtError("ConfigReaderFailures: " + e.toList.mkString(" | "), None) }
      categoryLimit = parsed.commoditylimit
    } yield categoryLimit

  private def getConfig(profileType: ProfileType, category: ConsumptionCategory): Option[List[CategoryCapValue]] = {
    for {
      all <- categoryLimitConfig.toOption
      commodity <- all.find(_.profiletype === profileType)
      config <- commodity.contribution.find(_.category === category).map(_.limit)
    } yield config
  }

  def baseLoad(profileType: ProfileType, category: ConsumptionCategory): Option[BaseLoadPercent] =
    for {
      config <- getConfig(profileType, category)
      limit: List[BaseLoadPercent] = config.collect { case x: BaseLoadPercent => x }
      x <- limit.headOption
    } yield x

  def minLimit(profileType: ProfileType, category: ConsumptionCategory): Option[MinTotalAbsoluteCapRaw] =
    for {
      config <- getConfig(profileType, category)
      limit: List[MinTotalAbsoluteCapRaw] = config.collect { case x: MinTotalAbsoluteCapRaw => x }
      x <- limit.headOption
    } yield x

  def maxLimit(profileType: ProfileType, category: ConsumptionCategory): Option[MaxCategoryAbsoluteCapRaw] =
    for {
      config <- getConfig(profileType, category)
      limit: List[MaxCategoryAbsoluteCapRaw] = config.collect { case x: MaxCategoryAbsoluteCapRaw => x }
      x <- limit.headOption
    } yield x
}
object CategoryLimitFromConfig {
  val configPath: String = "pld.categoryLimits"

  case class Contribution(category: ConsumptionCategory, limit: List[CategoryCapValue])
  case class CategoryLimit(profiletype: ProfileType, contribution: List[Contribution])

  case class CommodityCategoryLimit(commoditylimit: List[CategoryLimit])
  def apply(config: Config)(implicit conv: ConfigConvert[CommodityCategoryLimit]): CategoryLimitFromConfig =
    new CategoryLimitFromConfig(config)
}