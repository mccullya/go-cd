package com.onzo.pipeline.config

object MapUtils {

  implicit class AlphabetIndex(val underlying: String) extends AnyVal {
    def toIndex: Int = underlying.toUpperCase.charAt(0).toInt - 'A'.toInt
  }

  def adjustMap[K,V](map: Map[K,V], key: K, maybeValue: Option[V]): Map[K,V] = {
    maybeValue match {
      case Some(value) => map.updated(key, value)
      case None => map
    }
  }

  implicit class RichMap[K,V](underlying: Map[K,V]) {
    def adjust(key: K, maybeValue: Option[V]): Map[K,V] = adjustMap(underlying, key, maybeValue)
  }

}
