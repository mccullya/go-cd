package com.onzo.pipeline.config

import com.onzo.collector.domain.ColumnMapping

/**
  * Created by toby.hobson on 27/02/2017.
  */
trait ClientColumnMapping[T] {
  def asBatchMapping: ColumnMapping[T]
}
