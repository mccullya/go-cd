package com.onzo.pipeline.config

import com.onzo.collector.domain.CsrRecord.{ElectricCsrRecord, GasCsrRecord}
import com.onzo.collector.domain.{ColumnMapping, CsrRecord, ElectricSensorReading, GasSensorReading}
import com.onzo.pipeline.config.MapUtils._

//@formatter:off
case class ElecConsumptionColumnMapping(
  sensorId: String,
  timestamp: String,
  cumulativeEnergy: String
) extends ClientColumnMapping[ElectricSensorReading] {
//@formatter:on

  def asBatchMapping: ColumnMapping[ElectricSensorReading] = {
    ColumnMapping(
      Map(
        ElectricSensorReading.Fields.SensorIdField -> sensorId.toIndex,
        ElectricSensorReading.Fields.TimestampField -> timestamp.toIndex,
        ElectricSensorReading.Fields.CumulativeEnergyField -> cumulativeEnergy.toIndex
      )
    )
  }
}

//@formatter:off
case class GasConsumptionColumnMapping(
  sensorId: String,
  timestamp: String,
  cumulativeVolume: String
) extends ClientColumnMapping[GasSensorReading] {
//@formatter:on

  def asBatchMapping: ColumnMapping[GasSensorReading] = {
    ColumnMapping(
      Map(
        GasSensorReading.Fields.SensorIdField -> sensorId.toIndex,
        GasSensorReading.Fields.TimestampField -> timestamp.toIndex,
        GasSensorReading.Fields.CumulativeVolumeField -> cumulativeVolume.toIndex
      )
    )
  }
}

//@formatter:off
case class ElectricCsrColumnMapping(
  householdId: String,
  sensorId: String,
  status: String,
  standingCharge: String,
  unitRate: String,
  tags: String,
  zipPostalCode: Option[String],
  latitude: Option[String],
  longitude: Option[String]
) extends ClientColumnMapping[ElectricCsrRecord] {
//@formatter:on

  override def asBatchMapping: ColumnMapping[ElectricCsrRecord] = {
    ColumnMapping(Map(
      CsrRecord.Fields.HouseholdIdField -> householdId.toIndex,
      CsrRecord.Fields.StatusField -> status.toIndex,
      CsrRecord.Fields.SensorIdField -> sensorId.toIndex,
      CsrRecord.Fields.DailyStandingChargeField -> standingCharge.toIndex,
      CsrRecord.Fields.CostPerUnitField -> unitRate.toIndex,
      CsrRecord.Fields.DailyStandingChargeField -> standingCharge.toIndex,
      CsrRecord.Fields.TagsField -> tags.toIndex)
      .adjust(CsrRecord.Fields.ZipPostalCodeField, zipPostalCode.map(_.toIndex))
      .adjust(CsrRecord.Fields.LatitudeField, latitude.map(_.toIndex))
      .adjust(CsrRecord.Fields.LongitudeField, longitude.map(_.toIndex))
    )
  }

}

//@formatter:off
case class GasCsrColumnMapping(
  householdId: String,
  sensorId: String,
  status: String,
  standingCharge: String,
  unitRate: String,
  calorificValue: String,
  tags: String,
  zipPostalCode: Option[String],
  latitude: Option[String],
  longitude: Option[String]
) extends ClientColumnMapping[GasCsrRecord] {
//@formatter:on

  override def asBatchMapping: ColumnMapping[GasCsrRecord] = {
    ColumnMapping(Map(
        CsrRecord.Fields.HouseholdIdField -> householdId.toIndex,
        CsrRecord.Fields.StatusField -> status.toIndex,
        CsrRecord.Fields.SensorIdField -> sensorId.toIndex,
        CsrRecord.Fields.DailyStandingChargeField -> standingCharge.toIndex,
        CsrRecord.Fields.CostPerUnitField -> unitRate.toIndex,
        CsrRecord.Fields.DailyStandingChargeField -> standingCharge.toIndex,
        CsrRecord.Fields.CalorificValueField -> calorificValue.toIndex,
        CsrRecord.Fields.TagsField -> tags.toIndex)
        .adjust(CsrRecord.Fields.ZipPostalCodeField, zipPostalCode.map(_.toIndex))
        .adjust(CsrRecord.Fields.LatitudeField, latitude.map(_.toIndex))
        .adjust(CsrRecord.Fields.LongitudeField, longitude.map(_.toIndex))
    )
  }
}