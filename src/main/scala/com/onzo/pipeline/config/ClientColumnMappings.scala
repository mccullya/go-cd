package com.onzo.pipeline.config

//@formatter:off
case class ClientColumnMappings(
  skipNumberOfRows: Int,
  elecConsumption: ElecConsumptionColumnMapping,
  gasConsumption: GasConsumptionColumnMapping,
  electricCsr: Option[ElectricCsrColumnMapping],
  gasCsr: Option[GasCsrColumnMapping]
)
//@formatter:off