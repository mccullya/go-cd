package com.onzo.pipeline.config

import java.time.format.DateTimeFormatter

import com.onzo.collector.domain.BatchCollectionClientConfig
import com.onzo.common.ConfigurationSource
import com.onzo.common.domain.ClientId
import com.onzo.pipeline.config.pld.PldConfigurationBuilder
import com.typesafe.config.{Config, ConfigFactory}

//@formatter:on
case class ClientConfig(
  id: Int,
  name: String,
  timestampPattern: String,
  columnMappings: ClientColumnMappings,
  pldConfiguration: PldConfigurationBuilder
) {
  def clientId: ClientId = ClientId(id)
}
//formatter:off

object ClientConfig {
  implicit def asBatchClientConfig(clientConfig: ClientConfig): BatchCollectionClientConfig = {
    BatchCollectionClientConfig(
      id = clientConfig.id.toString,
      electricConsumptionColumnMapping = clientConfig.columnMappings.elecConsumption.asBatchMapping,
      gasConsumptionColumnMapping = clientConfig.columnMappings.gasConsumption.asBatchMapping,
      csrElectricColumnMapping = clientConfig.columnMappings.electricCsr.map(_.asBatchMapping),
      csrGasColumnMapping = clientConfig.columnMappings.gasCsr.map(_.asBatchMapping),
      skipNumberOfRows = clientConfig.columnMappings.skipNumberOfRows,
      dateTimeFormatter = DateTimeFormatter.ofPattern(clientConfig.timestampPattern)
    )
  }
  val config: Config = ConfigFactory.parseResources(ConfigurationSource("client").source)
}
