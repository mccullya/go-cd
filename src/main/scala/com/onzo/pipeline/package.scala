package com.onzo

import com.onzo.common.domain.{ClientId, HouseholdId}
import com.onzo.etl.core.domain.AugmentedSensorReading
import com.onzo.etl.core.domain.AugmentedSensorReading.Resolution.{Minutes15, Minutes60}
import com.onzo.logging.{AppName, AtlasLogging, ShowClient}
import com.onzo.pipeline.Pipeline.ClientResult
import com.onzo.pipeline.config.ClientConfig
import com.onzo.pipeline.domain.HouseholdProfileEntity.GetProfileIdFn
import com.onzo.pipeline.domain._
import com.onzo.pipeline.stubbed.{DailyConsumption, Interval}
import jp.ne.opt.chronoscala.Imports._
import util.scalalocal.RichMap._

package object pipeline extends AtlasLogging {

  val appName = AppName("pipeline")

  implicit val clientConfigShowClient = new ShowClient[ClientConfig] {
    override def show(a: ClientConfig): String = a.name
  }

  implicit val clientResultShowClient = new ShowClient[ClientResult] {
    override def show(a: ClientResult): String = a.name
  }

  val logError: (String) => Unit = logger.error(appName)
  val logErrorT: (String, Throwable) => Unit = logger.errorT(appName)
  val logInfo: (=> String) => Unit = logger.info(appName)
  val logDebug: (=> String) => Unit = logger.debug(appName)

  def logClientError[T](client: T)(f: => String)(implicit ev: ShowClient[T]): Unit = logger.clientError(appName)(client)(f)

  def logClientInfo[T](client: T)(f: => String)(implicit ev: ShowClient[T]): Unit = logger.clientInfo(appName)(client)(f)

  def logClientDebug[T](client: T)(f: => String)(implicit ev: ShowClient[T]): Unit = logger.clientDebug(appName)(client)(f)

  implicit def windowedIntervalToDaily(input: Map[ZonedDateTime, ElectricConsumption]): Seq[Interval] = {
    input.keySorted.map(i =>
      Interval(i._1, i._2.energy.value)
    ).toSeq
  }

  implicit def windowedConsumptionToDaily(windowedElectricConsumption: WindowedElectricConsumption)
                                         (implicit findProfileById: (ClientId, HouseholdId) => HouseholdProfileEntity): DailyConsumption = {
    DailyConsumption(
      windowedElectricConsumption.day,
      windowedElectricConsumption.clientId,
      windowedElectricConsumption.onzoDeviceId,
      findProfileById(windowedElectricConsumption.clientId, windowedElectricConsumption.onzoHouseholdId),
      windowedElectricConsumption.cost,
      intervals = windowedElectricConsumption.intervals
    )

  }

  implicit class RichWindowedConsumption(val underlying: WindowedElectricConsumption) extends AnyVal {
    def asDailyConsumption(implicit findProfileById: GetProfileIdFn): DailyConsumption = underlying
  }

  implicit def toPipelineResolution(etlResolution: AugmentedSensorReading.Resolution): Resolution = {
    etlResolution match {
      case Minutes15 => Resolution.Min15
      case Minutes60 => Resolution.Min60
    }
  }
}