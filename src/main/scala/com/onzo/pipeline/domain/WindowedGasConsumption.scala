package com.onzo.pipeline.domain

import java.time.{LocalDate, LocalDateTime, ZoneId}

import squants.energy.{Energy, Power}
import squants.market.Money
import squants.{Volume, VolumeFlow}
/*
//@formatter:off
case class WindowedGasConsumption(
  clientId: ClientId,
  onzoHouseholdId: HouseholdId,
  onzoDeviceId: OnzoDeviceId,
  day: LocalDate,
  resolution: Resolution,
  timezone: ZoneId,
  energy: Energy,
  volume: Volume,
  flowRate: VolumeFlow,
  averagePower: Power,
  cost: Money,
  intervals: Map[LocalDateTime, GasConsumption])
//@formatter:on
*/