package com.onzo.pipeline.domain

import squants.energy.{Energy, Power}
import squants.market.Money
import squants.{Volume, VolumeFlow}

case class ElectricConsumption(energy: Energy, power: Power, cost: Money)

case class GasConsumption(energy: Energy, power: Power, volume: Volume, flowRate: VolumeFlow, cost: Money)

sealed trait Resolution {
  def asSeconds: Int
}

object Resolution {

  case object Min15 extends Resolution {
    override def asSeconds: Int = 15 * 60
  }

  case object Min60 extends Resolution {
    override def asSeconds: Int = 60 * 60
  }

}

sealed trait Breakdown {
  def cost: Money
}

case class ElectricEnergyBreakdown(energy: Energy, cost: Money) extends Breakdown

case class GasEnergyBreakdown(energy: Energy, cost: Money) extends Breakdown

case class GasVolumeBreakdown(volume: Volume, cost: Money) extends Breakdown