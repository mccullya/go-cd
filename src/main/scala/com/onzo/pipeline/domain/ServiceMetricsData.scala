package com.onzo.pipeline.domain

import java.time.Instant

import com.onzo.common.domain.{ClientId, HouseholdId, SensorId}

sealed trait ServiceMetricsData

case class BreakdownOutputWasProduced(
  clientId: ClientId,
  householdId: HouseholdId,
  sensorId: SensorId,
  when: Instant = Instant.now
) extends ServiceMetricsData

case class ConsumptionOutputWasProduced(
  clientId: ClientId,
  householdId: HouseholdId,
  sensorId: SensorId,
  when: Instant = Instant.now
) extends ServiceMetricsData
