package com.onzo.pipeline.domain

import com.onzo.pipeline.stubbed.DailyConsumption

/* Error */
sealed trait PipelineError {
  val message: String
}

case class BatchCollectorError(message: String) extends PipelineError {
  override def toString: String = s"BatchCollectorError(message=$message)"
}

case class CsrRecordWarning(message: String) extends PipelineError {
  override def toString: String = s"CsrRecordWarning(message=$message)"
}

case class GenericError(message: String) extends PipelineError

case class ETLError(message: String) extends PipelineError {
  override def toString: String = s"ETLError(message=$message)"
}

case class PLDError(dailyConsumption: DailyConsumption, error: String) extends PipelineError {
  override val message: String = s"Unable to disaggregate $dailyConsumption: $error"
}

case class SaveError(message: String) extends PipelineError {
  override def toString: String = s"SaveError(message=$message)"
}

/* End Error */