package com.onzo.pipeline.domain

import java.time.ZoneId

import com.onzo.common.domain.{ClientId, HouseholdId, OnzoDeviceId}
import com.onzo.pipeline.stubbed.DailyConsumption
import jp.ne.opt.chronoscala.NamespacedImports._
import squants.energy.{Energy, Power}
import squants.market.Money

sealed trait WindowedConsumption {
  def asDailyConsumption(implicit findProfileById: (ClientId, HouseholdId) => HouseholdProfileEntity): DailyConsumption
}

case class WindowedElectricConsumption(clientId: ClientId,
                                       onzoHouseholdId: HouseholdId,
                                       onzoDeviceId: OnzoDeviceId,
                                       day: RunDay,
                                       resolution: Resolution,
                                       timezone: ZoneId,
                                       energy: Energy,
                                       averagePower: Power,
                                       cost: Money,
                                       intervals: Map[ZonedDateTime, ElectricConsumption]) extends WindowedConsumption{
  def asDailyConsumption(implicit findProfileById: (ClientId, HouseholdId) => HouseholdProfileEntity): DailyConsumption = this
}

import squants.{Volume, VolumeFlow}

case class WindowedGasConsumption(clientId: ClientId,
                                  onzoHouseholdId: HouseholdId,
                                  onzoDeviceId: OnzoDeviceId,
                                  day: RunDay,
                                  resolution: Resolution,
                                  timezone: ZoneId,
                                  energy: Energy,
                                  volume: Volume,
                                  flowRate: VolumeFlow,
                                  averagePower: Power,
                                  cost: Money,
                                  intervals: Map[ZonedDateTime, GasConsumption])
