package com.onzo.pipeline.domain

import jp.ne.opt.chronoscala.NamespacedImports._

/**
  * Simply to add some type safety to the run day concept inherited from the Classic Platform
  * Abstracting the run day as a client period view rather then being hard baked
  * may arguably be preferable
  * @param underlying
  */
@deprecated("the concept of a run period is arguably better viewed from a client perspective", "tbc")
case class RunDay(underlying: LocalDate) extends AnyVal
