package com.onzo.pipeline.domain

import com.onzo.common.domain.{ClientId, HouseholdId, SensorId}
import jp.ne.opt.chronoscala.NamespacedImports._
import pld.data.ConsumptionCategory

// TODO ATLAS-310 remove
case class DailyElectricBreakdown(clientId: ClientId,
                                  onzoHouseholdId: HouseholdId,
                                  sensorId: SensorId,  // required for service metrics; not currently persisted to the electric breakdown table
                                  day: RunDay,
                                  breakdowns: Map[ConsumptionCategory, ElectricEnergyBreakdown])

case class DailyGasBreakdown(clientId: ClientId,
                             onzoHouseholdId: HouseholdId,
                             sensorId: SensorId,  // required for service metrics; not currently persisted to the gas breakdown table
                             day: RunDay,
                             breakdowns: Map[ConsumptionCategory, Breakdown])


/*case class DailyBreakdown[T <: Breakdown](clientId: ClientId,
                          onzoHouseholdId: HouseholdId,
                          day: ZonedDateTime,
                          breakdowns: Map[ConsumptionCategory, T])
*/