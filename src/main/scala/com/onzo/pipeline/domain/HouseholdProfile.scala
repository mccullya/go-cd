package com.onzo.pipeline.domain

import com.onzo.common.domain.{ClientId, HouseholdId}
import com.onzo.pipeline.SnakifiedSprayJsonSupport
import spray.json.RootJsonFormat

case class HouseholdProfile(
  houseType: Option[String],
  numberOfPeople: Option[Int],
  peoplePlus: Option[Boolean],
  numberOfBedrooms: Option[Int],
  bedroomsPlus: Option[Boolean],
  // Electric
  electricHeating: Option[Boolean],
  topUpElectricHeating: Option[Boolean],
  electricHotWater: Option[Boolean],
  electricCooking: Option[Boolean],
  topUpElectricCooking: Option[Boolean],
  // Gas
  gasHeating: Option[Boolean],
  gasCooking: Option[Boolean],
  gasHotWater: Option[Boolean],
  // Cooling
  compressiveCooling: Option[Boolean],
  fanCooling: Option[Boolean])

object HouseholdProfile {
  def empty: HouseholdProfile = HouseholdProfile(
    None, None, None, None, None, None, None, None, None, None, None, None, None, None, None
  )
}

trait HouseholdProfileJsonProtocol extends SnakifiedSprayJsonSupport {
  implicit val pldFormat: RootJsonFormat[HouseholdProfile] = jsonFormat15(HouseholdProfile.apply)
}

case class HouseholdProfileEntity(clientId: ClientId, onzoHouseholdId: HouseholdId, householdProfile: HouseholdProfile)
object HouseholdProfileEntity {
  type GetProfileIdFn = (ClientId, HouseholdId) => HouseholdProfileEntity
}
