package com.onzo.pipeline

import java.util.Objects

import akka.actor.{Actor, Props}
import com.onzo.pipeline.EtlStatusActor.{LogEtlElectricOutput, LogEtlGasOutput, Reset, SensorHash}
import com.onzo.pipeline.domain.{WindowedElectricConsumption, WindowedGasConsumption}

class EtlStatusActor extends Actor {

  import scala.language.postfixOps

  override def receive: Receive = process(Set.empty[SensorHash])

  def process(sensorDays: Set[SensorHash]): Receive = {
    case LogEtlElectricOutput(consumption) =>
      val hash = hashSensorDay(consumption)
      if (sensorDays.contains(hash)) sender() ! 0 else sender() ! 1
      context.become(process(sensorDays + hash))
    case LogEtlGasOutput(consumption) =>
      val hash = hashSensorDay(consumption)
      if (sensorDays.contains(hash)) sender() ! 0 else sender() ! 1
      context.become(process(sensorDays + hash))
    case Reset =>
      logInfo("Resetting ETLStatusActor state")
      context.become(process(Set.empty))
      sender() ! ()
    case m => logError(s"Unexpected message ${m.toString}")
  }

  private def hashSensorDay(consumption: WindowedElectricConsumption): SensorHash = {
    val clientId = consumption.clientId.value.toString
    val householdId = consumption.onzoHouseholdId.value
    val deviceId = consumption.onzoDeviceId.value.toString
    val day = consumption.day.underlying.toEpochDay.toString
    val hash = Objects.hash(clientId, householdId, deviceId, day)
    SensorHash(hash)
  }

  private def hashSensorDay(consumption: WindowedGasConsumption): SensorHash = {
    val clientId = consumption.clientId.value.toString
    val householdId = consumption.onzoHouseholdId.value
    val deviceId = consumption.onzoDeviceId.value
    val day = consumption.day.underlying.toEpochDay.toString
    val hash = Objects.hash(clientId, householdId, deviceId, day)
    SensorHash(hash)
  }
}

object EtlStatusActor {

  private case class SensorHash(value: Int) extends AnyVal

  case class LogEtlElectricOutput(consumption: WindowedElectricConsumption)

  case class LogEtlGasOutput(consumption: WindowedGasConsumption)

  case object Reset

  def props: Props = Props[EtlStatusActor]
}
