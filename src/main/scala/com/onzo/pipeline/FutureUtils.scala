package com.onzo.pipeline

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration.FiniteDuration

object FutureUtils {

  import akka.actor.Scheduler
  import akka.pattern.after

  def retry[T](f: => Future[T], delays: Seq[FiniteDuration])
              (implicit ec: ExecutionContext, s: Scheduler): Future[T] =
    f recoverWith {
      case _ if delays.nonEmpty => after(delays.head, s)(retry(f, delays.tail))
    }
}
