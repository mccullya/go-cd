#!/bin/bash
cd "$(dirname "$0")"
echo "Starting the pipeline service"

if [ -z ${XMX+x} ]
then
	export XMSOPT=""
else 
	export XMSOPT="-Xms$XMS"
fi

if [ -z ${XMX+x} ]
then
	export XMXOPT=""
else 
	export XMXOPT="-Xmx$XMX"
fi

echo "settings: xms: $XMS xmx: $XMX cassandra host: $CASSANDRA_HOST s3 bucket: $S3BUCKET jmx_enabled: $JMX_ENABLED"

if [ -z ${JMX_ENABLED+x} ] 
then 
  echo "jmx not set";
  export JMX_OPTS="" 
else 
  echo "jmx is set"
  export JMX_OPTS="-javaagent:./jmx_prometheus_javaagent-0.9.jar=7171:config.yaml -Dcom.sun.management.jmxremote.port=1616 -Dcom.sun.management.jmxremote.rmi.port=1616 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.local.only=false -Djava.rmi.server.hostname=localhost"
fi

if [ -e "/config/logback.xml" ]
then
  export LOGBACK="-Dlogback.configurationFile=/config/logback.xml"
fi

export JAVA_OPTS=" $XMXOPT $XMSOPT $LOGBACK $JMX_OPTS "
echo "java $JAVA_OPTS -jar /app/atlas-pipeline-fat.jar"

java $JAVA_OPTS -jar /app/atlas-pipeline-fat.jar




