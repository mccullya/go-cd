FROM java
RUN apt-get update 

EXPOSE 8080
EXPOSE 1616
EXPOSE 7171

ARG build_version=1.0.0-SNAPSHOT
ARG build_name=atlas-pipeline
ENV build_version=$build_version
RUN mkdir /app && \
    echo $build_name-$build_version > /version.txt
ADD scripts/run.sh /app/run.sh
ADD scripts/entrypoint.sh /app/entrypoint.sh
ADD target/scala-2.12/atlas-pipeline-fat-$build_version.jar /app/atlas-pipeline-fat.jar
RUN curl -o /app/jmx_prometheus_javaagent-0.9.jar https://repo1.maven.org/maven2/io/prometheus/jmx/jmx_prometheus_javaagent/0.9/jmx_prometheus_javaagent-0.9.jar 
COPY scripts/jmxconfig.yaml /app/config.yaml
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
WORKDIR /app
ENTRYPOINT  ["bash", "/app/run.sh"]
