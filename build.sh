#!/bin/bash
if [ -z "$1" ]
  then
    rake default
	exit $?
fi

rake $1
exit $?