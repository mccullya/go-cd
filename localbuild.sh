#!/bin/bash
cd "$(dirname "$0")"
echo "Running localbuild of sbt inside container"


cat >~/.sbt/repositories <<EOL
[repositories]
local
my-ivy-proxy-releases: https://artifactory.dev.onzo.cloud/artifactory/virsbt/, [organization]/[module]/(scala_[scalaVersion]/)(sbt_[sbtVersion]/)[revision]/[type]s/[artifact](-[classifier]).[ext]
my-maven-proxy-releases: https://artifactory.dev.onzo.cloud/artifactory/virsbt/
EOL

#sbt clean test it:test 
sbt clean test 


echo "clean up any files owned by root"
chown -R $USERID:$GROUPID ~/.sbt 
chown -R $USERID:$GROUPID ~/.ivy2
chown -R $USERID:$GROUPID /code/target 

echo "exit code: $exit_code"
exit $exit_code