package com.onzo.common

import squants.Angle
import squants.space._

case class Place(name: String) extends AnyVal
case class Location(place: Place, north: Angle, east: Angle)
