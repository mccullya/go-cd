package com.onzo.common

import cats.data.NonEmptyList
import cats._

sealed abstract class ProfileComponent extends Ordered[ProfileComponent] {
  def compare(that: ProfileComponent): Int = this.toString compareTo that.toString
}
object ProfileComponent {
  // Electric
  case object ElectricCooking extends ProfileComponent
  case object TopUpElectricCooking extends ProfileComponent
  case object ElectricHeating extends ProfileComponent
  case object TopUpElectricHeating extends ProfileComponent
  case object ElectricHotWater extends ProfileComponent

  // Gas
  case object GasCooking extends ProfileComponent
  case object GasHeating extends ProfileComponent
  case object GasHotWater extends ProfileComponent

  // Cooling
  case object CompressiveCooling extends ProfileComponent
  case object FanCooling extends ProfileComponent

  implicit val order = new Order[ProfileComponent] {
    def compare(x: ProfileComponent, y: ProfileComponent): Int = x.toString compareTo y.toString
  }

  val all: Set[ProfileComponent] = Set(ElectricCooking, TopUpElectricCooking, ElectricHeating, TopUpElectricHeating, ElectricHotWater,
    GasCooking, GasHeating, GasHotWater,
    CompressiveCooling, FanCooling)
}

// https://github.com/typelevel/cats/issues/123
class NonEmptySet[T] private (private val underlying: Set[T]) {
  // for now just use this
  def value: Set[T] = underlying

  override def equals(obj: scala.Any): Boolean = obj match {
    case nes: NonEmptySet[T] => value.equals(nes.value)
    case _ => false
  }
}

object NonEmptySet {
  def apply[T](set: Set[T]): Option[NonEmptySet[T]] = {
    if (set.isEmpty) {
      None
    } else {
      Some(new NonEmptySet(set))
    }
  }
  def apply[T](elem: T) = new NonEmptySet[T](Set(elem))
  def apply[T](nel: NonEmptyList[T])(implicit order: Order[T]): NonEmptySet[T] =
    new NonEmptySet(nel.distinct.toList.toSet)
}

case class ProfileType(commodity: Commodity) extends AnyVal

case class FilteredHouseholdProfile(underlying: Set[ProfileComponent])

case class HouseholdProfile(underlying: Set[ProfileComponent]) {
  def filter(allow: Set[ProfileComponent]) = FilteredHouseholdProfile(underlying intersect allow)
}