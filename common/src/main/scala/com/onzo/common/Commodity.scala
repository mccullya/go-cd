package com.onzo.common

sealed trait Commodity

object Commodity {
  case object Electric extends Commodity
  case object Gas extends Commodity
}
