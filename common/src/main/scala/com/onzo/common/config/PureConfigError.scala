package com.onzo.common.config

import datatypes.{Error => AdtError}
import pureconfig.error.ConfigReaderFailures

/**
  * This can be helpful as although we want to halt on a config error at startup
  * there are times when it may be useful to add some context for prompt remedy
  */
object PureConfigError {
  implicit class ConfigReaderFailuresOps[A](val configReaderFailures: ConfigReaderFailures) extends AnyVal {

    def toError(context: String): AdtError = {
      val errors: List[String] = configReaderFailures.toList map { a =>
        val location = a.location.map(_.description).getOrElse("unspecified config reader error location")
        s"${a.toString} at $location"
      }
      AdtError((context :: errors).mkString(" : "), None)
    }
  }
}
