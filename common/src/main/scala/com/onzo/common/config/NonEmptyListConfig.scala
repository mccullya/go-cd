package com.onzo.common.config

import cats.data.NonEmptyList
import com.typesafe.config.{Config, ConfigFactory}
import pureconfig.error.ConfigReaderFailures
import pureconfig._
import pureconfig.ConfigConvert
import pureconfig.syntax._

import scala.reflect.ClassTag
import scala.util.Try

object NonEmptyListConfig {

  def parse: String => Option[NonEmptyList[Int]] = (s: String) => {
    val conf: Config = ConfigFactory.parseString(s)
    case class Conf(data: List[Int])
    val parsed = conf.to[Conf]
    val builder: Option[List[Int]] = parsed.toOption.map(_.data)
    builder flatMap { a => NonEmptyList.fromList(a) }
  }

  implicit def nonEmptyListConfigConvert: ConfigConvert[NonEmptyList[Int]] =
    ConfigConvert.viaNonEmptyStringOpt[NonEmptyList[Int]](NonEmptyListConfig.parse, _.toString)
}
