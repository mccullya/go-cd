package com.onzo.common

case class Percent(percent: Double) {
  require(!(percent < 0), s"percentage value cannot be less than zero but is $percent")
  require(!(percent > 100), s"percentage value cannot be more than 100 but is $percent")
  val normal: Double = percent / 100
  val oneMinus: Double = 1 - normal
}