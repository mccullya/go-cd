package com.onzo.common

import scala.util.Try

case class ConfigurationSource(name: String) {
  private val fullName = s"config.$name"
  private val fromProp: Try[String] = Try { System.getProperty(fullName) }
  private val fromEnv: Try[String] = Try { System.getenv(fullName).toUpperCase }
  val source: String = {
    val attempt = fromEnv.recoverWith({ case _ => fromProp }).getOrElse(name)
    val prefix = if (attempt == null) name
    else attempt
    prefix + ".conf"
  }
}
