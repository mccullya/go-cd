package pureconfiglocal.java8time

import java.time._
import java.time.format.DateTimeFormatter

import pureconfig.ConfigConvert
import pureconfig.ConfigConvert._

/**
  * based on module-joda
  *
  * Provides methods that create [[ConfigConvert]] instances from a set of parameters used to configure the instances.
  *
  * The result of calling one of the methods can be assigned to an `implicit val` so that `pureconfig` will be able to
  * use it:
  * {{{
  *   implicit val localDateConfigConvert = makeLocalDateConfigConvert(ISO_DATE_TIME)
  * }}}
  */
package object configurable {
  def dateTimeConfigConvert(formatter: DateTimeFormatter): ConfigConvert[ZonedDateTime] =
    viaNonEmptyString[ZonedDateTime](
      catchReadError(ZonedDateTime.parse(_, formatter)), (a: ZonedDateTime) => a.format(formatter))

  def localDateConfigConvert(formatter: DateTimeFormatter): ConfigConvert[LocalDate] =
    viaNonEmptyString[LocalDate](
      catchReadError(LocalDate.parse(_, formatter)), (a: LocalDate) => a.format(formatter))

  def localTimeConfigConvert(formatter: DateTimeFormatter): ConfigConvert[LocalTime] =
    viaNonEmptyString[LocalTime](
      catchReadError(LocalTime.parse(_, formatter)), (a: LocalTime) => a.format(formatter))

  def localDateTimeConfigConvert(formatter: DateTimeFormatter): ConfigConvert[LocalDateTime] =
    viaNonEmptyString[LocalDateTime](
      catchReadError(LocalDateTime.parse(_, formatter)), (a: LocalDateTime) => a.format(formatter))

  def monthDayConfigConvert(formatter: DateTimeFormatter): ConfigConvert[MonthDay] =
    viaNonEmptyString[MonthDay](
      catchReadError(MonthDay.parse(_, formatter)), (a: MonthDay) => a.format(formatter))

  def yearMonthConfigConvert(formatter: DateTimeFormatter): ConfigConvert[YearMonth] =
    viaNonEmptyString[YearMonth](
      catchReadError(YearMonth.parse(_, formatter)), (a: YearMonth) => a.format(formatter))
}
