package com.onzo.common.testdata


import jp.ne.opt.chronoscala.Imports._
import org.scalacheck.Gen

import scala.math.{pow, round}

class MeterTestData {

  def roundTo(x: Double, d: Int = 3): Double = round(x*pow(10, d))/pow(10, d)

  case class Reading(meterNo: Int, timeStamp: ZonedDateTime, reading: Double) {
    override def toString: String = s"$meterNo,$timeStamp,${roundTo(reading)}"
  }

  type InputData = List[String]

  val data30MinsOf1Min: InputData =
    """999,2016-01-22 23:30:00,8.600000000
      |999,2016-01-22 23:31:00,8.600000000
      |999,2016-01-22 23:32:00,8.600000000
      |999,2016-01-22 23:33:00,8.600000000
      |999,2016-01-22 23:34:00,8.600000000
      |999,2016-01-22 23:35:00,8.600000000
      |999,2016-01-22 23:36:00,8.600000000
      |999,2016-01-22 23:37:00,8.600000000
      |999,2016-01-22 23:38:00,8.600000000
      |999,2016-01-22 23:39:00,8.600000000
      |999,2016-01-22 23:40:00,8.600000000
      |999,2016-01-22 23:41:00,8.600000000
      |999,2016-01-22 23:42:00,8.600000000
      |999,2016-01-22 23:43:00,8.600000000
      |999,2016-01-22 23:44:00,8.600000000
      |999,2016-01-22 23:45:00,9.733333333
      |999,2016-01-22 23:46:00,9.733333333
      |999,2016-01-22 23:47:00,9.733333333
      |999,2016-01-22 23:48:00,9.733333333
      |999,2016-01-22 23:49:00,9.733333333
      |999,2016-01-22 23:50:00,9.733333333
      |999,2016-01-22 23:51:00,9.733333333
      |999,2016-01-22 23:52:00,9.733333333
      |999,2016-01-22 23:53:00,9.733333333
      |999,2016-01-22 23:54:00,9.733333333
      |999,2016-01-22 23:55:00,9.733333333
      |999,2016-01-22 23:56:00,9.733333333
      |999,2016-01-22 23:57:00,9.733333333
      |999,2016-01-22 23:58:00,9.733333333
      |999,2016-01-22 23:59:00,9.733333333""".stripMargin.split("\n").toList

  def minuteStream(from: ZonedDateTime = ZonedDateTime.now().withSecond(0).withNano(0) - 1.day): Stream[ZonedDateTime] = {
    def next(s: Stream[ZonedDateTime], a: ZonedDateTime): Stream[ZonedDateTime] = a #:: next(s, a + 1.minute)
    next(Stream.empty, from)
  }
  lazy val minute: Stream[ZonedDateTime] = minuteStream()

  def energyReadingStream(from: Double = 10): Stream[Double] = {
    def vary(a: Double) = {
      val b =
        if (a > 5) a/100 * Gen.choose(1, 50).sample.get
        else Gen.choose(0, 20).sample.get
      def up = if (a + b < 100) a + b else a - b
      def down = if (a - b > 0) a - b else a + b
      if (Gen.choose(0, 1).sample.get == 0) up else down
    }
    def next(s: Stream[Double], a: Double): Stream[Double] = a #:: next(s, vary(a))
    next(Stream.empty, from)
  }
  lazy val energyReading: Stream[Double] = energyReadingStream()

  def data1DayOf1MinReading: Int => MeterDay = { meterNo =>
    val minutesInDay = (60*24)-1
    val meterMins: List[(ZonedDateTime, Double)] = minute.take(minutesInDay).toList.zip(energyReading.take(minutesInDay))
    val readings = meterMins map { case ((a: ZonedDateTime, b: Double)) => Reading(meterNo, a, b) }
    readings
  }

  def data1DayOfNMeters: Int => List[Reading] = n => Range(1, n).toList.flatMap(data1DayOf1MinReading(_))

  lazy val data1Dayof1Min1000Meters = data1DayOfNMeters(1000) map (_.toString)
  lazy val data1Dayof1Min40000Meters = data1DayOfNMeters(40000) map (_.toString)

  type MeterDay = List[Reading]

  def singleMeterStream(startMeterNo: Int): Stream[MeterDay] = {
    def next(s: Stream[MeterDay], a: MeterDay): Stream[MeterDay] = a #:: next(s, data1DayOf1MinReading(a.head.meterNo + 1))
    next(Stream.empty, data1DayOf1MinReading(startMeterNo))
  }

  def nSingleMeters(n : Int): Stream[MeterDay] = singleMeterStream(1).take(n)

  val dataDailyCurrentMonthSoFarReading: InputData =
    """9999,2016-01-12 00:00:00,5011.0
      |9999,2016-01-13 00:00:00,4757.0
      |9999,2016-01-14 00:00:00,5000.0
      |9999,2016-01-15 00:00:00,3200.0
      |9999,2016-01-16 00:00:00,5325.0
      |9999,2016-01-17 00:00:00,5010.0
      |9999,2016-01-18 00:00:00,5007.0
      |9999,2016-01-19 00:00:00,4975.0
      |9999,2016-01-20 00:00:00,5002.0
      |9999,2016-01-21 00:00:00,5011.0""".stripMargin.split("\n").toList

  /**
    * this represents comm_1min_1day in Driver
    * aws s3 ls s3://onzo-data-greenchoice/test-green-p4-pce/dwh/energy/intraday/20160122/1min/
    * 2016-11-15 00:16:01      51855 998.csv
    * 2016-11-15 00:16:00      51885 999.csv
    * which contain 1 min readings for the day:
    * as per data30MinsOf1Min above
    * This is always 1min data
    * This is found under dwh/commodity/intraday/ on s3
    */
  lazy val data1DayOf1Min: InputData = data1DayOf1MinReading(1234) map (_.toString)

  /**
    * this represents comm_daily_currentmonthsofar in Driver
    * aws s3 ls s3://onzo-data-greenchoice/test-green-p4-pce/dwh/energy/daily/single/9999/#
    * 2016-11-15 00:49:15         31 20160112.csv
    * 2016-11-18 11:37:26         31 20160113.csv
    * which contain 1 reading per day:
    * 9999,2016-01-12 00:00:00,5011.0
    * This is always daily data
    * This is found under dwh/commodity/daily/single/ on s3
    */
  lazy val dataDailyCurrentMonthSoFar: InputData = dataDailyCurrentMonthSoFarReading map (_.toString)

  /**
    * This represents comm_daily_1day in Driver
    * This is always daily data
    * This is found under dwh/commodity/daily/single/ on s3
    */
  lazy val dataDaily1Day: InputData = dataDailyCurrentMonthSoFarReading.last.toList map (_.toString)

  /**
    * this represents comm_daily_7days in Driver
    * this is either daily or raw depending whether e.g. energy or power
    * This is found os s3 under either:
    *   dwh/commodity/raw for for raw data
    *   dwh/commodity/daily/single for daily data
    */
  lazy val dataDaily7Days: InputData = dataDailyCurrentMonthSoFarReading.takeRight(7) map (_.toString)

  //
  /**
    * this represents comm_daily_28days in Driver
    * this is either daily or raw  depending whether e.g. energy or power
    * This is found os s3 under either:
    *   dwh/commodity/raw for for raw data
    *   dwh/commodity/daily/single for daily data
    */
  lazy val dataDaily28Days: InputData = dataDailyCurrentMonthSoFarReading.takeRight(28) map (_.toString)

  val commodity = List(     // agg1 agg2
    "energy",               // 1min daily
    "energy_cost",          // 1min daily
    "power",                // raw  raw
    "power_cost",           // raw  raw
    "gas",                  // 1min daily
    "gas_cost",             // 1min daily
    "gas_flow_rate",        // raw  raw
    "gas_flow_rate_cost")   // raw  raw
}
