package com.onzo.common.config

import cats.data.NonEmptyList
import com.typesafe.config.{Config, ConfigFactory}
import com.onzo.common.config.NonEmptyListConfig._
import org.scalactic.TypeCheckedTripleEquals
import org.scalatest.{Matchers, WordSpec}
import pureconfig._
import pureconfig.error.ConfigReaderFailures
import pureconfig.syntax._


class NonEmptyListConfigSpec extends WordSpec with Matchers with TypeCheckedTripleEquals {

  "a ConfigConvert[List[Int]" when {
    "used to parse a valid String of List[Int]" should {
      "construct an object containing a List[Int]" in {
        val data = List(1, 2, 3)
        val toParse = s""" { data: [1, 2, 3] } """
        val conf: Config = ConfigFactory.parseString(toParse)
        case class Conf(data: List[Int])
        conf.to[Conf] should === (Right(Conf(data)))
      }
    }
  }

  "a NonEmptyListConfig" when {
    "parsing a valid string representation of a NonEmptyList" should {
      "produce a NonEmptyList" in {
        val toParse = s""" { data: [1, 2, 3] } """
        val r = NonEmptyListConfig.parse(toParse)
        r should === (Some(NonEmptyList(1, List(2,3))))
      }
    }
  }

  // N.B. temporary workaround until we have a ConfigReader[NonEmptyList[_]]
  case class Nel(data: NonEmptyList[Int])
  case class NelWrap(data: List[Int]) {
    def unwrap: Nel = Nel(NonEmptyList.fromListUnsafe(data))
  }
  /* "a ConfigConvert[NonEmptyList[Int]" when {
    "used to parse a valid String of NonEmptyList[Int]" should {
      "construct a NonEmptyList[Int]" in {
        val data = NonEmptyList.fromListUnsafe(List(1, 2, 3))
        val toParse = s""" { data: [1, 2, 3] } """
        val conf: Config = ConfigFactory.parseString(toParse)
        case class Conf(data: NonEmptyList[Int])
        val r = conf.to[Conf]
        r should === (Right(Conf(data)))
      }
    }
  } */
  "a configuration containing a List" when {
    "parsed via a NelWrap" should {
      "construct a NonEmptyList" in {
        val data = NonEmptyList.fromListUnsafe(List(1, 2, 3))
        val toParse = s""" { data: [1, 2, 3] } """
        val conf: Config = ConfigFactory.parseString(toParse)
        val r = conf.to[NelWrap].map(_.unwrap)
        r should === (Right(Nel(data)))
      }
    }
  }
}
