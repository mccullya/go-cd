package com.onzo.common.config

import com.onzo.common.{HouseholdProfile, ProfileComponent}
import com.onzo.common.ProfileComponent.{ElectricHeating, TopUpElectricCooking}
import org.scalactic.TypeCheckedTripleEquals
import org.scalatest.{Matchers, WordSpec}

class HouseholdProfileSpec extends WordSpec with Matchers with TypeCheckedTripleEquals {

  "a household profile" when {
    "compared with another profile described by the same components" should {
      "be equal" in {
        val s: Set[ProfileComponent] = Set(ElectricHeating, TopUpElectricCooking)
        HouseholdProfile(s) should === (HouseholdProfile(s))
      }
    }
    "compared with another profile described with different components" should {
      "not be equal" in {
        val s0: Set[ProfileComponent] = Set(ElectricHeating, TopUpElectricCooking)
        val s1: Set[ProfileComponent] = Set(ElectricHeating)
        HouseholdProfile(s0) should not equal HouseholdProfile(s1)
      }
    }
  }
}
