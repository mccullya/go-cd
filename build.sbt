lazy val AkkaVersion = "2.4.17"
lazy val AkkaHttpVersion = "10.0.5"
lazy val BatchCollectorVersion = "1.0.130"
lazy val BreezeVersion = "0.13"
lazy val CassandraVersion = "3.1.2"
lazy val ChronoscalaVersion = "0.1.2"
lazy val EtlVersion = "1.0.73"
lazy val ScalaTestVersion = "3.0.1"
lazy val PldVersion = "1.0.73"
lazy val PureConfigVersion = "0.7.0"

lazy val commonSettings = Seq(
  organization := "com.onzo",
  scalaVersion := "2.12.1"
)

lazy val commonLibraries = Seq(
  libraryDependencies ++= Seq(
    "com.github.pureconfig" %% "pureconfig" % PureConfigVersion,
    "com.github.pureconfig" %% "pureconfig-squants" % PureConfigVersion,
    "com.onzo" %% "data" % PldVersion,
    "org.typelevel" %% "squants" % "1.2.0"
  ),
  resolvers += "Artifactory" at "https://artifactory.dev.onzo.cloud/artifactory/virsbt/"
)

lazy val commonMathLibraries = Seq(
  libraryDependencies ++= Seq(
    "org.scalanlp" %% "breeze" % BreezeVersion,
    "org.scalanlp" %% "breeze-natives" % BreezeVersion
  ),
  resolvers += "Artifactory" at "https://artifactory.dev.onzo.cloud/artifactory/virsbt/"
)

lazy val common = project.in(file("common"))
  .settings(commonSettings: _*)
  .settings(commonLibraries: _*)
  .settings(
    name := "common",
    libraryDependencies ++= Seq(
        "org.scalatest" %% "scalatest" % ScalaTestVersion % "test",
        "org.scalactic" %% "scalactic" % ScalaTestVersion
    )
  )
lazy val IntegrationTest = config("it") extend(Test)

lazy val root = (project in file("."))
  .configs(IntegrationTest)
  .settings(Defaults.itSettings: _*)
  .dependsOn(common)
  .aggregate(common)
  .settings(commonSettings: _*)
  .settings(commonLibraries: _*)
  .settings(commonMathLibraries: _*)
  .settings(
    name := "atlas-pipeline",
    version := sys.env.get("GO_PIPELINE_LABEL").getOrElse("0.0.1-SNAPSHOT"),
    assemblyJarName in assembly := s"${name.value}-fat-${version.value}.jar",
    // This seems to break the tests ??
    // testOptions in Test += Tests.Argument(TestFrameworks.ScalaTest,  "-h" , "target/test-reports"),
    assemblyMergeStrategy in assembly := {
      case PathList(ps@_*) if ps.last endsWith "io.netty.versions.properties" => MergeStrategy.first
      case PathList(ps@_*) if ps.last endsWith "logback.xml" => MergeStrategy.first
      case x => val oldStrategy = (assemblyMergeStrategy in assembly).value; oldStrategy(x)
    },
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
      "com.typesafe.akka" %% "akka-stream-contrib" % "0.6",
      "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-spray-json" % AkkaHttpVersion,
      "com.github.scopt" %% "scopt" % "3.5.0",
      "com.onzo" %% "atlas-logging" % "1.0.25",
      "com.onzo" %% "atlas-batch-collector" % BatchCollectorVersion,
      "com.onzo" %% "atlas-etl" % EtlVersion,
      "com.onzo" %% "proportionalloaddisaggregation" % PldVersion,
      "com.enragedginger" %% "akka-quartz-scheduler" % "1.6.0-akka-2.4.x",
      "com.amazonaws" % "aws-java-sdk" % "1.11.83",
      "com.datastax.cassandra" % "cassandra-driver-core" % CassandraVersion,
      "com.datastax.cassandra" % "cassandra-driver-mapping" % CassandraVersion,
      "com.datastax.cassandra" % "cassandra-driver-extras" % CassandraVersion,
      "com.github.dnvriend" %% "akka-persistence-inmemory" % "2.4.18.0" % "it,test",
      "jp.ne.opt" %% "chronoscala" % ChronoscalaVersion,
      "org.scalatest" %% "scalatest" % ScalaTestVersion % "it,test",
      "org.scalactic" %% "scalactic" % ScalaTestVersion
    ),
    resolvers += "Artifactory" at "https://artifactory.dev.onzo.cloud/artifactory/virsbt/",
    // Otherwise cassandra blows up due to a race condition
    // when trying to execute multiple DDL calls at the same time
    // however this is not a problem when running DML in parallel
    parallelExecution in IntegrationTest := false,
    mainClass in (Compile, run) := Some("com.onzo.pipeline.webserver.Runner"),
    mainClass in (Compile, packageBin) := Some("com.onzo.pipeline.webserver.Runner"),
    // Mostly stubbed code that will soon be replaced, or utilities needed solely for development
    coverageExcludedPackages :=
      "com.onzo.pipeline.CSVGenerator;com.onzo.pipeline.domain.*;" +
      "com.onzo.pipeline.Pipeline.ClientResult;com.onzo.pipeline.config.ClientConfig;" +
      "com.onzo.pipeline.stubbed.*;com.onzo.pipeline.webserver.Runner"
  )

fork in run := true
