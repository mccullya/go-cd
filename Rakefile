require 'open3'
require 'fileutils'
require 'json'
require 'etc'
require 'fileutils'
require 'socket'
require 'resolv'
#setup some variables 

def get_env(env_name,default_value)
  if(ENV[env_name].to_s != '')
    return ENV[env_name]
  end
  return default_value
end

region = get_env('AWS_REGION',"eu-west-1")
container_name = get_env('NAME',"atlas-pipeline")
aws_account = get_env('AWS_ACCOUNT',"505016431031")
version = get_env('GO_PIPELINE_LABEL',"1.0")
repository = get_env('REPOSITORY',"#{aws_account}.dkr.ecr.#{region}.amazonaws.com")
#registry_ids = "505016431031 763397143088" #not sure we need both
registry_ids = "505016431031"
base_tag="atlas/#{container_name}"
version_tag="#{base_tag}:#{version}"
latest_tag="#{base_tag}:latest"
repo_latest_tag="#{repository}/#{latest_tag}"
repo_latest_version_tag="#{repository}/#{version_tag}"
artifactory_endpoint="https://artifactory.dev.onzo.cloud/artifactory/virsbt/"

command = get_env('COMMAND',"sbt clean test it:test")


module Utils
  class Subprocess
    def initialize(cmd, &block)
      Open3.popen3(cmd) do |stdin, stdout, stderr, thread|
        { :out => stdout, :err => stderr }.each do |key, stream|
          Thread.new do
            until (line = stream.gets).nil? do
              if key == :out
                yield line, nil, thread if block_given?
              else
                yield nil, line, thread if block_given?
              end
            end
          end
        end
        thread.join # don't exit until the external process is done
        exit_code = thread.value
		if(exit_code != 0)
			puts("Failed to execute_cmd #{cmd} exit code: #{exit_code}")
			Kernel.exit(false)
		end
      end
    end
  end
end

def execute_cmd(cmd,chdir=File.dirname(__FILE__))
	puts("execute_cmd: #{cmd}")	
	Utils::Subprocess.new cmd do |stdout, stderr, thread|
  		puts "\t#{stdout}"
  		if(stderr.nil? == false)
  			puts "\t#{stderr}"	
  		end
	end
end


def is_automated_build()
	return (ENV['GO_PIPELINE_LABEL'].nil? == false)
end

def log(msg)
  puts("#{msg}\n")
end

def can_resolve_service(hostname)
  i = 0
  while i < 10
    begin
        puts(Resolv.getaddress hostname)
        return "resolved #{hostname}"
        sleep(1)
    rescue Resolv::ResolvError
       puts("failed to resolve #{hostname} [#{i}]")
       i = i + 1
    end
    fail("failed to resolve the service #{hostname}")
  end
end

def is_port_open(hostname,port)
  can_resolve_service(hostname)
  begin
    Socket.tcp(hostname, port, connect_timeout: 1) {}
    return true
  rescue Errno::ETIMEDOUT
    
  end
  return false
end


log(base_tag)
log(version_tag)
log(latest_tag)
log(repo_latest_tag)
log(repo_latest_version_tag)
#tasks

#task :default => [:inject_labels,:login_ecr,:pull_base_image, :build,:create_container,:repository_tag,:push]
task :default do 
  log("default task")
  if(is_automated_build())
    Rake::Task["automated"].invoke
  else
    Rake::Task["local"].invoke
  end
end

task :pr_build => [:set_sbt_repo, :build]
task :automated =>  [:set_sbt_repo, :build, :create_assembly, :inject_labels,:login_ecr, :create_container,:repository_tag,:push, :git_tag] 
task :local => [:login_ecr,:local_build]

task :go_task do 
    log("running #{command}")
    execute_cmd(command)
end

task :inject_labels do 
  if(is_automated_build())
    lines = File.readlines('Dockerfile')
    lines << "LABEL build-by=\"automation\" \\
                    build-git=\"#{`git rev-parse --verify HEAD`.chomp}\" \\
                    build-pipeline_name=\"#{ENV['GO_PIPELINE_NAME']}\" \\
                    build-pipeline_version=\"#{version}\" \\   
                    build-date=\"#{Time.now.getutc}\" \\
                    build-name=\"#{container_name}\" \\
                    vendor=\"onzo.com\" \\
                    license=\"restricted\"                       
                  "
    File.open("Dockerfile", "w+") do |f|
      f.puts(lines)
    end
  end
end

task :create_repo do 
  if(is_automated_build())
    found = false
    data = `aws ecr --region #{region} describe-repositories`
    json = JSON.parse(data) 
    json['repositories'].each do |repo|
      if repo['repositoryName'] == base_tag
        found = true
        puts("repository already exists #{base_tag}")
        break
      end
    end
    if(!found)
      execute_cmd("aws ecr create-repository --region #{region} --repository-name #{base_tag}")   
    end
  end
end

task :build do
  log("Building #{base_tag}")
  if(is_automated_build())
    log("running #{command}")
    execute_cmd(command)
  else  
    Rake::Task["local_build"].execute
  end   
end

task :local_build do
  log("Building #{base_tag}")
    sbt_folder = File.join(Etc.getpwuid.dir,'.sbt')
    ivy2_folder = File.join(Etc.getpwuid.dir,'.ivy2')
    user_id = Etc.getpwuid.uid
    group_id = Etc.getpwuid.gid
    execute_cmd("docker run --privileged -e \"USERID=#{user_id}\" -e \"GROUPID=#{group_id}\" -v #{Dir.getwd}:/code:Z -v #{sbt_folder}:/root/.sbt -v #{ivy2_folder}:/root/.ivy2  505016431031.dkr.ecr.eu-west-1.amazonaws.com/onzocom/gocd-agent-sbt:16.12.3 /code/localbuild.sh")
end

task :create_assembly do 
  create_assembly = "sbt 'set test in assembly := {}' assembly"
  log("running #{create_assembly}")
  execute_cmd(create_assembly)
end

task :set_sbt_repo do 
  FileUtils::mkdir_p(File.join(Etc.getpwuid.dir,'.sbt'))  
  repo_file = File.join(Etc.getpwuid.dir,'.sbt','repositories')
  if(File.file?(repo_file) == false)
    open(repo_file, 'w') { |f|
      f << "[repositories]\n"
      f << "local\n"
      f << "my-ivy-proxy-releases:#{artifactory_endpoint}, [organization]/[module]/(scala_[scalaVersion]/)(sbt_[sbtVersion]/)[revision]/[type]s/[artifact](-[classifier]).[ext]\n"
      f << "my-maven-proxy-releases: #{artifactory_endpoint}\n"
    }
  end 
end

task :create_container do 
  execute_cmd("docker build -t #{base_tag} --build-arg build_version=#{version} .")
end

task :login_ecr do
  log("logging out of ecr to bypass token expired issue")
  system("docker logout 505016431031.dkr.ecr.eu-west-1.amazonaws.com")
  log("logging into ecr") 
  system("aws configure set region #{region}") 
  login_cmd = `aws ecr get-login --region #{region} --registry-ids #{registry_ids}`
	system(login_cmd)
end

task :repository_tag do 
    execute_cmd("docker tag #{latest_tag} #{repo_latest_tag}")
    execute_cmd("docker tag #{latest_tag} #{repo_latest_version_tag}")
end

task :push do
    if(is_automated_build())
      execute_cmd("docker push #{repo_latest_tag}")
      execute_cmd("docker push #{repo_latest_version_tag}")
    end
end

task :setup_it_test do 

  puts("setup")
  puts(`kubectl delete -f k8s/cassandra.yaml`)
  puts(`kubectl delete -f k8s/schema.yaml`)  
  sleep(5)
  puts("spinning up cassandra")
  puts(`kubectl create -f k8s/cassandra.yaml`)
  sleep(10)
  puts("now waiting for port available on service atlas-pipeline-cass")

  i = 10
  while ((is_port_open("atlas-pipeline-cass",9042) == false) && i > 0)   
    i = i - 1
    sleep(2)
  end
  o = is_port_open("atlas-pipeline-cass",9042)
  puts("is port open : #{o}")
  execute_cmd("kubectl create -f k8s/schema.yaml")
  sleep(5)
  execute_cmd("kubectl logs --namespace build atlas-pipeline-schema")
  puts("run test")


  log("running #{command}")
  exitcode = 1
  process = IO.popen(command) do |io|
    while line = io.gets
      line.chomp!
      puts(line)
    end
    io.close
    exitcode = $?.exitstatus
  end
  #execute_cmd(command)
  
  puts("teardown")
  puts(`kubectl delete -f k8s/cassandra.yaml`)   
  # set schema version
  puts(`kubectl delete -f k8s/schema.yaml`)
  if(exitcode != 0 )
    fail("failed with exit code #{exitcode}")
  end

end

task :git_tag do 
  execute_cmd("git tag #{ENV['GO_PIPELINE_LABEL']}")
  execute_cmd("git push origin #{ENV['GO_PIPELINE_LABEL']}")
end 
